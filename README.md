# Implicit Interactions Analysis Tool
The Implicit Interactions Analysis Tool has been designed and developed to support the "Cybersecurity Assurance for Critical Infrastructure" project led by [Dr. Jason Jaskolka](https://carleton.ca/jaskolka/) of Carleton University, in collaboration with the [Critical Infrastructure Resilience Institute (CIRI)](https://ciri.illinois.edu/) at the University of Illinois at Urbana-Champaign. 

## Table of contents
* [Description](#description)
* [Installation](#installation)
* [Building and Running the Tool](#building-and-running-the-tool)
	+ [Configuring Maude for Running the Tool in macOS](#configuring-maude-for-running-the-tool-in-macos)
    + [Configuring Maude for Running the Tool in Linux](#configuring-maude-for-running-the-tool-in-linux)
    + [Running the Tool for the Example Systems](#running-the-tool-for-the-example-systems)
* [Resources](#resources)

## Description
Critical infrastructure systems including water and wastewater distribution systems, transportation systems, communications networks, manufacturing facilities, and energy systems consist of components linked in complex ways. This can lead to unforeseen interactions among components that may not be expected or intended by the designers and operators of the system. The presence of these implicit interactions in a system can indicate unforeseen flaws that, if not mitigated, could result in the loss of system stability. In work under CIRI support to date, a rigorous mathematical approach for identifying the existence of implicit interactions in a critical infrastructure system has been developed. Approaches for analyzing the identified implicit interactions to determine how critical system components can be directly or indirectly influenced by other system components through exploitation of the implicit interactions have also been developed.

The Implicit Interactions Analysis Tool automates the methods and approaches developed and described in [1-4] to support system designers and integrators in identifying and analyzing implicit interactions in critical systems that have been specified using the Communicating Concurrent Kleene Algebra (C<sup>2</sup>KA) modelling framework [5,6]. The Implicit Interactions Analysis Tool identifies the set of implicit interactions given the C<sup>2</sup>KA specification of a system and the specification of the set of intended interactions.
It also computes the severity, exploitability, and the set of attack scenarios for the identified set of implicit interactions.

## Installation
The Implicit Interactions Analysis Tool requires the following packages:

1. **Haskell Platform**: The Implicit Interactions Analysis Tool is implemented in the Haskell Programming Language. The Haskell Platform provides GHC 8.8.4, along with the full tool and library suite. The Haskell Programming Language can be run in Windows, Linux, and macOS. Available at [http://hackage.haskell.org/platform/](http://hackage.haskell.org/platform/).
2. **Haskell Packages**: The following Haskell packages are required: ``matrix``, ``maude``, ``parsec``, ``vector``, and ``split``. After having installed the Haskell Platform, you will be able to download and install the required packages using the Haskell Cabal (Common Architecture for Building Applications and Libraries).  To install the required Haskell packages, type the following at a Terminal: 

		sudo cabal install matrix --lib
		sudo cabal install maude --lib 
		sudo cabal install parsec --lib
		sudo cabal install vector --lib
		sudo cabal install split --lib

	*Note 1: Depending on the Haskell installation, additional packages may be required in order to resolve dependencies.*
	
	*Note 2: It is a known issue that the cabal installation of the Maude package gives an error. A corrected version of the Maude package is included in the repository in ``lib/maude``. To compile and install this version of the package, type the following at a Terminal:*

		cd <path_to>/lib/maude
		cabal configure
		cabal build
		sudo cabal install --lib
	
3. **Maude**: The Implicit Interactions Analysis Tool makes use of the Maude Term Rewriting System (version 2.7). Maude is a high-performance reflective language and system supporting both equational and rewriting logic specification and programming for a wide range of applications. This is currently included with the source files for the Implicit Interactions Analysis Tool. Maude can be run in Linux and macOS. Available at [http://maude.cs.illinois.edu/](http://maude.cs.illinois.edu/).


## Building and Running the Tool
The Implicit Interactions Analysis Tool has three pre-specified example systems:

* Manufacturing Cell Control System (see ``doc/ManufacturingCell``)
* Maritime Port Container Terminal Coordination System (see ``doc/PortTerminal``)
* Batch Chemical Reactor System (see ``doc/ChemicalReactor``)


To run the Implicit Interactions Analysis Tool, it is important to ensure that the *Maude Interface* is correctly configured for the target platform. 

### Configuring Maude for Running the Tool in macOS
In ``MaudeInterface.lhs`` the path to Maude should be set as follows in the Maude configuration functions as shown below. Specifically, we point to ``maude/maude-core/macos/maude.darwin64``.

	manufacturingConf :: MaudeConf
	manufacturingConf = MaudeConf { maudeCmd  = "maude/maude-core/macos/maude.darwin64" ,
                                    loadFiles = ["maude/manufacturing/Manufacturing.maude"]  }
								
	portConf :: MaudeConf
	portConf = MaudeConf { maudeCmd  = "maude/maude-core/macos/maude.darwin64" ,
                           loadFiles = ["maude/port/Port.maude"]     }	 
					   
	chemicalConf :: MaudeConf
	chemicalConf = MaudeConf { maudeCmd  = "maude/maude-core/macos/maude.darwin64" ,
                               loadFiles = ["maude/chemical/Chemical.maude"]     }	 

Note that these configurations correspond to the pre-specified example systems.

### Configuring Maude for Running the Tool in Linux
In ``MaudeInterface.lhs`` the path to Maude should be set as follows in the Maude configuration functions as shown below. Specifically, we point to ``maude/maude-core/linux/maude.linux64``.

	manufacturingConf :: MaudeConf
	manufacturingConf = MaudeConf { maudeCmd  = "maude/maude-core/linux/maude.linux64" ,
                                    loadFiles = ["maude/manufacturing/Manufacturing.maude"]  }
								
	portConf :: MaudeConf
	portConf = MaudeConf { maudeCmd  = "maude/maude-core/linux/maude.linux64" ,
                           loadFiles = ["maude/port/Port.maude"]     }	 
					   
	chemicalConf :: MaudeConf
	chemicalConf = MaudeConf { maudeCmd  = "maude/maude-core/linux/maude.linux64" ,
                               loadFiles = ["maude/chemical/Chemical.maude"]     }	 

Note that these configurations correspond to the pre-specified example systems.


### Running the Tool for the Example Systems
To run each of the examples, the ``maudeRewrite`` function in ``MaudeInterface.lhs`` needs to be configured with the correct Maude configuration for the corresponding example. For example, to run the maritime port container terminal coordination system example, the ``maudeRewrite`` function in ``MaudeInterface.lhs`` should contain:

	rewrite <- runMaude portConf $ Rewrite term

Once the *Maude Interface* is correctly configured, the example can be run by setting the ``main`` function in ``Test.lhs`` to point to the corresponding example. For example, to run the maritime port container terminal coordination system example, the ``main`` function in ``Test.lhs`` should contain:

	main = portExample

The Implicit Interactions Analysis Tool can be built using the Makefile included in the repository package. To use the Makefile, type ``make`` at a Terminal prompt. The Makefile will create a binary target called ``ImplicitInteractionsAnalysisTool``. 

To run the Implicit Interactions Analysis Tool, type ``./ImplicitInteractionsAnalysisTool`` at a Terminal prompt and the binary target will be executed by the system. The tool will run the function specified in the ``main`` function in the ``Test.lhs`` file. To obtain the results in a more readable and processable form, it is recommended that they be redirected to an output text file as follows:

	./ImplicitInteractionsAnalysisTool > OUTPUT.txt

	
## Resources
See the following resources for additional information about identifying and analyzing implicit interactions and the C<sup>2</sup>KA modeling framework.

1. J. Jaskolka and J. Villasenor, "[An approach for identifying and analyzing implicit interactions in distributed systems](http://ieeexplore.ieee.org/document/7875503/)," IEEE Transactions on Reliability, vol. 66, pp. 529–546, June 2017.
2. J. Jaskolka and J. Villasenor, "[Identifying implicit component interactions in distributed cyber-physical systems](http://hdl.handle.net/10125/41886)," in Proceedings of the 50th Hawaii International Conference on System Sciences, HICSS-50, pp. 5988–5997, January 2017.
3. J. Jaskolka, "[Evaluating the exploitability of implicit interactions in distributed systems](https://arxiv.org/abs/2006.06045)," arXiv:2006.06045 [cs.CR], June 2020.
4. J. Jaskolka, "[Identifying and analyzing implicit interactions in a wastewater dechlorination system](https://doi.org/10.1007/978-3-030-64330-0_3)," in Computer Security, CyberICPS 2020, SECPRE 2020, ADIoT 2020 (S. Katsikas et al., eds.), vol. 12501 of Lecture Notes in Computer Science, pp 34–51, Guildford, UK, 2020. Springer, Cham.
5. J. Jaskolka, R. Khedri, and Q. Zhang, "[Endowing concurrent Kleene algebra with communication actions](http://link.springer.com/chapter/10.1007%2F978-3-319-06251-8_2)," in Proceedings of the 14th International Conference on Relational and Algebraic Methods in Computer Science (P. Höfner, P. Jipsen, W. Kahl, and M. Müller, eds.), vol. 8428 of Lecture Notes in Computer Science, pp. 19–36, Springer International Publishing Switzerland, 2014.
6. J. Jaskolka and R. Khedri, "[A formulation of the potential for communication condition using C<sup>2</sup>KA](http://arxiv.org/abs/1408.5964)," in Proceedings of the 5th International Symposium on Games, Automata, Logics and Formal Verification (A. Peron and C. Piazza, eds.), vol. 161 of Electronic Proceedings in Theoretical Computer Science, pp. 161–174, Verona, Italy: Open Publishing Association, September 2014.