# Compiler and Flags
HS = ghc
HSFLAGS = -O2 --make -w

# Sources, Objects, Binaries, and Targets
LHS_SRC =                      \
	StimulusTypes.lhs          \
	StimulusPrinter.lhs        \
	StimulusParser.lhs         \
	Stimulus.lhs               \
	BehaviourTypes.lhs         \
	BehaviourPrinter.lhs       \
	BehaviourParser.lhs        \
	Behaviour.lhs              \
	NextStimulusTypes.lhs      \
	NextStimulusPrinter.lhs    \
	NextStimulusParser.lhs     \
	NextStimulus.lhs           \
	NextBehaviourTypes.lhs     \
	NextBehaviourPrinter.lhs   \
	NextBehaviourParser.lhs    \
	NextBehaviour.lhs          \
	LevelThreeSpecTypes.lhs    \
	LevelThreeSpecPrinter.lhs  \
	LevelThreeSpecParser.lhs   \
	LevelThreeSpec.lhs         \
	Orbits.lhs                 \
	StrongOrbits.lhs           \
	Stabilisers.lhs            \
	FixedPoints.lhs            \
	AgentTypes.lhs             \
	AgentPrinter.lhs           \
	AgentParser.lhs            \
	Agent.lhs                  \
	SoCATypes.lhs              \
	SoCAPrinter.lhs            \
	SoCAParser.lhs             \
	SoCA.lhs                   \
	MaudeInterface.lhs         \
	CommPaths.lhs              \
	PFCviaStimuli.lhs          \
	PFCviaEnvironment.lhs      \
	PFC.lhs                    \
	Implicit.lhs               \
	Intended.lhs               \
	InfluencingStimuli.lhs     \
	Analysis.lhs               \
	Simulation.lhs             \
	ManufacturingCell.lhs      \
	ChemicalReactor.lhs        \
	PortTerminal.lhs           \
	Test.lhs

SRC_DIR = src

SOURCES = $(patsubst %,$(SRC_DIR)/%,$(LHS_SRC))

BINARY = ImplicitInteractionsAnalysisTool

TARGETS = $(BINARY)

AUX = $(SRC_DIR)/*.o $(SRC_DIR)/*.hi $(SRC_DIR)/*~ 

# Build All
all : $(TARGETS)

# Build Binary Target
$(BINARY) : $(SOURCES)	
	$(HS) $(HSFLAGS) $^ -o $@ 

# Clean Up	
clean : 
	rm -f $(BINARY) $(AUX)
