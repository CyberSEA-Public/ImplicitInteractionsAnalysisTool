\subsection{StimulusPrinter}

\begin{SourceHeader}
	FileName			& : & 	StimulusPrinter.lhs		\\
	Project				& : &   \projectname			\\
	Last Modified Date 	& : & 	July 15, 2014 			\\
	Last Modified By 	& : &	Jason Jaskolka	 		\\
\end{SourceHeader}

\begin{ModuleDescription}
	The \codestyle{StimulusPrinter} module contains an implementation of a pretty printer for external stimuli. This module allows for the display of expressions from a stimulus structure and attempts to match the notation used in~\cite{Jaskolka2013aa,Jaskolka2014aa}.
\end{ModuleDescription}


\begin{code}
module StimulusPrinter ( Show (..) ) where
\end{code}


The \codestyle{StimulusTypes} module is imported in order to put the types for external stimuli in scope so that the expressions may be pretty printed. Various functions from the \codestyle{Text.PrettyPrint.HughesPJ} library are also imported to allow for the definition of the pretty printer. 
\begin{code}
import StimulusTypes

import Text.PrettyPrint.HughesPJ ( Doc         , 
                                   parens      , 
                                   renderStyle , 
                                   style       , 
                                   text        , 
                                   (<>)        , 
                                   (<+>)         )
\end{code}


An instance of the the \codestyle{Show} class is created for \codestyle{StimExpr}s. This instance uses the functions imported from the \codestyle{Text.PrettyPrint.HughesPJ} library defines a pretty printer for stimulus expressions so that when printed, they are easy to read.
\begin{code}
instance Show (StimExpr) where
	show e = renderStyle style $ prettyPrint 0 e
\end{code}


\codestyle{deactivation} provides the symbol used for the deactivation stimulus~``$D$''~($\Dstim$).
\begin{code}
deactivation :: Doc
deactivation = text "\x1D589"
\end{code}


\codestyle{neutral} provides the symbol used for the neutral stimulus~``$N$''~($\Nstim$).
\begin{code}
neutral :: Doc
neutral = text "\120211"
\end{code}


\codestyle{literal} provides the text representation required for the pretty printer of a given literal stimulus symbol.
\begin{code}
literal :: Stim -> Doc	
literal s = text s
\end{code}


\codestyle{choice} provides the symbol used for the choice operator~``$++$''~($\STIMplus$) of a stimulus structure.
\begin{code}
choice :: Doc 
choice = text "\8853"
\end{code}


\codestyle{compose} provides the symbol used for the composition operator~``$**$''~($\STIMdot$) of a stimulus structure.
\begin{code}
compose :: Doc 
compose = text "\8857"
\end{code}


\codestyle{prettyPrint} provides the pretty printing translations for a given stimulus expression (\codestyle{StimExpr}). It also takes the number of currently open parentheses in order to determine to proper grouping of terms. 
\begin{code}
prettyPrint :: Int -> StimExpr -> Doc
prettyPrint _ (D)           = deactivation
prettyPrint _ (N)           = neutral
prettyPrint _ (Literal s)   = literal s
prettyPrint p (Choice  s t) = parentheses (p > 0) $ prettyPrint 1 s <+> choice  <+> prettyPrint 2 t
prettyPrint p (Compose s t) = parentheses (p > 0) $ prettyPrint 1 s <+> compose <+> prettyPrint 2 t
\end{code}


\codestyle{parentheses} is a helper function that determines how to parenthesize a stimulus expression. This function takes a \codestyle{Bool} argument specifying whether the given \codestyle{Doc} needs to be wrapped in parentheses.
\begin{code}
parentheses :: Bool -> Doc -> Doc
parentheses True  d = parens d
parentheses False d = d
\end{code}