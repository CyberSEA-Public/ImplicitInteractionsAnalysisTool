\subsection{Implicit}

\begin{SourceHeader}
	FileName			& : & 	Implicit.lhs		\\
	Project				& : &   \projectname		\\
	Last Modified Date 	& : & 	September 2, 2016 	\\
	Last Modified By 	& : &	Jason Jaskolka	 	\\
\end{SourceHeader}

\begin{ModuleDescription}
	The \codestyle{Implicit} module provides the functions for identifying implicit component interactions in \socas.
\end{ModuleDescription}


\begin{code}
module Implicit ( findImplicit ,
                  severity       ) where
\end{code}


The \codestyle{CommType}, \codestyle{Path}, and \codestyle{Paths} types are imported from the \codestyle{CommPaths} module. A number of additional library functions are also imported to compute the longest common substring.
\begin{code}
import CommPaths    ( CommType (..) ,
                      Path          ,
                      Paths           )

import Data.Function ( on )
import Data.List     ( maximumBy ,
                       tails       )			   
\end{code}


The following function \codestyle{findImplicit} finds the implicit interactions in a list of communication paths with respect to a given set of intended communication paths represented as a list of strings. An implicit interaction is one in which the severity is not equal to 0. 
\begin{code}
findImplicit :: Paths -> Paths -> Paths
findImplicit [] int     = []
findImplicit [p] int    = if (severity int p == 0.0) then [] else [p]
findImplicit (p:ps) int = findImplicit [p] int ++ findImplicit ps int
\end{code}


The function \codestyle{severity} computes the severity of a given interaction. The \emph{severity of~$p$} (denoted~$\severity{p}$) is calculated as follows:
\begin{equation*}
	\severity{p} = 1 - \max_{q \in \intended} \bigg\{\frac{\abs{\lcs{p}{q}}}{\min \bigP{\abs{p},\abs{q}}}\bigg\}
\end{equation*}
where~$\lcs{p}{q}$ denotes the longest common substring of interactions~$p$ and~$q$.
\begin{code}
severity :: Paths -> Path -> Float
severity int p = 1.00 - (maximum $ map ( \x -> (((fromIntegral $ length $ lcs p x) - 1) / ((fromIntegral $ min (length p) (length x)) - 1)) ) int)
\end{code}


The following function \codestyle{lcs} computes the longest common substring in two lists. 
\begin{code}
lcs xs ys = maximumBy (compare `on` length) . concat $ [f xs' ys | xs' <- tails xs] ++ [f xs ys' | ys' <- drop 1 $ tails ys]
	where 
		f xs ys    = scanl g [] $ zip xs ys		
		g z (x, y) = if (x == y || ((fst x == fst y) && (snd x == X || snd y == X || snd y == B))) then z ++ [x] else []
\end{code}
