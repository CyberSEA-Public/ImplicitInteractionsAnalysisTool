\subsection{NextBehaviourPrinter}

\begin{SourceHeader}
	FileName			& : & 	NextBehaviourPrinter.lhs	\\
	Project				& : &   \projectname				\\
	Last Modified Date 	& : & 	July 15, 2014 				\\
	Last Modified By 	& : &	Jason Jaskolka	 			\\
\end{SourceHeader}

\begin{ModuleDescription}
	The \codestyle{NextBehaviourPrinter} module contains an implementation of a pretty printer for the next behaviour mapping~$\actOp$. This module allows for the display of the next behaviour mapping and attempts to match the notation used in~\cite{Jaskolka2013aa,Jaskolka2014aa}.
\end{ModuleDescription}


\begin{code}
module NextBehaviourPrinter ( printNBMap ) where
\end{code}


The \codestyle{NextBehaviourTypes} module is imported in order to put the types for the next behaviour mapping in scope so that the mapping may be pretty printed. Various functions from the \codestyle{Text.PrettyPrint.HughesPJ} library are also imported to allow for the definition of the pretty printer. 
\begin{code}
import NextBehaviourTypes
	
import Text.PrettyPrint.HughesPJ ( Doc         , 
                                   renderStyle , 
                                   style       , 
                                   text        , 
                                   (<>)        , 
                                   (<+>)         )
\end{code}


An instance of the the \codestyle{Show} class is created for \codestyle{NBExpr}s. This instance uses the functions imported from the \codestyle{Text.PrettyPrint.HughesPJ} library defines a pretty printer for elements of the next behaviour mapping so that when printed, they are easy to read.
\begin{code}
instance Show (NBExpr) where
	show e = renderStyle style $ prettyPrint e
\end{code}


\codestyle{nbOp} provides the symbol used for the next behaviour mapping~``$\actOp$''.
\begin{code}
nbOp :: Doc 
nbOp = text "\x25CB"	
\end{code}


\codestyle{prettyPrint} pretty prints elements of the next behaviour mapping.
\begin{code}
prettyPrint :: NBExpr -> Doc
prettyPrint (NB s a b) = stimToDoc s <+> nbOp <+> ckaToDoc a <+> text "=" <+> ckaToDoc b 
	where
		stimToDoc = text . show . parseStimExpr
		ckaToDoc  = text . show . parseCKAExpr
\end{code}


\codestyle{printNBMap} pretty prints a next stimulus mapping.
\begin{code}
printNBMap :: NBMap -> IO ()
printNBMap []     = return ()
printNBMap (m:ms) = (putStrLn $ "\t" ++ show m) >> printNBMap ms
\end{code}