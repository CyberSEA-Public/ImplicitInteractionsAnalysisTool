\subsection{PFC}

\begin{SourceHeader}
	FileName			& : & 	PFC.lhs				\\
	Project				& : &   \projectname		\\
	Last Modified Date 	& : & 	July 19, 2016 		\\
	Last Modified By 	& : &	Jason Jaskolka	 	\\
\end{SourceHeader}

\begin{ModuleDescription}
	The \codestyle{PFC} module provides the functionality to automatically verify the \PFC condition for covert channel existence. This module implements the tests found in~\cite{Jaskolka2014ac}.
\end{ModuleDescription}


\begin{code}
module PFC ( makeAdjMatrix            ,
             pfc                      , 
             pfcViaEnv                , 
             pfcViaStim               ,
             isCommFixedPoint         ,
             isUniversallyInfluential ,
             isStimConnected            ) where
\end{code}


A variety of types and functions are imported from the \codestyle{Agent}, \codestyle{SoCA}, \codestyle{CommPaths}, \codestyle{PFCviaStimuli}, and \codestyle{PFCviaEnvironment} modules so that they may be used in this module. Additionally, a number of functions from the Haskell \codestyle{Control.Applicative}, \codestyle{Data.Set}, and \codestyle{Data.List} libraries are imported.
\begin{code}
import Agent                 ( AgentName      ,
                               AgentSpec (..) , 
                               Agent (..)       )
import SoCA                  ( SoCA (..)   , 
                               lookupAgent   )
import CommPaths             ( AgentPair     ,
                               CommType (..) ,
                               DirectComm    ,
                               Paths         ,
                               mkTree        ,
                               paths           )
import PFCviaStimuli         ( dPFCviaStim )
import PFCviaEnvironment     ( dPFCviaEnv )

import Data.List             ( (\\)         ,
                               permutations )
import Data.Matrix as Matrix ( fromList ) 
import Data.Set as Set       ( toList   ,
                               fromList   )
import Data.Tree             ( Tree )

-- Progress monitoring
import Debug.Trace           ( trace )
\end{code}


The \codestyle{makeAdjMatrix} function constructs and adjacency matrix representation of the communication graph for a given \codestyle{SoCA}. The entries represent the edge type.
\begin{code}
makeAdjMatrix :: SoCA -> [AgentPair] -> [CommType]
makeAdjMatrix _ [] = []
makeAdjMatrix s ((a,b):ps) = if a == b then trace ("Inserting X for " ++ show a ++ " and " ++ show b ++ "...")  (X: (makeAdjMatrix s ps)) else case (dPFCviaStim s agentA agentB , dPFCviaEnv s agentA agentB) of
		(True,  True)  -> trace ("Inserting B for " ++ show a ++ " and " ++ show b ++ "...")  ( B : (makeAdjMatrix s ps))
		(True,  False) -> trace ("Inserting S for " ++ show a ++ " and " ++ show b ++ "...")  ( S : (makeAdjMatrix s ps))
		(False, True)  -> trace ("Inserting E for " ++ show a ++ " and " ++ show b ++ "...")  ( E : (makeAdjMatrix s ps))
		(False, False) -> trace ("Inserting X for " ++ show a ++ " and " ++ show b ++ "...")  ( X : (makeAdjMatrix s ps))
		where 
			agentA = lookupAgent a s
			agentB = lookupAgent b s			
\end{code}


\codestyle{dPFC} verifies the potential for direct communication between two agents. For~$\Agent{A},\Agent{B} \in \C$, we say that~$\Agent{A}$ has the \emph{\PFCD} with~$\Agent{B}$ (denoted by~$\pfcD{\Agent{A}}{\Agent{B}}$) if and only if~$\STIMcommD{\Agent{A}}{\Agent{B}} \Ors \ENVcommD{\Agent{A}}{\Agent{B}}$.
\begin{code}
dPFC :: SoCA -> Agent -> Agent -> Bool
dPFC s a b = dPFCviaStim s a b || dPFCviaEnv s a b
\end{code}


\codestyle{pfc} verifies the potential for communication between two agents. We say that~$\Agent{A}$ has the \emph{\PFC} with~$\Agent{B}$ (denoted by~$\pfc{\Agent{A}}{\Agent{B}}$) if and only if~$\pfcD{\Agent{A}}{\Agent{B}} \Ors \biglnotation{\exists}{\Agent{C}}{\Agent{C} \in \C}{\pfcD{\Agent{A}}{\Agent{C}} \nAnd \pfc{\Agent{C}}{\Agent{B}}}$. 
This function corresponds to the \PFC condition for covert channel existence~\cite{Jaskolka2014ac} and returns the satisfiability of the \PFC condition along with a list of all of the potential communications paths between the two given agents.
\begin{code}
pfc :: Tree (AgentName,CommType) -> Agent -> Agent -> (Bool, Paths)
pfc s a b = case ps of
		[]        -> (False, [])
		otherwise -> (True , ps)
	where
		ps = filter (\l -> fst (head l) == name a ) $ paths s
\end{code}


\codestyle{pfcViaEnv} verifies the potential for communication via shared environments between two agents. For~$\Agent{A},\Agent{B} \in \C$ such that~$\Agent{A} \neq \Agent{B}$, we say~$\agent{A}{a}$ has the \emph{\PFC via shared environments} with~$\agent{B}{b}$ (denoted by~$\ENVcomm{\Agent{A}}{\Agent{B}}$) if and only if~$\depTC{b}{a}$ where~$\depOpTC$ is the transitive closure of the given dependence relation. This function is implemented by determining if there is a \PFC between the two given agents and then checking whether any of the communication paths that are returned consist only of direct communications via shared environments.
\begin{code}
pfcViaEnv :: SoCA -> Agent -> Agent -> Bool
pfcViaEnv s agentA agentB = check $ snd $ pfc t agentA agentB
	where
		xs  = map name $ Set.toList $ agents s
		top = ((,) <$> xs <*> xs)
		n   = length xs
		mat = Matrix.fromList n n $ makeAdjMatrix s top
		t   = mkTree xs mat [] (name agentA) (name agentB, X)
		check :: Paths -> Bool
		check [] = False
		check (p:ps) =  all ( \x ->  E == snd x || X == snd x) p	|| check ps
\end{code}


\codestyle{pfcViaEnv} verifies the potential for communication via external stimuli between two agents. We say that~$\Agent{A}$ has the \emph{\PFC via external stimuli with~$\Agent{B}$ using at most~$n$ basic stimuli} (denoted by~$\STIMcommN{\Agent{A}}{\Agent{B}}{n}$) if and only if~$\biglnotation{\exists}{\Agent{C}}{\Agent{C} \in \C \nAnd \Agent{C} \neq \Agent{A} \nAnd \Agent{C} \neq \Agent{B}}{\STIMcommN{\Agent{A}}{\Agent{C}}{(n-1)} \nAnd \STIMcommD{\Agent{C}}{\Agent{B}}}$. More generally, we say that~$\Agent{A}$ has the \emph{\PFC via external stimuli} with~$\Agent{B}$ (denoted by~$\STIMcomm{\Agent{A}}{\Agent{B}}$) if and only if~$\biglnotation{\exists}{n}{n \ge 1}{\STIMcommN{\Agent{A}}{\Agent{B}}{n}}$. This function is implemented by determining if there is a \PFC between the two given agents and then checking whether any of the communication paths that are returned consist only of direct communications via external stimuli.
\begin{code}
pfcViaStim :: SoCA -> Agent -> Agent -> Bool
pfcViaStim s agentA agentB = check $ snd $ pfc t agentA agentB
	where
		xs  = map name $ Set.toList $ agents s
		top = ((,) <$> xs <*> xs)
		n   = length xs
		mat = Matrix.fromList n n $ makeAdjMatrix s top
		t   = mkTree xs mat [] (name agentA) (name agentB, X)
		check :: Paths -> Bool
		check [] = False
		check (p:ps) =  all ( \x ->  S == snd x || X == snd x) p	|| check ps		
\end{code}


\codestyle{isCommFixedPoint} determines whether the given agent is a communication fixed point with respect to the given \soca. We say that an agent~$\Agent{A} \in \C$ is a \emph{communication fixed point} if and only if~$\biglnotation{\forall}{\Agent{B}}{\Agent{B} \in \C \STdiff \set{\Agent{A}}}{\notSTIMcomm{\Agent{A}}{\Agent{B}}}$.
\begin{code}
isCommFixedPoint :: SoCA -> Agent -> Bool
isCommFixedPoint soca agent = all (\x -> not $ pfcViaStim soca agent x) as
	where
		as = (toList $ agents soca) \\ [agent]
\end{code}


\codestyle{isCommFixedPoint} determines whether the given agent is universally influential with respect to the given \soca. An agent~$\Agent{A} \in \C$ is said to be \emph{universally influential} if and only if~$\biglnotation{\forall}{\Agent{B}}{\Agent{B} \in \C \STdiff \set{\Agent{A}}}{\STIMcomm{\Agent{A}}{\Agent{B}}}$.
\begin{code}
isUniversallyInfluential :: SoCA -> Agent -> Bool
isUniversallyInfluential soca agent = all (\x -> pfcViaStim soca agent x) as
	where
		as = (toList $ agents soca) \\ [agent]
\end{code}


\codestyle{isStimConnected} determines whether the \soca is stimuli-connected. A \socaC is said to be \emph{stimuli-connected} if and only if for every~$X_1$ and~$X_2$ that form a partition of~$\C$, we have~$\lnotation{\exists}{\Agent{A},\Agent{B}}{\Agent{A} \in X_1 \nAnd \Agent{B} \in X_2}{\STIMcomm{\Agent{A}}{\Agent{B}} \Ors \STIMcomm{\Agent{B}}{\Agent{A}}}$.
\begin{code}
isStimConnected :: SoCA -> Bool
isStimConnected soca  
	| parts == [] = False 
	| otherwise   = allReachable soca parts
	where 
		parts = generatePartitions soca
\end{code}


\codestyle{allReachable} tests whether every agent is a participant, either as the source or sink, of at least one direct communication via external stimuli in a given \soca.
\begin{code}
allReachable :: SoCA -> [([Agent],[Agent])]	-> Bool
allReachable _ [] = True
allReachable s (p:ps) = or [(dPFCviaStim s x y) || (dPFCviaStim s y x) | x <- fst p, y <- snd p] && allReachable s ps
\end{code}	

\codestyle{generatePartitions} generates all possible partitions of the set of agents in a given \soca. Two subsets~$X_1$ and~$X_2$ of~$\C$ form a partition of~$\C$ if and only if~$X_1 \STmeet X_2 = \STbot$ and~$X_1 \STjoin X_2 = \C$. This function ignores the partitions where either one of the sets are empty.
\begin{code}
generatePartitions :: SoCA -> [([Agent],[Agent])]
generatePartitions soca = toList $ Set.fromList $ rmdups $ splits (length as) as
	where
		as = permutations $ toList $ agents soca
		splits :: Int -> [[Agent]] -> [([Agent],[Agent])]
		splits 0 l = []
		splits n l = (map (splitAt n) l) ++ (splits (n-1) l)	
		rmdups :: [([Agent],[Agent])] -> [([Agent],[Agent])]
		rmdups []     = []
		rmdups (p:ps) = case (fst p, snd p) of
			([], _)   -> rmdups ps
			(_, [])   -> rmdups ps
			otherwise -> (toList $ Set.fromList $ fst p, toList $ Set.fromList $ snd p) : (rmdups ps)
\end{code}