\subsection{Stabilisers}

\begin{SourceHeader}
	FileName			& : & 	Stabilisers.lhs		\\
	Project				& : &   \projectname		\\
	Last Modified Date 	& : & 	August 1, 2014 		\\
	Last Modified By 	& : &	Jason Jaskolka	 	\\
\end{SourceHeader}

\begin{ModuleDescription}
	The \codestyle{Stabilisers} module provides the functionality to compute the stabilisers of agent behaviours or external stimuli. 
\end{ModuleDescription}

\begin{code}
module Stabilisers ( ckaStab  , 
                     stimStab   ) where
\end{code}

We import a number of types and functions from the \codestyle{Behaviour} and \codestyle{Stimulus} modules so that they may be in scope in this module. Additionally, the \codestyle{FixedPoints} module is imported to determine where a given agent behaviour or external stimulus is a fixed point as it is needed in the computation of stabilisers. Lastly, we import the the required functions from the Haskell \codestyle{Data.Set} and \codestyle{Data.Text} libraries.
\begin{code}
import Behaviour   ( CKA               , 
                     CKASet            ,
                     parseCKAExpr      ,
                     ckaForMaude       ,
                     ckaTermLength     ,
                     generateCKATerms    )
import Stimulus    ( Stim              , 
                     StimSet           ,
                     parseStimExpr     ,
                     stimForMaude      ,
                     stimTermLength    ,
                     generateSTIMTerms   )
import FixedPoints ( isFixedCKA        , 
                     isFixedSTIM         )
					 
import MaudeInterface ( maude )	 

import Data.Set    ( fromList, map )
import Data.Text   ( unpack )
\end{code}


\codestyle{ckaStab} computes the stabiliser with respect to a stimulus set and a specified behaviour. The \emph{stabiliser} of~$a$ in~$\stim$ is the set given by~$\stab{a} = \sets{s \in \STIMset}{\lAct{a}{s} = a}$. We compute the stabiliser of an agent behaviour using a list comprehension.
\begin{code}
ckaStab :: StimSet -> Int -> CKA -> StimSet
ckaStab ss n a = fromList [show $ parseStimExpr $ filter (/= '\'') $ maude s | s <- stims, isFixedCKA s a]
	where
		stims = generateSTIMTerms ss n
\end{code}


\codestyle{stimStab}  computes the stabiliser with respect to a behaviour set and a specified external stimulus. The \emph{stabiliser} of~$s$ in~$\cka$ is the set given by~$\stab{s} = \sets{a \in \CKAset}{\lOut{a}{s} = s}$. We compute the stabiliser of an external stimulus using a list comprehension.
\begin{code}
stimStab :: CKASet -> Int  -> Stim -> CKASet
stimStab as n s = fromList [show $ parseCKAExpr $ filter (/= '\'') $ maude a | a <- behaviours, isFixedSTIM s a]
	where
		behaviours = generateCKATerms as n
\end{code}