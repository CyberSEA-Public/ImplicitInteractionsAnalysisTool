\subsection{AgentPrinter}

\begin{SourceHeader}
	FileName			& : & 	AgentPrinter.lhs 	\\
	Project				& : &   \projectname		\\
	Last Modified Date 	& : & 	July 30, 2014 		\\
	Last Modified By 	& : &	Jason Jaskolka	 	\\
\end{SourceHeader}

\begin{ModuleDescription}
	The \codestyle{AgentPrinter} module contains an implementation of a print function for agent specifications.
\end{ModuleDescription}


\begin{code}
module AgentPrinter ( printAgent ) where
\end{code}


The \codestyle{AgentTypes} module is imported in order to put the types for agents in scope so that the agent specifications may be printed. The \codestyle{printNBMap} and \codestyle{printNSMap} functions are imported respectively from the \codestyle{NextBehaviour} and \codestyle{NextStimulus} modules so that the mapping specifications for an agent may be printed.
\begin{code}
import AgentTypes    ( AgentSpec (..) , 
                       Agent (..)       )
import NextBehaviour ( printNBMap )
import NextStimulus  ( printNSMap )
\end{code}


\codestyle{printAgent} provides the printing functionality for a given agent. The function prints and formats the name and specification of the given agent. 
\begin{code}
printAgent :: Agent -> IO ()
printAgent a = do
	putStrLn $ name a ++ " := " ++ (show $ expr $ spec a)
	putStrLn $ "\x25CB_" ++ name a ++ " := "
	printNBMap $ nbmap $ spec a
	putStrLn $ "\x03BB_" ++ name a ++ " := "
	printNSMap $ nsmap $ spec a
	mapM_ print (cbspec $ spec a)
\end{code}