\subsection{BehaviourTypes}

\begin{SourceHeader}
	FileName			& : & 	BehaviourTypes.lhs	\\
	Project				& : &   \projectname		\\
	Last Modified Date 	& : & 	July 2, 2014 		\\
	Last Modified By 	& : &	Jason Jaskolka	 	\\
\end{SourceHeader}

\begin{ModuleDescription}
	The \codestyle{BehaviourTypes} module contains all of the type definitions for \CKAabbrv behaviours.
\end{ModuleDescription}


\begin{code}
module BehaviourTypes ( CKA          ,
                        CKASet       ,
                        CKAExpr (..)   ) where
\end{code}


The \codestyle{Set} constructor from the Haskell \codestyle{Data.Set} library is imported to allow for the construction of sets of \CKAabbrv behaviours.
\begin{code}
import Data.Set ( Set )
\end{code}


\codestyle{CKA} is a type for \CKAabbrv behaviours. A \CKAabbrv behaviours is represented as a \codestyle{String}.
\begin{code}
type CKA = String	
\end{code}


A \codestyle{CKASet} is a type for sets of \CKAabbrv behaviours. A \codestyle{CKASet} is represented as a set of \codestyle{CKA} behaviours. The set representation follows the mathematical definition of a \CKAabbrv and ensures that there are no duplicate behaviours.
\begin{code}
type CKASet = Set CKA	
\end{code}


A \codestyle{CKAExpr} is a data type for expressions from a \CKAabbrv. A \codestyle{CKAExpr} is constructed from the inactive agent (\codestyle{Z}), the idle agent (\codestyle{O}), \CKAabbrv literals, and the choice, sequential composition, parallel composition, sequential iteration, and parallel iteration operators. This data type construction follows the algebraic signature of a \CKAabbrv given by~$\CKAstructure$. \codestyle{CKAExpr} derives the \codestyle{Eq} class in order to allow for comparisons of \codestyle{CKAExpr}s.
\begin{code}
data CKAExpr = 
	Z                        |
	O                        |
	Literal CKA              |
	Choice  CKAExpr CKAExpr  | 
	Seq     CKAExpr CKAExpr  | 
	Par     CKAExpr CKAExpr  | 
	IterSeq CKAExpr          |
	IterPar CKAExpr				
		deriving (Eq)		
\end{code}
