\subsection{Orbits}

\begin{SourceHeader}
	FileName			& : & 	InfluencingStimuli.lhs	\\
	Project				& : &   \projectname			\\
	Last Modified Date 	& : & 	November 4, 2016 		\\
	Last Modified By 	& : &	Jason Jaskolka	 		\\
\end{SourceHeader}

\begin{ModuleDescription}
	The \codestyle{InfluencingStimuli} module provides the functionality to compute the influencing stimuli for an agent. 
\end{ModuleDescription}


\begin{code}
module InfluencingStimuli ( inflS, inflV, attackS, attackV, attack, exploit ) where
\end{code}


We import a number of types and functions from the \codestyle{Behaviour} and \codestyle{Stimulus} modules so that they may be in scope in this module. We also import the needed functions from the \codestyle{FixedPoints} module. Lastly, we import the the required functions from the Haskell \codestyle{Data.Set} and \codestyle{Data.Text} libraries.
\begin{code}
import Behaviour      ( CKA               , 
                        CKASet            ,
                        extractCKA        ,
                        parseCKAExpr      ,
                        ckaForMaude       ,
                        ckaTermLength     ,
                        generateCKATerms    )
import Stimulus       ( Stim              , 
                        StimSet           ,
                        extractStim       ,   
                        parseStimExpr     ,
                        stimForMaude      ,
                        stimTermLength    ,
                        generateSTIMTerms   )
import FixedPoints    ( isFixedCKA )						

import LevelThreeSpec      ( Stmt      , 
                             Constants , 
                             VarName   ,
                             getSpec   , 
							 getName   ,
                             getDefs   , 
                             getRefs     )
import Agent               ( AgentSpec (..) , 
                             Agent (..)       )
import SoCA                ( SoCA (..)  ,
                             lookupAgent  )

import CommPaths ( CommType (..) ,
                   Path            )

import MaudeInterface ( maude )

import Data.Set       ( Set      ,
                        (\\)     ,
						isSubsetOf, 
                        toList ,
                        fromList ,
                        intersection,
                        member   ,
						size, 						 
                        empty      )
import Data.List      ( (!!),
                         findIndex )
import Data.Maybe      ( fromJust )
import Data.Text      ( unpack )

import Debug.Trace
\end{code}


\begin{code}
inflS :: SoCA -> Agent -> StimSet
inflS soca a = fromList [s | s <- stims, not $ isFixedCKA s as]
	where
		ss    = (stim soca) \\ (fromList ["D", "N"])
		stims = generateSTIMTerms ss 1 -- for atomic stimuli only
		as = show $ expr $ spec a
\end{code}

\begin{code}
inflSS :: SoCA -> CKA -> StimSet
inflSS soca k = fromList [s | s <- stims, not $ isFixedCKA s k]
	where
		ss    = (stim soca) \\ (fromList ["D", "N"])
		stims = generateSTIMTerms ss 1 -- for atomic stimuli only
\end{code}

\begin{code}
inflV :: SoCA -> Agent -> Set VarName
inflV soca a =  fromList $ concatMap ( \x -> getRefs x cs ) stmts
	where
		stmts  = map getSpec $ cbspec $ spec a
		cs     = consts soca
\end{code}

\begin{code}
inflVV :: SoCA -> Agent -> CKA -> Set VarName
inflVV soca a k = fromList $ getRefs (getSpec (cb !! index)) cs
	where 
		cb    = cbspec $ spec $ a
		index = fromJust $ findIndex (== k) (map getName cb)
		cs     = consts soca
\end{code}

\begin{code}
defX:: Agent -> CKA -> Set VarName
defX a k = fromList $ getDefs $ getSpec (cb !! index)
	where 
		cb    = cbspec $ spec $ a
		index = fromJust $ findIndex (== k) (map getName cb)
\end{code}


\begin{code}
attackS :: SoCA -> Path -> StimSet
attackS soca [] = empty
attackS soca ((a,t):ps) 
	| t == S && length ps == 1 = inflS soca nextA
	| t == S && length ps > 1  = case (toList attS, toList attV) of 
		([] ,[])  -> empty
		([],_)    -> fromList [s | s <- infls, or (map (\x -> (s `member` (inflSS soca x)) && ((((defX nextA (nextK s x)) `intersection` attV) /= empty) )) subs  )]	
		(_,[])    -> fromList [s | s <- infls, or (map (\x -> (s `member` (inflSS soca x)) && (( any (\t -> (("\'" ++ t) `member` attS)) (extractStim $ parseStimExpr $ maude $ "NS((" ++ s ++ "),('" ++ x ++"))")) )) subs  )]	
		otherwise -> fromList [s | s <- infls, or (map (\x -> (s `member` (inflSS soca x)) && (( any (\t -> (("\'" ++ t) `member` attS)) (extractStim $ parseStimExpr $ maude $ "NS((" ++ s ++ "),('" ++ x ++"))")) || (((defX nextA (nextK s x)) `intersection` attV) /= empty) )) subs  )]	
	| otherwise                = empty
	where 
		ss    = toList $ (stim soca) \\ (fromList ["D", "N"])
		nextA = lookupAgent (fst $ head ps) soca
		subs  = toList (extractCKA $ expr $ spec nextA)
		attV  = attackV soca ps
		attS  = attackS soca ps
		nextK x y = show $ parseCKAExpr $ maude $ "NB((" ++ x ++ "),('" ++ y ++"))"
		infls = concatMap (\x -> toList $ inflSS soca x) subs
\end{code}


\begin{code}
attackV :: SoCA -> Path -> Set VarName
attackV soca [] = empty
attackV soca ((a,t):ps) 
	| t == E && length ps == 1 = inflV soca nextA
	| t == E && length ps > 1  = case (toList attS, toList attV) of 
		([],[])   -> empty
		([],_)    -> fromList [v | v <- refV, s <- infls, or (map (\x -> (v `member` (inflVV soca nextA x)) && ((((defX nextA x) `intersection` attV) /= empty) )) subs )]
		(_,[])    -> fromList [v | v <- refV, s <- infls, or (map (\x -> (v `member` (inflVV soca nextA x)) && ((any (\t -> (("\'" ++ t) `member` attS)) (extractStim $ parseStimExpr $ maude $ "NS((" ++ s ++ "),('" ++ x ++"))")) )) subs )]
		otherwise -> fromList [v | v <- refV, s <- infls, or (map (\x -> (v `member` (inflVV soca nextA x)) && ((any (\t -> (("\'" ++ t) `member` attS)) (extractStim $ parseStimExpr $ maude $ "NS((" ++ s ++ "),('" ++ x ++"))")) || (((defX nextA x) `intersection` attV) /= empty) )) subs )]	
	| otherwise                = empty
	where
		ss    = toList $ (stim soca) \\ (fromList ["D", "N"])
		nextA = lookupAgent (fst $ head ps) soca
		subs  = toList (extractCKA $ expr $ spec nextA)
		attV  = attackV soca ps
		attS  = attackS soca ps
		infls = concatMap (\x -> toList $ inflSS soca x) subs
		refV  = concatMap (\x -> toList $ inflVV soca nextA x) subs
\end{code}

\begin{code}
attack :: SoCA -> Path -> Set String
attack soca p@((a,t):ps)
	| t == S = attackS soca p
	| t == E = attackV soca p
\end{code}


\begin{code}
exploit :: SoCA -> Path -> Float
exploit soca p@((a,t):ps)
	| t == S && length ps > 1 = 
		trace ("\tatt/infl(" ++ show p ++ ") = [" ++ show (aP) ++ " INT " ++ show (aS) ++ "] / " ++ show (aS) ++ " = " ++ show (intPS) ++ " / " ++ show (aS) ++ " = " ++ show ( (fromIntegral $ size $ intPS) / (fromIntegral $ size $ aS)))
		(((fromIntegral $ size $ intPS) / (fromIntegral $ size $ aS)) * (exploit soca ps))
	| t == E && length ps > 1 =
		trace ("\tatt/ref(" ++ show p ++ ") = [" ++ show (aP) ++ " INT " ++ show (aV) ++ "] / " ++ show (aV) ++ " = " ++ show (intPV) ++ " / " ++ show (aV) ++ " = " ++  show ((fromIntegral $ size $ intPV) / (fromIntegral $ size $ aV)))
		(((fromIntegral $ size $ intPV) / (fromIntegral $ size $ aV)) * (exploit soca ps))
	| otherwise               = 
		trace ("\tatt/infl(" ++ show p ++ ") = " ++ show ((aP)) ++ " / (" ++ show (aV) ++ " OR " ++ show (aS) ++ ")\n")
		1.0
	where
		nextA = lookupAgent (fst $ head ps) soca
		aP = attack soca p
		aS = inflS soca nextA
		aV = inflV soca nextA
		intPS = aP `intersection` aS
		intPV = aP `intersection` aV
\end{code}