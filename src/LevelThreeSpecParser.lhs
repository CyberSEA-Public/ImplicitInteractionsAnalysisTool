\subsection{LevelThreeSpecParser}

\begin{SourceHeader}
	FileName			& : & 	LevelThreeSpecParser.lhs	\\
	Project				& : &   \projectname				\\
	Last Modified Date 	& : & 	June 13, 2014 				\\
	Last Modified By 	& : &	Jason Jaskolka	 			\\ \\
\end{SourceHeader}

\begin{ModuleDescription}
	The \codestyle{LevelThreeSpecParser} module contains an implementation of a parser for the \levelThree of agents. This module allows for the parsing of \codestyle{String} representations of the \levelThree of agents to \codestyle{Spec} representations.
\end{ModuleDescription}


\begin{code}
module LevelThreeSpecParser ( specParser ,
                              parseSpec    ) where
\end{code}


The \codestyle{LevelThreeSpecTypes} module is imported in order to put the types for the \levelThree of agents in scope so that the expressions, conditions, guards, statements, and specifications may be parsed. The \codestyle{(<*)} function from the Haskell \codestyle{Control.Applicative} library is imported to sequence actions while discarding the value of the second argument. Lastly, various functions required for the parsing functionality from the Haskell \codestyle{Text.Parsec} libraries are imported. 
\begin{code}
import LevelThreeSpecTypes

import Control.Applicative  ( (<*) )

import Text.Parsec          ( alphaNum ,
                              eof      ,
                              letter   ,
                              parse    ,
                              oneOf    ,
                              sepBy1   ,							  
                              (<?>)    , 
                              (<|>)      )
import Text.Parsec.Expr     ( Assoc (AssocLeft)     ,
                              Operator (Infix)      , 
                              Operator (Prefix)     ,
                              buildExpressionParser   )
import Text.Parsec.Language ( emptyDef )
import Text.Parsec.String   ( Parser )							  
import Text.Parsec.Token    ( GenLanguageDef (..) ,
                              GenTokenParser (..) ,
                              makeTokenParser       )
\end{code}


\codestyle{grammar} provides the \levelThree \codestyle{Spec} language grammar. In the \codestyle{reservedNames}, we have reserved keyword called ``end'' which is required for use later with the \codestyle{AgentParser} module to determine when to stop parsing a \codestyle{CBSpec} (\ie a list of \codestyle{Spec}s).
\begin{code}
grammar = emptyDef{ identStart      = alphaNum               ,
                    identLetter     = alphaNum               ,
                    opStart         = oneOf "=:+-/*[]|~&"    ,
                    opLetter        = oneOf "=>&|"           ,
                    reservedOpNames = ["+", "-", "*", "/"    , 
                                       ":=", "=>", "->", "|" , 
                                       "~", "&&", "||"       , 
                                       "[", "]", "()"]       ,
                    reservedNames   = ["abort", "skip"       , 
                                       "if", "fi"            , 
                                       "do", "od"            , 
                                       "send", "receive"     ,
                                       "true", "false"       , 
                                       "end"]                  }			 
\end{code}


A \codestyle{TokenParser} is constructed for the \levelThree \codestyle{Spec} language grammar.
\begin{code}
TokenParser{ parens     = m_parens     ,
             identifier = m_identifier , 
             reservedOp = m_reservedOp ,
             reserved   = m_reserved   ,
             semiSep1   = m_semiSep1   ,
             whiteSpace = m_whiteSpace   } = makeTokenParser grammar
\end{code}	


\codestyle{condParser} provides the parser to parse \levelThree \codestyle{Cond}s.
\begin{code}	
condParser :: Parser Cond
condParser = buildExpressionParser table term <?> "Cond"
	where
		table = [ [Prefix (m_reservedOp "~"   >> return (Neg))  ]          ,
		          [Infix  (m_reservedOp "&&"  >> return (And))  AssocLeft] ,
		          [Infix  (m_reservedOp "||"  >> return (Or))   AssocLeft] ,
		          [Infix  (m_reservedOp "="   >> return (Eq))   AssocLeft] ,
		          [Infix  (m_reservedOp ">"   >> return (Gt))   AssocLeft] ,
		          [Infix  (m_reservedOp "<"   >> return (Lt))   AssocLeft] ,
		          [Infix  (m_reservedOp ">="  >> return (Geq))  AssocLeft] ,
		          [Infix  (m_reservedOp "<="  >> return (Leq))  AssocLeft]   ]
		term  = m_parens condParser
		        <|> fmap Con m_identifier 
		        <|> (m_reserved "true"  >> return (T))
		        <|> (m_reserved "false" >> return (F))
\end{code}


\codestyle{exprParser} provides the parser to parse \levelThree \codestyle{Expr}s.
\begin{code}	
exprParser :: Parser Expr
exprParser = buildExpressionParser table term <?> "Expr"
	where
		table = [ [Infix (m_reservedOp "+"  >> return (Add)) AssocLeft] ,
		          [Infix (m_reservedOp "-"  >> return (Sub)) AssocLeft] ,
		          [Infix (m_reservedOp "*"  >> return (Mul)) AssocLeft] ,
		          [Infix (m_reservedOp "/"  >> return (Div)) AssocLeft]   ]
		term  = m_parens exprParser
		        <|> fmap Val m_identifier
\end{code}


\codestyle{guardParser} provides the parser to parse \levelThree \codestyle{Guard}s.
\begin{code}
guardParser :: Parser Guard
guardParser = fmap Alt (sepBy1 guard (m_reservedOp "|"))
	where
		guard = do
			c <- condParser
			m_reservedOp "->"
			s <- stmtParser
			return (G c s)	
\end{code}


\codestyle{stmtParser} provides the parser to parse \levelThree \codestyle{Stmt}s.
\begin{code}	
stmtParser :: Parser Stmt
stmtParser = fmap Seq (m_semiSep1 stmt)
	where
		stmt = (m_reserved "abort" >> return (Abort))
			<|>  (m_reserved "skip"  >> return (Skip))
			<|> do 
				v <- m_identifier
				m_reservedOp ":="
				e <- exprParser
				return (v := e)
			<|> do 
				m_reserved "if"
				g <- guardParser
				m_reserved "fi"
				return (If g)
			<|> do
				m_reserved "do"
				g <- guardParser
				m_reserved "od"
				return (Do g)
			<|> do
				m_reserved "send"
				e <- exprParser
				return (Send e)
			<|> do
				m_reserved "receive"
				e <- exprParser
				return (Receive e)				
\end{code}


\codestyle{specParser} provides the parser to parse \levelThree \codestyle{Spec}s and is used as a part of the main parser.
\begin{code}	
specParser :: Parser Spec
specParser = do 
	n <- m_identifier
	m_reservedOp "=>"
	m_reservedOp "["
	s <- stmtParser
	m_reservedOp "]"
	return (n ::= s)
\end{code}


\codestyle{mainParser} is the main parsing function for \levelThree \codestyle{Spec}s. It ignores whitespace and ends when the ``end of file'' (\codestyle{eof}) token of the input string is reached.
\begin{code}
mainParser :: Parser Spec
mainParser = m_whiteSpace >> specParser <* eof	
\end{code}


\codestyle{parseSpec} translates a \codestyle{String} representation of a \levelThree into the \codestyle{Spec} data type representation.
\begin{code}
parseSpec :: String -> Spec
parseSpec input = case parse mainParser "" input of
	Left  err -> error $ show err
	Right ans -> ans   			  
\end{code}