\subsection{NextStimulusTypes}

\begin{SourceHeader}
	FileName			& : & 	NextStimulusTypes.lhs	\\
	Project				& : &   \projectname			\\
	Last Modified Date 	& : & 	July 15, 2014 			\\
	Last Modified By 	& : &	Jason Jaskolka	 		\\
\end{SourceHeader}

\begin{ModuleDescription}
	The \codestyle{NextStimulusTypes} module contains all of the type definitions for the next stimulus mapping~$\lOutSig$ from the \rightSemimodule{\cka}~$\OutSemimodule$. 
\end{ModuleDescription}


\begin{code}
module NextStimulusTypes ( NSExpr (..)      ,
                           NSMap            ,
                           module Behaviour , 
                           module Stimulus    ) where
\end{code}


The \codestyle{Behaviour} and \codestyle{Stimulus} modules are imported to interface with the type definitions that they provide.
\begin{code}
import Behaviour ( CKA           ,
                   parseCKAExpr    )
import Stimulus  ( Stim          ,
                   parseStimExpr   )
\end{code}


An \codestyle{NSExpr} is is a data type for elements of the next stimulus mapping~$\outOp$. An \codestyle{NSExpr} is constructed following the signature~$\lOutSig$ of the next stimulus mapping. \codestyle{NSExpr} derives the \codestyle{Eq} class in order to allow for comparisons of \codestyle{NSExpr}s.
\begin{code}
data NSExpr = NS Stim CKA Stim
	deriving (Eq)	
\end{code}


An \codestyle{NSMap} is a type for the next stimulus mapping~$\outOp$. An \codestyle{NSMap} is represented by a list of \codestyle{NSExpr}s.
\begin{code}	
type NSMap = [NSExpr]
\end{code}