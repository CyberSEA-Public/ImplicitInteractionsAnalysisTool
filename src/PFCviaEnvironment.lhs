\subsection{PFCviaEnvironment}

\begin{SourceHeader}
	FileName			& : & 	PFCviaEnvironment.lhs	\\
	Project				& : &   \projectname		\\
	Last Modified Date 	& : & 	August 6, 2014 		\\
	Last Modified By 	& : &	Jason Jaskolka	 	\\
\end{SourceHeader}

\begin{ModuleDescription}
	The \codestyle{PFCviaEnvironment} module provides the functionality to automatically verify the \PFC condition for covert channel existence. This module implements the tests found in~\cite{Jaskolka2014ac}.
\end{ModuleDescription}


\begin{code}
module PFCviaEnvironment ( dPFCviaEnv ) where
\end{code}


A variety of types and functions are imported from the \codestyle{LevelThreeSpec}, \codestyle{Agent}, and \codestyle{SoCA} modules so that they may be used in this module. Additionally, a number of functions from the Haskell \codestyle{Control.Applicative} and \codestyle{Data.List} libraries are imported.
\begin{code}
import LevelThreeSpec      ( Stmt      , 
                             Constants , 
                             getSpec   , 
                             getDefs   , 
                             getRefs     )
import Agent               ( AgentSpec (..) , 
                             Agent (..)       )
import SoCA                ( SoCA (..) )

import Control.Applicative ( (<$>) , 
                             (<*>)   )
import Data.List           ( intersect )
\end{code}


\codestyle{dPFCviaEnv} verifies the potential for direct communication via shared environments between two agents. For~$\Agent{A},\Agent{B} \in \C$ such that~$\Agent{A} \neq \Agent{B}$, we say~$\agent{A}{a}$ has the \emph{\PFCD via shared environments} with~$\agent{B}{b}$ (denoted by~$\ENVcommD{\Agent{A}}{\Agent{B}}$) if and only if~$\dep{b}{a}$. In this case, the dependency relation~$\depOp$ is defined as~$\dep{b}{a} \mIff \mathrm{Def}(a) \STmeet \mathrm{Ref}(b) \neq \STbot$ where~$\mathrm{Def}(a)$ is the set of all defined variables in the behaviour~$a$ and~$\mathrm{Ref}(b)$ is the set of all referenced variables in the behaviour~$b$.
\begin{code}	
dPFCviaEnv :: SoCA -> Agent -> Agent -> Bool
dPFCviaEnv soca agentA agentB =  any ( \x -> not $ null $ (getDefs $ fst x) `intersect` (getRefs (snd x) cs) ) stmts	
	where
		as    = map getSpec $ cbspec $ spec agentA
		bs    = map getSpec $ cbspec $ spec agentB
		stmts = (,) <$> as <*> bs
		cs    = consts soca
\end{code}