\subsection{Orbits}

\begin{SourceHeader}
	FileName			& : & 	Orbits.lhs			\\
	Project				& : &   \projectname		\\
	Last Modified Date 	& : & 	July 29, 2014 		\\
	Last Modified By 	& : &	Jason Jaskolka	 	\\
\end{SourceHeader}

\begin{ModuleDescription}
	The \codestyle{Orbits} module provides the functionality to compute the orbits of agent behaviours or external stimuli. 
\end{ModuleDescription}


\begin{code}
module Orbits ( ckaOrbit  ,
                stimOrbit   ) where
\end{code}


We import a number of types and functions from the \codestyle{Behaviour} and \codestyle{Stimulus} modules so that they may be in scope in this module. Additionally, the \codestyle{maude} function from the \codestyle{MaudeInterface} module is imported to allow for calls to \maude. Lastly, we import the the required functions from the Haskell \codestyle{Data.Set} and \codestyle{Data.Text} libraries.
\begin{code}
import Behaviour      ( CKA               , 
                        CKASet            ,
                        parseCKAExpr      ,
                        ckaForMaude       ,
                        ckaTermLength     ,
                        generateCKATerms    )
import Stimulus       ( Stim              , 
                        StimSet           ,
                        parseStimExpr     ,
                        stimForMaude      ,
                        stimTermLength    ,
                        generateSTIMTerms   )
import MaudeInterface ( maude )

import Data.Set       ( fromList,
                        size      )
import Data.Text      ( unpack )
\end{code}



\codestyle{ckaOrbit} computes an orbit with respect to a stimulus set and a specified behaviour. The \emph{orbit} of~$a$ in~$\stim$ is the set given by~$\orb{a} = \sets{\lAct{a}{s}}{s \in \STIMset}$. We compute the orbit of an agent behaviour using a list comprehension.
\begin{code}
ckaOrbit :: StimSet -> CKA -> CKASet
ckaOrbit ss a = fromList [maude $ "NB((" ++ s ++ "),(" ++ term ++"))" | s <- stims]
	where
		expr  = parseCKAExpr a
		n     = (ckaTermLength expr) + 2 -- plus 2 to accomodate the number of total choices of behaviours
		stims = generateSTIMTerms ss n
		term  = unpack $ ckaForMaude expr
\end{code}


\codestyle{stimOrbit} computes an orbit with respect to a behaviour set and a specified stimulus.. The \emph{orbit} of~$s$ in~$\cka$ is the set given by~$\orb{s} = \sets{\lOut{a}{s}}{a \in \CKAset}$. We compute the orbit of an external stimulus using a list comprehension.
\begin{code}
stimOrbit :: CKASet -> Stim -> StimSet
stimOrbit as s = fromList [maude $ "NS((" ++ term ++ "),(" ++ a ++"))" | a <- behaviours]
	where
		expr       = parseStimExpr s
		n          = stimTermLength expr -- length StimSet
		behaviours = generateCKATerms as n
		term       = unpack $ stimForMaude expr
\end{code}