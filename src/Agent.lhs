\subsection{Agent}

\begin{SourceHeader}
	FileName			& : & 	Agent.lhs			\\
	Project				& : &   \projectname		\\
	Last Modified Date 	& : & 	July 30, 2014 		\\
	Last Modified By 	& : &	Jason Jaskolka	 	\\
\end{SourceHeader}

\begin{ModuleDescription}
	The \codestyle{Agent} module is a high-level module for agents and agent specification and provides an interface for the \codestyle{AgentTypes}, \codestyle{AgentPrinter}, and \codestyle{AgentParser} modules.\\
\end{ModuleDescription}


\begin{code}
module Agent ( load                , 
               module AgentTypes   , 
               module AgentPrinter , 
               module AgentParser    ) where
\end{code}


The \codestyle{AgentTypes}, \codestyle{AgentPrinter}, and \codestyle{AgentParser} modules are imported so that they may be in scope in this module and so that they can be exported as part of the interface of this module. The \codestyle{CKAExpr}, \codestyle{NSMap}, \codestyle{NBMap}, \codestyle{CBSpec} constructors are imported respectively from the \codestyle{Behaviour}, \codestyle{NextStimulus}, \codestyle{NextBehaviour}, and \codestyle{LevelThreeSpec} modules so that they may be used in the specification of an agent. Lastly, the \codestyle{catchIOError} function from the Haskell \codestyle{System.IO.Error} library is imported to catch and handle file reading errors.
\begin{code}
import AgentTypes
import AgentPrinter
import AgentParser

import Behaviour      ( CKAExpr )
import NextStimulus   ( NSMap )
import NextBehaviour  ( NBMap )
import LevelThreeSpec ( CBSpec )

import System.IO.Error ( catchIOError )
\end{code}


\codestyle{newAgent} constructs an \codestyle{Agent} from a given agent name and specification.
\begin{code}
newAgent :: AgentName -> AgentSpec -> Agent
newAgent n s = Agent n s
\end{code}


\codestyle{newAgentSpec} constructs an \codestyle{AgentSpec} from a CKA term, next behaviour mapping, next stimulus mapping, and \levelThree.
\begin{code}
newAgentSpec :: CKAExpr -> NBMap -> NSMap -> CBSpec -> AgentSpec
newAgentSpec cka nb ns specs = AgentSpec cka nb ns specs
\end{code}


\codestyle{load} reads the agent specification file at the given \codestyle{FilePath}. If the read occurs without error, \codestyle{load} returns the parsed \codestyle{Agent}, otherwise it returns an error message.
\begin{code}
load :: FilePath -> IO Agent
load agent = catchIOError (readFile agent) (errorMsg agent) >>= \a -> return $ parseAgent a
	where
		errorMsg :: FilePath -> IOError -> IO String
		errorMsg fp = error $ "File " ++ fp ++ " does not exist!" 
\end{code}