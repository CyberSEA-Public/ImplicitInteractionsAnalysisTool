\subsection{Simulation}

\begin{SourceHeader}
	FileName			& : & 	Simulation.lhs		\\
	Project				& : &   \projectname		\\
	Last Modified Date 	& : & 	March 24, 2017 		\\
	Last Modified By 	& : &	Jason Jaskolka	 	\\
\end{SourceHeader}

\begin{ModuleDescription}
	The \codestyle{Simulation} module provides the functionality to build simulation traces and to assess the potential impact of attack scenarios.
\end{ModuleDescription}


\begin{code}
module Simulation ( states, events, traces, simulate, buildSys ) where
\end{code}

We import a number of types and functions from the \codestyle{Behaviour} and \codestyle{Stimulus} modules so that they may be in scope in this module. Additionally, the \codestyle{maude} function from the \codestyle{MaudeInterface} module is imported to allow for calls to \maude. Lastly, we import a number of functions from the Haskell libraries.
\begin{code}
import Behaviour ( CKA, CKAExpr, parseCKAExpr, ckaForMaude )
import Stimulus  ( Stim, StimExpr, parseStimExpr, stimForMaude )

import Agent
import SoCA

import MaudeInterface ( maude )

import Data.Text ( Text, pack, unpack, split, strip )
import Data.List ( isInfixOf, maximumBy, intercalate )
import Data.List.Split ( splitOn )
import Data.Ord ( comparing )
import Data.Set ( toList )

import Debug.Trace
\end{code}


\codestyle{states} computes the set of abstract system states for a given behaviour.
\begin{code}
states :: CKAExpr -> [CKAExpr]
states a = map parseCKAExpr $ map stripParens $ map unpack $ map strip $ split (== '+') $ pack $ maude aTerm
	where
		aTerm = unpack $ ckaForMaude a
\end{code}

\codestyle{events} computes the set of abstract system events for a given stimulus.
\begin{code}
events :: StimExpr -> [StimExpr]
events s = map parseStimExpr $ map stripParens $ map unpack $ map strip $ map pack $ splitOn "\8853" $ maude sTerm
	where
		sTerm = unpack $ stimForMaude s
\end{code}


The following functions are used to clean up the abstract states or events by identifying and removing unneeded parentheses that are introduced by \maude.
\begin{code}
-- Strips the unneeded parentheses by taking the term between the outside matched parentheses. If there are no parentheses, or iff all parenthese are ballenced (i.e.,(2*length ps) == cnt) then simply take the entire string.
stripParens :: String -> String
stripParens s = case (odd cnt) of
	True -> filter (/='(') $ filter (/=')') s
	False -> take (j - i + 1) (drop i s)
	where
		ps  = parenPairs s
		cnt = countParens s
		(i,j) = case (ps == [] || (2*length ps) == cnt) of
			True -> (0,length s)
			False -> outsideParens ps	

-- Finds the pairs of parenthesis from a string representation of a term.
parenPairs :: String -> [(Int, Int)]
parenPairs = go 0 []
  where
    go _ _        []         = []
    go j acc      ('(' : cs) =          go (j + 1) (j : acc) cs
    go j []       (')' : cs) =          go (j + 1) []        cs -- unbalanced parentheses!
    go j (i : is) (')' : cs) = (i, j) : go (j + 1) is        cs
    go j acc      (c   : cs) =          go (j + 1) acc       cs

countParens :: String -> Int
countParens s = length $ filter (\x -> x == '(' || x == ')') s

-- Finds the parenthesis pair that represents the outside pair for a given term. Any parentheses insude this reange are needed, and any outside this range are not.
outsideParens :: [(Int, Int)] -> (Int, Int)
outsideParens = maximumBy $ comparing diff 
	where
		diff p = (snd p) - (fst p)
\end{code}


Build the simulation tree!
\codestyle{traces} computes the set of system traces given an abstract system states and an abstract system event. It terminates when there is a fixed point. Otherwise for each step in the simulation, it records the next behavior and stimulus. If there is any branching, it records each path. In this way, this function returns a list of paths/sequences representing the possible execution traces.
\begin{code}
traces :: StimExpr -> CKAExpr -> [[(StimExpr,CKAExpr)]]
traces s a
	| (x == s') && (y == a') 				= [[(x, y)]]
	| otherwise = [[(x, y)] ++ p | p <- concat [(traces x' y') | x' <- evs, y' <- sts]]
	where
		aTerm = unpack $ ckaForMaude a
		sTerm = unpack $ stimForMaude s
		x = parseStimExpr $ maude sTerm
		y = parseCKAExpr $ maude aTerm
		s' = parseStimExpr $ maude $ "NS((" ++ sTerm ++ "),(" ++ aTerm ++"))"
		a' = parseCKAExpr $ maude $ "NB((" ++ sTerm ++ "),(" ++ aTerm ++"))"
		evs = events s'
		sts = states a'	
\end{code}


\codestyle{simulate} computes the set of system traces given any stimulus and behaviour. This equates to finding all paths from a forest of execution trees.
\begin{code}
simulate :: String -> String -> [[(StimExpr,CKAExpr)]]
simulate s a = concat [traces x y | x <- events sexpr, y <- states aexpr]
	where 
		sexpr = parseStimExpr s
		aexpr = parseCKAExpr a
\end{code}

\codestyle{buildSys} builds the abstract system specification by joining each agent specification by parallel composition. This is used for the simulation.
\begin{code}
buildSys :: SoCA -> CKAExpr
buildSys soca = parseCKAExpr $ intercalate " * " as
	where 
	as = map show $ map expr $ map spec $ toList $ agents soca
\end{code}
