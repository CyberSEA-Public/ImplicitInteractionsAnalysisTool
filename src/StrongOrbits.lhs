\subsection{StrongOrbits}

\begin{SourceHeader}
	FileName			& : & 	StrongOrbits.lhs	\\
	Project				& : &   \projectname		\\
	Last Modified Date 	& : & 	July 29, 2014 		\\
	Last Modified By 	& : &	Jason Jaskolka	 	\\
\end{SourceHeader}

\begin{ModuleDescription}
	The \codestyle{StrongOrbits} module provides the functionality to compute the strong orbits of agent behaviours or external stimuli. 
\end{ModuleDescription}


\begin{code}
module StrongOrbits ( ckaStrongOrbit  , 
                      stimStrongOrbit   ) where
\end{code}


We import a number of types and functions from the \codestyle{Behaviour} and \codestyle{Stimulus} modules so that they may be in scope in this module. Additionally, the \codestyle{Orbits} module is imported as the computation of orbits is needed for the computation of strong orbits. Lastly, we import the the required functions from the Haskell \codestyle{Data.Set} library.
\begin{code}
import Behaviour ( CKA       , 
                   CKASet      )
import Stimulus  ( Stim      , 
                   StimSet     )
import Orbits    ( ckaOrbit  , 
                   stimOrbit   )

import Data.Set  ( toList    ,
                   fromList    )
\end{code}

\codestyle{ckaStrongOrbit} computes the strong orbit with respect to a stimulus set, a behaviour set, and a specified behaviour. The \emph{strong orbit} of~$a$ in~$\stim$ is the set given by~$\orbS{a} = \sets{b \in \CKAset}{\orb{b} = \orb{a}}$. We compute the strong orbit of an agent behaviour using a list comprehension.
\begin{code}
ckaStrongOrbit :: StimSet -> CKASet -> CKA -> CKASet
ckaStrongOrbit s bs a = fromList [b | b <- toList bs, ckaOrbit s a == ckaOrbit s (filter (/= '\'') b)] 
\end{code}


\codestyle{stimStrongOrbit} computes the strong orbit with respect to a behaviour set, a stimulus set, and a specified external stimulus. The \emph{strong orbit} of~$s$ in~$\cka$ is the set given by~$\orbS{s} = \sets{t \in \STIMset}{\orb{t} = \orb{s}}$. We compute the strong orbit of an external stimulus using a list comprehension.
\begin{code}
stimStrongOrbit :: CKASet -> StimSet -> Stim -> StimSet
stimStrongOrbit a ss t = fromList [s | s <- toList ss, stimOrbit a t == stimOrbit a (filter (/= '\'') s)] 
\end{code}