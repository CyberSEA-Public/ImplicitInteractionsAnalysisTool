\subsection{NextStimulus}

\begin{SourceHeader}
	FileName			& : & 	NextStimulus.lhs	\\
	Project				& : &   \projectname		\\
	Last Modified Date 	& : & 	July 15, 2014 		\\
	Last Modified By 	& : &	Jason Jaskolka	 	\\
\end{SourceHeader}

\begin{ModuleDescription}
	The \codestyle{NextStimulus} module is a high-level module for next stimulus mappings and provides an interface for the \codestyle{NextStimulusTypes}, \codestyle{NextStimulusPrinter}, and \codestyle{NextStimulusParser} modules.
\end{ModuleDescription}


\begin{code}
module NextStimulus ( nsForMaude                 ,  
                      module NextStimulusTypes   , 
                      module NextStimulusPrinter , 
                      module NextStimulusParser    ) where
\end{code}


The \codestyle{NextStimulusTypes}, \codestyle{NextStimulusPrinter}, and \codestyle{NextStimulusParser} modules are imported so that they may be in scope in this module and so that they can be exported as part of the interface of this module. A number of functions from the Haskell \codestyle{Data.Text} library are imported for use in the functions implemented in this module.
\begin{code}
import NextStimulusTypes
import NextStimulusPrinter
import NextStimulusParser

import Data.Text as Text ( Text ,  
                           pack   )
\end{code}


\codestyle{nsForMaude} translates a given \codestyle{NSExpr} to a \codestyle{Text} representation which is suitable for use with \maude.
\begin{code}
nsForMaude :: NSExpr -> Text
nsForMaude (NS s a t)
	| s == "N" || s == "D" = case t of
		"N" -> pack $ "NS(" ++ s ++ ",'" ++ a ++ ") =  " ++ t
		"D" -> pack $ "NS(" ++ s ++ ",'" ++ a ++ ") =  " ++ t
		otherwise            -> pack $ "NS(" ++ s ++ ",'" ++ a ++ ") = '" ++ t	
	| t == "N" || t == "D" = pack $ "NS('" ++ s ++ ",'" ++ a ++ ") =  " ++ t
	| otherwise            = pack $ "NS('" ++ s ++ ",'" ++ a ++ ") = '" ++ t
\end{code}