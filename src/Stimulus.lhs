\subsection{Stimulus}

\begin{SourceHeader}
	FileName			& : & 	Stimulus.lhs		\\
	Project				& : &   \projectname		\\
	Last Modified Date 	& : & 	July 29, 2014 		\\
	Last Modified By 	& : &	Jason Jaskolka	 	\\
\end{SourceHeader}

\begin{ModuleDescription}
	The \codestyle{Stimulus} module is a high-level module for external stimuli and provides an interface for the \codestyle{StimulusTypes}, \codestyle{StimulusPrinter}, and \codestyle{StimulusParser} modules.
\end{ModuleDescription}

\begin{code}
module Stimulus ( extractStim            , 
                  generateSTIMTerms      ,
                  isValidStimExpr        ,
                  stimForMaude           ,
                  stimTermLength         ,
                  module StimulusTypes   , 
                  module StimulusPrinter , 
                  module StimulusParser    ) where
\end{code}


The \codestyle{StimulusTypes}, \codestyle{StimulusPrinter}, and \codestyle{StimulusParser} modules are imported so that they may be in scope in this module and so that they can be exported as part of the interface of this module. A number of functions from the Haskell \codestyle{Data.Set} and \codestyle{Data.Text} libraries are imported for use in the functions implemented in this module.
\begin{code}
import StimulusTypes
import StimulusPrinter
import StimulusParser

import Data.Set as Set   ( empty      , 
                           isSubsetOf ,
                           singleton  , 
                           union      , 
                           toList       )
import Data.Text as Text ( Text       , 
                           concat     , 
                           pack         )
\end{code}


\codestyle{extractStim} extracts the set of all of the basic stimuli in a given \codestyle{StimExpr}.
\begin{code}
extractStim :: StimExpr -> StimSet
extractStim D             = Set.empty
extractStim N             = Set.empty
extractStim (Literal s)   = Set.singleton s 
extractStim (Choice  s t) = extractStim s `union` extractStim t
extractStim (Compose s t) = extractStim s `union` extractStim t
\end{code}


\codestyle{isValidStimExpr} verifies that each basic stimulus in a given \codestyle{StimExpr} is a member of the given \codestyle{StimSet} representing the set of basic stimuli that may be part of a valid \codestyle{StimExpr}.
\begin{code}
isValidStimExpr :: StimExpr -> StimSet -> Bool
isValidStimExpr expr set = extractStim expr `isSubsetOf` set
\end{code}


\codestyle{generateSTIMTerms} generates a list of terms of the stimulus structure consisting of all possible combinations of sequences of the given set of basic stimuli. \codestyle{generateSTIMTerms} also requires the maximum length of a generated term (\ie the maximum number of basic stimuli appearing in a term). The maximum length of a generated term is typically given as the length of the longest cycle in a given Mealy automata representation of the \levelOne of an agent.
\begin{code}
generateSTIMTerms :: StimSet -> Int -> [Stim]
generateSTIMTerms s 1 = toList s 
generateSTIMTerms s n = generateSTIMTerms s (n-1) ++ buildSTIMTerms n (toList s) (toList s)	
\end{code}


\codestyle{buildSTIMTerms} is a helper function for \codestyle{generateSTIMTerms}. It generates a list of all possible combinations of stimulus terms up to a given length using the given lists of basic stimuli.
\begin{code}
buildSTIMTerms :: Int -> [Stim] -> [Stim] -> [Stim]
buildSTIMTerms 1 _ _ = []
buildSTIMTerms n s t = concatSTIMTerms s t ++ (buildSTIMTerms (n-1) s $ concatSTIMTerms s t)
\end{code}


\codestyle{concatSTIMTerms} is a helper function for \codestyle{buildSTIMTerms}. It generates a list of all possible combinations of stimulus terms by concatenating each stimulus in the first given list to each stimulus of the second given list using the composition operator~``**''~($\STIMdot$) for stimuli.
\begin{code}
concatSTIMTerms :: [Stim] -> [Stim] -> [Stim]
concatSTIMTerms []     _ = [] 
concatSTIMTerms (s:ss) l = map ((s ++ " ** ") ++) l ++ concatSTIMTerms ss l
\end{code}


\codestyle{stimForMaude} translates a given \codestyle{StimExpr} to a \codestyle{Text} representation which is suitable for use with \maude.
\begin{code}
stimForMaude :: StimExpr -> Text
stimForMaude D             = pack $ "D" -- deactivation
stimForMaude N             = pack $ "N" -- neutral
stimForMaude (Literal s)   = pack $ "'" ++ s
stimForMaude (Choice  s t) = Text.concat [pack "(", stimForMaude s, pack " ++ ", stimForMaude t, pack ")"]
stimForMaude (Compose s t) = Text.concat [pack "(", stimForMaude s, pack " ** ", stimForMaude t, pack ")"]	
\end{code}


\codestyle{stimTermLength} determines the number of basic external stimuli is a given \codestyle{StimExpr}.
\begin{code}
stimTermLength :: StimExpr -> Int
stimTermLength D             = 1
stimTermLength N             = 1
stimTermLength (Literal s)   = 1
stimTermLength (Choice  s t) = stimTermLength s + stimTermLength t
stimTermLength (Compose s t) = stimTermLength s + stimTermLength t
\end{code}