\subsection{BehaviourPrinter}

\begin{SourceHeader}
	FileName			& : & 	BehaviourPrinter.lhs	\\
	Project				& : &   \projectname			\\
	Last Modified Date 	& : & 	July 15, 2014 			\\
	Last Modified By 	& : &	Jason Jaskolka	 		\\
\end{SourceHeader}

\begin{ModuleDescription}
	The \codestyle{BehaviourPrinter} module contains an implementation of a pretty printer for \CKAabbrv behaviours. This module allows for the display of expressions from a \CKAabbrv and attempts to match the notation used in~\cite{Jaskolka2013aa,Jaskolka2014aa}.
\end{ModuleDescription}


\begin{code}
module BehaviourPrinter ( Show (..) ) where
\end{code}


The \codestyle{BehaviourTypes} module is imported in order to put the types for \CKAabbrv behaviours in scope so that the expressions may be pretty printed. Various functions from the \codestyle{Text.PrettyPrint.HughesPJ} library are also imported to allow for the definition of the pretty printer. 
\begin{code}
import BehaviourTypes
import Prelude hiding ((<>))
import Text.PrettyPrint.HughesPJ ( Doc         , 
                                   parens      , 
                                   renderStyle , 
                                   style       , 
                                   text        , 
                                   (<>)        , 
                                   (<+>)         )
\end{code}


An instance of the the \codestyle{Show} class is created for \codestyle{CKAExpr}s. This instance uses the functions imported from the \codestyle{Text.PrettyPrint.HughesPJ} library to define a pretty printer for \CKAabbrv behaviour expressions so that when printed, they are easy to read.
\begin{code}
instance Show (CKAExpr) where
	show e = renderStyle style $ prettyPrint 0 e
\end{code}


\codestyle{inactive} provides the symbol used for the inactive agent behaviour~``$0$''.
\begin{code}
inactive :: Doc
inactive = text "0"	
\end{code}


\codestyle{idle} provides the symbol used for the idle agent behaviour~``$1$''.
\begin{code}
idle :: Doc
idle = text "1"	
\end{code}


\codestyle{literal} provides the text representation required for the pretty printer of a given literal \CKAabbrv behaviour symbol.
\begin{code}
literal :: CKA -> Doc	
literal a = text a
\end{code}


\codestyle{choice} provides the symbol used for the choice operator~``$+$'' of a \CKAabbrv.
\begin{code}
choice :: Doc 
choice = text "+"	
\end{code}


\codestyle{seqt} provides the symbol used for the sequential composition operator~``$\CKAseq$'' of a \CKAabbrv.
\begin{code}
seqt :: Doc 
seqt = text ";"	
\end{code}


\codestyle{para} provides the symbol used for the parallel composition operator~``$\CKApar$'' of a \CKAabbrv.
\begin{code}
para :: Doc 
para = text "*"	
\end{code}


\codestyle{iterSeq} provides the symbol used for the sequential iteration operator~``$\:\hat{}\:\CKAseq$''~($\CKAiterSeq{}$) of a \CKAabbrv.
\begin{code}
iterSeq :: Doc 
iterSeq = text "^;"	
\end{code}


\codestyle{iterPar} provides the symbol used for the parallel iteration operator~``$\:\hat{}\:\CKApar$''~($\CKAiterPar{}$) of a \CKAabbrv.
\begin{code}
iterPar :: Doc 
iterPar = text "^*"	
\end{code}


\codestyle{prettyPrint} provides the pretty printing translations for a given \CKAabbrv behaviour expression (\codestyle{CKAExpr}). It also takes the number of currently open parentheses in order to determine to proper grouping of terms. 
\begin{code}
prettyPrint :: Int -> CKAExpr -> Doc
prettyPrint _ (Z)          = inactive
prettyPrint _ (O)          = idle
prettyPrint _ (Literal a)  = literal a
prettyPrint p (Choice a b) = parentheses (p > 0) $ prettyPrint 1 a <+> choice <+> prettyPrint 2 b
prettyPrint p (Seq a b)    = parentheses (p > 0) $ prettyPrint 1 a <+>  seqt  <+> prettyPrint 2 b
prettyPrint p (Par a b)    = parentheses (p > 0) $ prettyPrint 1 a <+>  para  <+> prettyPrint 2 b
prettyPrint p (IterSeq a)  = parentheses (p > 0) $ prettyPrint 1 a <> iterSeq
prettyPrint p (IterPar a)  = parentheses (p > 0) $ prettyPrint 1 a <> iterPar
\end{code}


\codestyle{parentheses} is a helper function that determines how to parenthesise a stimulus expression. This function takes a \codestyle{Bool} argument specifying whether the given \codestyle{Doc} needs to be wrapped in parentheses.
\begin{code}
parentheses :: Bool -> Doc -> Doc
parentheses True  d = parens d
parentheses False d = d
\end{code}