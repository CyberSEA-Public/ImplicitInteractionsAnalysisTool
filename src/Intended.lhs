\subsection{Intended}

\begin{SourceHeader}
	FileName			& : & 	Intended.lhs		\\
	Project				& : &   \projectname		\\
	Last Modified Date 	& : & 	July 26, 2016 		\\
	Last Modified By 	& : &	Jason Jaskolka	 	\\
\end{SourceHeader}

\begin{ModuleDescription}
	The \codestyle{Intended} module provides the functions for generating the set of intended system interactions.
\end{ModuleDescription}


\begin{code}
module Intended ( getAgents ,
                  intPaths  ,
                  mkIntTree , 
                  mkMatrix    ) where
\end{code}


The \codestyle{AgentName} and \codestyle{CommType} types are imported from the \codestyle{Agent} and \codestyle{CommPaths} modules, respectively. A number of additional library functions are also imported to work with matrices and trees.
\begin{code}
import Agent     ( AgentName )
import CommPaths ( CommType (..) )
	
import Data.Tree   ( Tree (..) )
import Data.List   ( lookup , 
                     (\\)   ,
                     nub    ,
                     sortOn   )
import Data.Matrix ( Matrix   , 
                     fromList ,
                     getRow   ,
                     setElem    )					 
import Data.Maybe  ( fromJust )
import Data.Vector ( findIndices , 
                     toList        )
\end{code}


The \codestyle{mkIntTree} function generates the tree representation of the unrolled message-passing diagram of a system design. The first argument is the list of enumerated nodes in the unrolled message-passing diagram. The second argument is an adjacency matrix of the underlying communication graph of the unrolled message-passing diagram. The third argument is the source agent node and the termination communication type \codestyle{X}. The function builds a tree based on the nodes that can be reached. Duplicates are allowed.
\begin{code}
mkIntTree :: [(Int, AgentName)] -> Matrix CommType -> ((Int, AgentName),CommType) -> Tree (AgentName,CommType)
mkIntTree as adjMat (source,t)
	| snk == []  = Node (src,t) []
	| otherwise  = Node (src,t) (map (mkIntTree as adjMat) snk)
		where
			snk = canReach (fst source) as adjMat
			src = snd source
\end{code}

The \codestyle{canReach} function identifies the reachable nodes in the unrolled message-passing diagram from a given node. The first argument is the node number, the second is a list of the enumerated nodes, and the third is the adjacency matrix of the underlying communication graph of the unrolled message-passing diagram. The function returns the list of reachable nodes and the edge type by which they can be reached.
\begin{code}
canReach :: Int -> [(Int, AgentName)] -> Matrix CommType -> [((Int, AgentName),CommType)]
canReach n xs adj = (zip ss stype) ++ (zip es etype) ++ (zip bs btype)
	where
		snk    = fromJust $ lookup n xs
		row    = getRow n adj
		ss     = map (xs !!) $ toList $ findIndices (== S) row
		stype  = replicate (length ss) S
		es     = map (xs !!) $ toList $ findIndices (== E) row
		etype  = replicate (length es) E
		bs     = map (xs !!) $ toList $ findIndices (== B) row
		btype  = replicate (length bs) B
\end{code}

The \codestyle{intPaths} function traverses a tree representation of the intended system interactions and lists them as paths. The helper functions \codestyle{treeFold} and \codestyle{allPaths} are responsible for traversing the tree and recording the visited nodes for each path. The \codestyle{typeShift} function rotates the communication types so that the list is in the form \codestyle{(Node,CommType)} where the communication type respects the edges that reaches the next nod (``reaches'' instead of ``reached by''.
\begin{code}
intPaths :: Tree (a,b) -> [[(a,b)]]
intPaths = map typeShift . allPaths

treeFold :: (a -> [b] -> b) -> Tree a -> b
treeFold func (Node header list) = func header (map (treeFold func) list)

allPaths :: Tree a -> [[a]]
allPaths t = treeFold (\a fs p ->
	let p' = p ++ [a] in
		if null fs then [p'] else concatMap ($ p') fs) t []

typeShift :: [(a,b)] -> [(a,b)]
typeShift l = zip as (ts ++ [t])
	where
		(as,(t:ts)) = unzip l	 
\end{code}

The \codestyle{mkMatrix} function constructs the adjacency matrix of the underlying communication graph of the unrolled message-passing diagram. It is generated from a list of the edges of the unrolled message-passing diagram. The \codestyle{processEdges} and \codestyle{initMatrix} are sued to initialize the matrix and set the elements based on the edges.
\begin{code}
mkMatrix :: [((Int,AgentName),(Int,AgentName),CommType)] -> Matrix CommType
mkMatrix es	= processEdges es (initMatrix es)
		
processEdges :: [((Int,AgentName),(Int,AgentName),CommType)] -> Matrix CommType -> Matrix CommType
processEdges []     m = m
processEdges ((a,b,t):es) m = processEdges es (setElem t ((fst a),(fst b)) m)

initMatrix :: [((Int,AgentName),(Int,AgentName),CommType)] -> Matrix CommType
initMatrix l	= fromList n n (replicate (n*n) X)
	where
		n = length $ getAgents l
\end{code}


The \codestyle{getAgents} function retrieves the enumeration mapping of the nodes in the unrolled message-passing diagram.
\begin{code}
getAgents :: [((Int,AgentName),(Int,AgentName),CommType)] -> [(Int,AgentName)]
getAgents l = sortOn fst $ nub (x ++ y)
	where
		(x,y,z) = unzip3 l
\end{code}