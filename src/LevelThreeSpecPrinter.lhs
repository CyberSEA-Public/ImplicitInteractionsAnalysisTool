\subsection{LevelThreeSpecPrinter}

\begin{SourceHeader}
	FileName			& : & 	LevelThreeSpecPrinter.lhs	\\
	Project				& : &   \projectname				\\
	Last Modified Date 	& : & 	July 16, 2014 				\\
	Last Modified By 	& : &	Jason Jaskolka	 			\\
\end{SourceHeader}

\begin{ModuleDescription}
	The \codestyle{LevelThreeSpecPrinter} module contains an implementation of a pretty printer for the \levelThree of agents. This module allows for the display of Dijkstra's guarded commands~\cite{Dijkstra1975aa}.
\end{ModuleDescription}


\begin{code}
module LevelThreeSpecPrinter ( Show (..) ) where
\end{code}


The \codestyle{LevelThreeSpecTypes} module is imported in order to put the types for the \levelThree of agent behaviours in scope so that the expressions may be pretty printed. Various functions from the \codestyle{Text.PrettyPrint.HughesPJ} library are also imported to allow for the definition of the pretty printer. 
\begin{code}
import LevelThreeSpecTypes

import Prelude hiding ((<>))
import Text.PrettyPrint.HughesPJ ( Doc         ,
                                   parens      , 
                                   renderStyle , 
                                   style       , 
                                   text        , 
                                   (<>)        , 
                                   (<+>)         )
\end{code}


An instance of the the \codestyle{Show} class is created for \codestyle{Spec}s. This instance uses the functions imported from the \codestyle{Text.PrettyPrint.HughesPJ} library defines a pretty printer for the \levelThree of agent behaviours so that when printed, they are easy to read.
\begin{code}
instance Show (Spec) where
	show e = renderStyle style $ prettySpec e
\end{code}


\codestyle{val} provides the text representation required for the pretty printer of a given literal value symbol.
\begin{code}
val :: String -> Doc	
val v = text v
\end{code}


\codestyle{add} provides the symbol used for the addition operator~$+$ for \levelThree \codestyle{Expr}s.
\begin{code}
add :: Doc 
add = text "+"	
\end{code}


\codestyle{sub} provides the symbol used for the subtraction operator~$-$ for \levelThree \codestyle{Expr}s.
\begin{code}
sub :: Doc 
sub = text "-"	
\end{code}


\codestyle{mul} provides the symbol used for the multiplication operator~$*$ for \levelThree \codestyle{Expr}s.
\begin{code}
mul :: Doc 
mul = text "*"	
\end{code}


\codestyle{divide} provides the symbol used for the division operator~$\backslash$ for \levelThree \codestyle{Expr}s.
\begin{code}
divide :: Doc 
divide = text "\\"	
\end{code}


\codestyle{neg} provides the symbol used for the negation operator~$\Not$ for \levelThree \codestyle{Cond}s.
\begin{code}
neg :: Doc 
neg = text "\x00AC"	
\end{code}


\codestyle{wedge} provides the symbol used for the conjunction operator~$\wedge$ for \levelThree \codestyle{Cond}s.
\begin{code}
wedge :: Doc 
wedge = text "\x2227"
\end{code}


\codestyle{vee} provides the symbol used for the disjunction operator~$\vee$ for \levelThree \codestyle{Cond}s.
\begin{code}
vee :: Doc 
vee = text "\x2228"
\end{code}

\codestyle{eqs} provides the symbol used for the equality operator~$=$ for \levelThree \codestyle{Cond}s.
\begin{code}
eqs :: Doc 
eqs = text "="	
\end{code}


\codestyle{arrow} provides the symbol used for the ``then'' guard operator~$\rightarrow$ for \levelThree \codestyle{Guard}s.
\begin{code}
arrow :: Doc 
arrow = text "\x2192"
\end{code}


\codestyle{alt} provides the symbol used for the alternative guard operator~$|$ for \levelThree \codestyle{Guard}s. \codestyle{alt} also provide a newline formatting character.
\begin{code}
alt :: Doc 
alt = text "\n |"	
\end{code}


\codestyle{defn} provides the symbol used for the assignment operator~$:=$ for \levelThree \codestyle{Stmt}s.
\begin{code}
defn :: Doc 
defn = text ":="	
\end{code}


\codestyle{seqs} provides the symbol used for the sequential composition operator~$;$ for \levelThree \codestyle{Stmt}s.
\begin{code}
seqs :: Doc 
seqs = text ";"	
\end{code}


\codestyle{is} provides the symbol used for the assignment operator~$\Rightarrow$ for \levelThree \codestyle{Spec}s. \codestyle{is} also provide a newline formatting character.
\begin{code}
is :: Doc 
is = text "\x21D2 \n"	
\end{code}


\codestyle{prettyExpr} provides the pretty printing translations for a given \levelThree expression (\codestyle{Expr}). It also takes the number of currently open parentheses in order to determine to proper grouping of terms. 
\begin{code}
prettyExpr :: Int -> Expr -> Doc
prettyExpr _ (Val e)   = val e
prettyExpr p (Add a b) = parentheses (p > 0) $ prettyExpr 1 a <+> add    <+> prettyExpr 2 b
prettyExpr p (Sub a b) = parentheses (p > 0) $ prettyExpr 1 a <+> sub    <+> prettyExpr 2 b
prettyExpr p (Mul a b) = parentheses (p > 0) $ prettyExpr 1 a <+> mul    <+> prettyExpr 2 b
prettyExpr p (Div a b) = parentheses (p > 0) $ prettyExpr 1 a <+> divide <+> prettyExpr 2 b
\end{code}


\codestyle{prettyCond} provides the pretty printing translations for a given \levelThree condition (\codestyle{Cond}). It also takes the number of currently open parentheses in order to determine to proper grouping of terms. 
\begin{code}
prettyCond :: Int -> Cond -> Doc
prettyCond _ (T)       = text "T"
prettyCond _ (F)       = text "F"
prettyCond p (Con c)   = text c
prettyCond p (Neg a)   = parentheses (p > 0) $ neg <> prettyCond 1 a
prettyCond p (And a b) = parentheses (p > 0) $ prettyCond 1 a <+> wedge <+> prettyCond 2 b
prettyCond p (Or a b)  = parentheses (p > 0) $ prettyCond 1 a <+> vee   <+> prettyCond 2 b
prettyCond p (Eq a b)  = parentheses (p > 0) $ prettyCond 1 a <+> eqs   <+> prettyCond 2 b
\end{code}


\codestyle{prettyGuard} provides the pretty printing translations for a given \levelThree guard (\codestyle{Guard}). 
\begin{code}
prettyGuard :: Guard -> Doc
prettyGuard (G c s)      = prettyCond 0 c <+> arrow <+> prettyStmt s
prettyGuard (Alt [])     = text "\n"
prettyGuard (Alt [g])    = prettyGuard g <+> prettyGuard (Alt [])
prettyGuard (Alt (g:gs)) = prettyGuard g <+> alt <+> prettyGuard (Alt gs)
\end{code}


\codestyle{prettyStmt} provides the pretty printing translations for a given \levelThree statement (\codestyle{Stmt}). 
\begin{code}
prettyStmt :: Stmt -> Doc
prettyStmt (Abort)      = text "abort"
prettyStmt (Skip)       = text "skip"
prettyStmt (s := e)     = val s <+> defn <+> prettyExpr 0 e
prettyStmt (Seq [])     = text ""
prettyStmt (Seq [s])    = prettyStmt s <+> prettyStmt (Seq [])
prettyStmt (Seq (s:ss)) = prettyStmt s <> seqs <+> prettyStmt (Seq ss)
prettyStmt (If g)       = text "if" <+> prettyGuard g <+> text "fi"
prettyStmt (Do g)       = text "do" <+> prettyGuard g <+> text "od"
\end{code}


\codestyle{prettySpec} provides the pretty printing translations for a given \levelThree of an agent behaviour (\codestyle{Spec}). 
\begin{code}
prettySpec :: Spec -> Doc
prettySpec (n ::= s) = val n <+> is <+> prettyStmt s
\end{code}


\codestyle{parentheses} is a helper function that determines how to parenthesize a stimulus expression. This function takes a \codestyle{Bool} argument specifying whether the given \codestyle{Doc} needs to be wrapped in parentheses.
\begin{code}
parentheses :: Bool -> Doc -> Doc
parentheses True  d = parens d
parentheses False d = d
\end{code}