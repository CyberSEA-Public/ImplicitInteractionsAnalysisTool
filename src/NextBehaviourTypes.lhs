\subsection{NextBehaviourTypes}

\begin{SourceHeader}
	FileName			& : & 	NextBehaviourTypes.lhs	\\
	Project				& : &   \projectname			\\
	Last Modified Date 	& : & 	July 15, 2014 			\\
	Last Modified By 	& : &	Jason Jaskolka	 		\\
\end{SourceHeader}

\begin{ModuleDescription}
	The \codestyle{NextBehaviourTypes} module contains all of the type definitions for the next behaviour mapping~$\lActSig$ from the \leftSemimodule{\stim}~$\ActSemimodule$. 
\end{ModuleDescription}


\begin{code}
module NextBehaviourTypes ( NBExpr (..)      ,
                            NBMap            ,
                            module Behaviour , 
                            module Stimulus    ) where
\end{code}


The \codestyle{Behaviour} and \codestyle{Stimulus} modules are imported to interface with the type definitions that they provide.
\begin{code}
import Behaviour ( CKA           ,
                   parseCKAExpr    )
import Stimulus  ( Stim          ,
                   parseStimExpr   )
\end{code}


An \codestyle{NBExpr} is is a data type for elements of the next behaviour mapping~$\actOp$. An \codestyle{NBExpr} is constructed following the signature~$\lActSig$ of the next behaviour mapping. \codestyle{NBExpr} derives the \codestyle{Eq} class in order to allow for comparisons of \codestyle{NBExpr}s.
\begin{code}
data NBExpr = NB Stim CKA CKA
	deriving (Eq)	
\end{code}


An \codestyle{NBMap} is a type for the next behaviour mapping~$\actOp$. An \codestyle{NBMap} is represented by a list of \codestyle{NBExpr}s.
\begin{code}	
type NBMap = [NBExpr]
\end{code}