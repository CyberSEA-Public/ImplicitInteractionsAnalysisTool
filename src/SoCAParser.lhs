\subsection{SoCAParser}

\begin{SourceHeader}
	FileName			& : & 	SoCAParser.lhs		\\
	Project				& : &   \projectname		\\
	Last Modified Date 	& : & 	July 30, 2014 		\\
	Last Modified By 	& : &	Jason Jaskolka	 	\\
\end{SourceHeader}

\begin{ModuleDescription}
	The \codestyle{SoCAParser} module contains an implementation of a parser for \socas. This module allows for the parsing of \codestyle{String} representations of \socas of the form shown in Figure~\ref{fig:soca_spec} to \codestyle{SoCA} representations.
\begin{figure}
\begin{center}
	\begin{minipage}{2in}
		\begin{verbatim}
			???
		\end{verbatim}
	\end{minipage}
\end{center}
\label{fig:soca_spec}
\caption{Form of a \soca specification file.}
\end{figure}
\end{ModuleDescription}


\begin{code}
module SoCAParser ( parseSoCA ) where
\end{code}


The \codestyle{SoCA} and \codestyle{Agent} types are imported respectively from the \codestyle{SoCATypes} and \codestyle{Agent} modules so that they may be used in the printing of a \soca. Additionally, various types and functions are imported from the Haskell \codestyle{Data.Set} library.
\begin{code}
import SoCATypes (SoCA)
\end{code}


\codestyle{parseSoCA} translates a \codestyle{String} representation \soca into the \codestyle{SoCA} data type representation.
\begin{code}	
parseSoCA :: String -> SoCA
parseSoCA input = error $ "parseSoCA: NOT YET IMPLEMENTED"  
\end{code}

