\subsection{NextBehaviourParser}

\begin{SourceHeader}
	FileName			& : & 	NextBehaviourParser.lhs	\\
	Project				& : &   \projectname			\\
	Last Modified Date 	& : & 	July 16, 2014 			\\
	Last Modified By 	& : &	Jason Jaskolka	 		\\
\end{SourceHeader}

\begin{ModuleDescription}
	The \codestyle{NextBehaviourParser} module contains an implementation of a parser for next behaviour mappings. This module allows for the parsing of \codestyle{String} representations of next behaviour mappings from a \leftSemimodule{\stim}~$\ActSemimodule$ to \codestyle{NBMap} representations.
\end{ModuleDescription}


\begin{code}
module NextBehaviourParser ( exprParser , 
                             parseNBMap   ) where
\end{code}


The \codestyle{NextBehaviourTypes} module is imported in order to put the types for next behaviour mappings in scope so that the expressions and mappings may be parsed. The \codestyle{(<*)} function from the Haskell \codestyle{Control.Applicative} library is imported to sequence actions while discarding the value of the second argument. Lastly, various functions required for the parsing functionality from the Haskell \codestyle{Text.Parsec} libraries are imported. 
\begin{code}
import NextBehaviourTypes

import Control.Applicative  ( (<*) )

import Text.Parsec          ( alphaNum ,
                              eof      ,
                              letter   ,
                              parse    ,
                              oneOf      )
import Text.Parsec.Language ( emptyDef )
import Text.Parsec.String   ( Parser )							  
import Text.Parsec.Token    ( GenLanguageDef (..) ,
                              GenTokenParser (..) ,
                              makeTokenParser       )
\end{code}


\codestyle{grammar} provides the \codestyle{NBExpr} language grammar.
\begin{code}
grammar = emptyDef{ identStart      = alphaNum                ,
                    identLetter     = alphaNum              ,
                    opStart         = oneOf "(),="          ,
                    opLetter        = oneOf "(),="          ,
                    reservedOpNames = ["(",")",",","=","."] ,
                    reservedNames   = []                      }					  
\end{code}


A \codestyle{TokenParser} is constructed for the \codestyle{NBExpr} language grammar.
\begin{code}
TokenParser{ parens     = m_parens     , 
             identifier = m_identifier , 
             reservedOp = m_reservedOp ,
             reserved   = m_reserved   ,
             semiSep1   = m_semiSep1   ,
             whiteSpace = m_whiteSpace   } = makeTokenParser grammar
\end{code}	

	
\codestyle{exprParser} provides the parser to parse \codestyle{NBExpr}s and is used as a part of the main parser.
\begin{code}	
exprParser :: Parser NBExpr
exprParser = do 
	m_reservedOp "("
	s <- m_identifier
	m_reservedOp ","
	a <- m_identifier
	m_reservedOp ")"
	m_reservedOp "="
	b <- m_identifier
	return $ NB s a b
\end{code}


\codestyle{mainParser} is the main parsing function for \codestyle{NBExpr}s. It ignores whitespace and ends when the ``end of file'' (\codestyle{eof}) token of the input string is reached.
\begin{code}	
mainParser :: Parser NBExpr
mainParser = m_whiteSpace >> exprParser <* eof
\end{code}


\codestyle{parseNBExpr} translates a \codestyle{String} representation of an element of a next behaviour mapping into the \codestyle{NBExpr} data type representation.
\begin{code}	
parseNBExpr :: String -> NBExpr
parseNBExpr input = case parse mainParser "" input of
	Left  err -> error $ show err
	Right ans -> ans   		  
\end{code}


\codestyle{parseNBMap} translates a list of \codestyle{String} representations of elements of a next behaviour mapping into the \codestyle{NBMap} type representation.
\begin{code}	
parseNBMap :: [String] -> NBMap
parseNBMap = map parseNBExpr
\end{code}