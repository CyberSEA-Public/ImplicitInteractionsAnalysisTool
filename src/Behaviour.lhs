\subsection{Behaviour}

\begin{SourceHeader}
	FileName			& : & 	Behaviour.lhs		\\
	Project				& : &   \projectname		\\
	Last Modified Date 	& : & 	July 29, 2014 		\\
	Last Modified By 	& : &	Jason Jaskolka	 	\\
\end{SourceHeader}

\begin{ModuleDescription}
	The \codestyle{Behaviour} module is a high-level module for \CKAabbrv behaviours and provides an interface for the \codestyle{BehaviourTypes}, \codestyle{BehaviourPrinter}, and \codestyle{BehaviourParser} modules.
\end{ModuleDescription}


\begin{code}
module Behaviour ( extractCKA              , 
                   generateCKATerms        ,
                   isValidCKAExpr          ,
                   ckaForMaude             ,
                   ckaTermLength           ,
                   module BehaviourTypes   , 
                   module BehaviourPrinter , 
                   module BehaviourParser    ) where
\end{code}


The \codestyle{BehaviourTypes}, \codestyle{BehaviourPrinter}, and \codestyle{BehaviourParser} modules are imported so that they may be in scope in this module and so that they can be exported as part of the interface of this module. A number of functions from the Haskell \codestyle{Data.Set} and \codestyle{Data.Text} libraries are imported for use in the functions implemented in this module.
\begin{code}
import BehaviourTypes
import BehaviourPrinter
import BehaviourParser	

import Data.Set as Set   ( empty      , 
                           isSubsetOf ,
                           singleton  , 
                           union      , 
                           toList       )
import Data.Text as Text ( Text       , 
                           concat     , 
                           pack         )
\end{code}


\codestyle{extractCKA} extracts the set of all of the basic \CKAabbrv behaviours in a given \codestyle{CKAExpr}.
\begin{code}
extractCKA :: CKAExpr -> CKASet
extractCKA Z            = Set.empty
extractCKA O            = Set.empty
extractCKA (Literal a)  = Set.singleton a
extractCKA (Choice a b) = extractCKA a `union` extractCKA b
extractCKA (Seq a b)    = extractCKA a `union` extractCKA b
extractCKA (Par a b)    = extractCKA a `union` extractCKA b
extractCKA (IterSeq a)  = extractCKA a
extractCKA (IterPar a)  = extractCKA a
\end{code}

\codestyle{isValidCKAExpr} verifies that each basic \CKAabbrv behaviour in a given \codestyle{CKAExpr} is a member of the given \codestyle{CKASet} representing the set of basic \CKAabbrv behaviours that may be part of a valid \codestyle{CKAExpr}.
\begin{code}
isValidCKAExpr :: CKAExpr -> CKASet -> Bool
isValidCKAExpr expr set = extractCKA expr `isSubsetOf` set
\end{code}


\codestyle{generateCKATerms} generates a list of terms of the \CKAabbrv consisting of all possible combinations of sequences of the given set of basic \CKAabbrv behaviours. \codestyle{generateCKATerms} also requires the maximum length of a generated term (\ie the maximum number of basic \CKAabbrv behaviours appearing in a term). The maximum length of a generated term is typically given as the length of the longest cycle in a given Mealy automata representation of the \levelOne of an agent.
\begin{code}
generateCKATerms :: CKASet -> Int -> [CKA]
generateCKATerms a 1 = toList a
generateCKATerms a n = generateCKATerms a (n-1) ++ buildCKATerms n (toList a) (toList a)	
\end{code}


\codestyle{buildCKATerms} is a helper function for \codestyle{generateCKATerms}. It generates a list of all possible combinations of \CKAabbrv behaviour terms up to a given length using the given lists of basic \CKAabbrv behaviours.
\begin{code}
buildCKATerms :: Int -> [CKA] -> [CKA] -> [CKA]
buildCKATerms 1 _ _ = []
buildCKATerms n a b = concatCKATerms a b ++ (buildCKATerms (n-1) a $ concatCKATerms a b)
\end{code}


\codestyle{concatCKATerms} is a helper function for \codestyle{buildCKATerms}. It generates a list of all possible combinations of \CKAabbrv behaviour terms by concatenating each \CKAabbrv behaviour in the first given list to each \CKAabbrv behaviour of the second given list using the sequential composition operator~``$\CKAseq$'' for \CKAabbrv behaviours.
\begin{code}
concatCKATerms :: [CKA] -> [CKA] -> [CKA]
concatCKATerms []     _ = [] 
concatCKATerms (a:as) l = map ((a ++ " ; ") ++) l ++ concatCKATerms as l
\end{code}


\codestyle{ckaForMaude} translates a given \codestyle{CKAExpr} to a \codestyle{Text} representation which is suitable for use with \maude.
\begin{code}
ckaForMaude :: CKAExpr -> Text
ckaForMaude Z            = pack $ "0" -- inactive
ckaForMaude O            = pack $ "1" -- idle
ckaForMaude (Literal a)  = pack $ "'" ++ a
ckaForMaude (Choice a b) = Text.concat [pack "(", ckaForMaude a, pack " + ", ckaForMaude b, pack ")"]
ckaForMaude (Seq a b)    = Text.concat [pack "(", ckaForMaude a, pack " ; ", ckaForMaude b, pack ")"]
ckaForMaude (Par a b)    = Text.concat [pack "(", ckaForMaude a, pack " * ", ckaForMaude b, pack ")"]
ckaForMaude (IterSeq a)  = Text.concat [pack "s2(", ckaForMaude a, pack ")"]
ckaForMaude (IterPar a)  = Text.concat [pack "s1(", ckaForMaude a, pack ")"]
\end{code}


\codestyle{ckaTermLength} determines the number of basic behaviours is a given \codestyle{CKAExpr}.
\begin{code}
ckaTermLength :: CKAExpr -> Int
ckaTermLength Z            = 1
ckaTermLength O            = 1
ckaTermLength (Literal a)  = 1
ckaTermLength (Choice a b) = ckaTermLength a + ckaTermLength b
ckaTermLength (Seq a b)    = ckaTermLength a + ckaTermLength b
ckaTermLength (Par a b)    = ckaTermLength a + ckaTermLength b
ckaTermLength (IterSeq a)  = 1 + ckaTermLength a
ckaTermLength (IterPar a)  = 1 + ckaTermLength a
\end{code}