\subsection{ManufacturingCell}

\begin{SourceHeader}
	FileName			& : & 	PortTerminal.lhs		\\
	Project				& : &   \projectname			\\
	Last Modified Date 	& : & 	December 7, 2016 		\\
	Last Modified By 	& : &	Jason Jaskolka	 		\\
\end{SourceHeader}

\begin{ModuleDescription}
	Provides a platform for testing the implementation of the \projectname with support for implicit interactions analysis.
\end{ModuleDescription}


\begin{code}
module PortTerminal ( portExample ) where
\end{code}

A number of types and functions are imported from various modules in order to run the analysis to identify implicit interactions in the manufacturing cell example.
\begin{code}
import Agent          ( AgentName ,
                        load      ,
						expr, spec  )
import Analysis       ( runAnalysis )
import CommPaths      ( CommType (..) , 
                        Paths,
						printPaths     )						
import Intended       ( getAgents , 
                        mkIntTree , 
                        mkMatrix  , 
                        intPaths    )
import MaudeInterface ( generateMaudeSpec ,
                        generateMaudeSOCA   )
import SoCA           ( SoCA         ,
                        addAgent     ,
                        newSoCA      , 
                        printSoCA    , 
                        setStimSet   , 
                        setCKASet    ,
                        setConstants   )

import Orbits	
-- For testing Influencing Stimuli
import InfluencingStimuli	
import SoCAPrinter		

-- -- For SIMULATION
-- import Simulation
-- import Data.Text as Text ( Text, concat, pack, unpack )
-- import Behaviour
-- import Stimulus
\end{code}

\codestyle{portExample} runs an analysis on the port container terminal example to identify the implicit interactions that may exist.
\begin{code}
portExample :: IO ()
portExample = do

	-- ESTABLISH SETS OF BASIC STIMULI AND BEHAVIORS
	let stimSet = setStimSet ["arrive", "mnge1", "mnge2", "ship1", "ship2", "crane1", "crane2", "allocd", "berth", "dock", "oper1", "oper2", "carrier", "assgnd", "serve", "served", "done", "compl1", "compl2", "deprt1", "deprt2", "D", "N"]	
	let ckaSet = setCKASet ["DEPART", "CLEARa", "CLEARb", "INIT", "MANa", "MANb", "SRVT", "POSN", "LEAVE", "CRANES", "PLAN", "DOCK", "RLSE", "ALLO", "FREE", "READ", "CARGO", "SEQ", "SERVE", "UPDT", "OPER", "AVAIL", "ASSGN", "NEAR", "MOVE", "0", "1", "SRVTb", "POSNb", "LEAVEb","CRANESb", "PLANb", "DOCKb", "RLSEb"]
	let constants = setConstants ["TRUE", "FALSE", "NULL", "MANIFEST", "LENGTH", "BAYS", "CONTAINERS", "ARRIVETIME", "DEPARTTIME", "WAITTIME", "CRANEEFF", "COMPL1", "COMPL2", "CRANE1", "CRANE2", "OPER1", "OPER2"]
	
	-- CREATE A NEW SYSTEM OF COMMUNICATION AGENTS
	let soca = newSoCA "PORT" stimSet ckaSet constants

	-- LOAD ALL OF THE AGENT SPECIFICATIONS
	agentPC  <- load "specs/PortTerminal/PC.txt"
	agentSMa <- load "specs/PortTerminal/SMa.txt"
	agentSMb <- load "specs/PortTerminal/SMb.txt"
	agentSVa <- load "specs/PortTerminal/SVa.txt"
	agentSVb <- load "specs/PortTerminal/SVb.txt"
	agentTM  <- load "specs/PortTerminal/TM.txt"
	agentCM  <- load "specs/PortTerminal/CM.txt"
	agentCC  <- load "specs/PortTerminal/CC.txt"

	-- ADD ALL OF THE AGENTS TO THE SYSTEM
	soca <- addAgent agentPC soca
	soca <- addAgent agentSMa soca
	soca <- addAgent agentSMb soca
	soca <- addAgent agentSVa soca	
	soca <- addAgent agentSVb soca
	soca <- addAgent agentTM soca
	soca <- addAgent agentCM soca
	soca <- addAgent agentCC soca
	printSoCA soca
	
	-- GENERATE THE MAUDE SPECIFICATIONS FOR EACH AGENT AND THE SYSTEM	
	generateMaudeSpec soca agentPC
	generateMaudeSpec soca agentSMa
	generateMaudeSpec soca agentSMb
	generateMaudeSpec soca agentSVa	
	generateMaudeSpec soca agentSVb
	generateMaudeSpec soca agentTM
	generateMaudeSpec soca agentCM
	generateMaudeSpec soca agentCC
	generateMaudeSOCA soca

	-- COMPUTE SET OF INTENDED INTERACTIONS
	-- 	MANAGE 1
	let matA = mkMatrix portEdgesA
	let xsA  = getAgents portEdgesA
	let intendedA  = intPaths $ mkIntTree xsA matA ((1,"PC"),X)
	-- MANAGE 2
	let matB = mkMatrix portEdgesB
	let xsB  = getAgents portEdgesB
	let intendedB  = intPaths $ mkIntTree xsB matB ((1,"PC"),X)

	let intended = intendedA ++ intendedB

	-- RUN ANALYSIS TO IDENTIFY IMPLICIT INTERACTIONS AND COMPUTE SEVERITY
	ints <- runAnalysis soca intended

	-- RUN ANALYSIS OF EXPLOITABILITY OF IMPLICIT INTERACTIONS
	runExploit soca ints
	putStrLn ""
	
\end{code}

The \codestyle{runExploit} function wraps the entire analysis of the exploitability of implicit interactions.
\begin{code}
runExploit :: SoCA -> Paths -> IO ()
runExploit s []     = putStr ""
runExploit s (p:ps) = do
	putStr   $ "\nImplicit Interaction = "
	printPaths [p]
	putStrLn $ "Exploitability       = " ++ show (exploit s p)
	runExploit s ps
\end{code}


The \codestyle{portEdges} function gives the set of intended interactions as a set of edges from the unrolled message-passing diagram for the port container terminal example. Each node in the unrolled message-passing diagram is enumerated and given a label of the form \codestyle{(Int,AgentName)}. An edge reads as (source, sink, type).
\begin{code}
portEdgesA :: [((Int,AgentName),(Int,AgentName),CommType)]
portEdgesA =
	[
		-- MANAGE 1
		((1,"PC"),(2,"SMa"),B),
		((2,"SMa"),(3,"SVa"),B),
		((1,"PC"),(3,"SVa"),E),
		((3,"SVa"),(4,"TM"),B),
		((4,"TM"),(5,"SVb"),S),
		((4,"TM"),(6,"SVa"),B),
		((1,"PC"),(6,"SVa"),E),
		((6,"SVa"),(7,"SMb"),S),
		((6,"SVa"),(8,"SMa"),B),
		((8,"SMa"),(9,"SVa"),S),
		((8,"SMa"),(10,"SVb"),S),
		((9,"SVa"),(11,"CM"),B),
		((11,"CM"),(12,"CC"),B),
		((12,"CC"),(13,"CM"),B),
		((13,"CM"),(14,"CC"),B),
		((14,"CC"),(15,"CM"),B),
		((15,"CM"),(16,"SVb"),S),
		((15,"CM"),(17,"SVa"),S),
		((17,"SVa"),(18,"TM"),S),
		((17,"SVa"),(19,"SMa"),S),
		((19,"SMa"),(20,"PC"),S)
	]	
	
portEdgesB :: [((Int,AgentName),(Int,AgentName),CommType)]
portEdgesB =
	[	-- MANAGE 2
		((1,"PC"),(2,"SMb"),B),
		((2,"SMb"),(3,"SVb"),B),
		((1,"PC"),(3,"SVb"),E),
		((3,"SVb"),(4,"TM"),B),
		((4,"TM"),(5,"SVa"),S),
		((4,"TM"),(6,"SVb"),B),
		((1,"PC"),(6,"SVb"),E),
		((6,"SVb"),(7,"SMa"),S),
		((6,"SVb"),(8,"SMb"),B),
		((8,"SMb"),(9,"SVb"),S),
		((8,"SMb"),(10,"SVa"),S),
		((9,"SVb"),(11,"CM"),B),
		((11,"CM"),(12,"CC"),B),
		((12,"CC"),(13,"CM"),B),
		((13,"CM"),(14,"CC"),B),
		((14,"CC"),(15,"CM"),B),
		((15,"CM"),(16,"SVa"),S),
		((15,"CM"),(17,"SVb"),S),
		((17,"SVb"),(18,"TM"),S),
		((17,"SVb"),(19,"SMb"),S),
		((19,"SMb"),(20,"PC"),S)
	]		
\end{code}