\subsection{StimulusTypes}

\begin{SourceHeader}
	FileName			& : & 	StimulusTypes.lhs	\\
	Project				& : &   \projectname		\\
	Last Modified Date 	& : & 	July 15, 2014 		\\
	Last Modified By 	& : &	Jason Jaskolka	 	\\
\end{SourceHeader}

\begin{ModuleDescription}
	The \codestyle{StimulusTypes} module contains all of the type definitions for external stimuli. 
\end{ModuleDescription}


\begin{code}
module StimulusTypes ( Stim          ,
                       StimSet       ,
                       StimExpr (..)   ) where
\end{code}


The \codestyle{Set} constructor from the Haskell \codestyle{Data.Set} library is imported to allow for the construction of sets of external stimuli.
\begin{code}
import Data.Set ( Set )
\end{code}


\codestyle{Stim} is a type for an external stimulus. An external stimulus is represented as a \codestyle{String}.
\begin{code}
type Stim = String	
\end{code}


A \codestyle{StimSet} is a type for sets of external stimuli. A \codestyle{StimSet} is represented as a set of \codestyle{Stim}. The set representation follows the mathematical definition of a stimulus structure and ensures that there are no duplicate external stimuli.
\begin{code}
type StimSet = Set Stim	
\end{code}


A \codestyle{StimExpr} is a data type for expressions from a stimulus structure. A \codestyle{StimExpr} is constructed from the deactivation stimulus (\codestyle{D}), the neutral stimulus (\codestyle{N}), stimulus literals, and the a choice and composition operators. This data type construction follows the algebraic signature of a stimulus structure given by~$\STIMstructure$. \codestyle{StimExpr} derives the \codestyle{Eq} class in order to allow for comparisons of \codestyle{StimExpr}s.
\begin{code}
data StimExpr = 
	D                          |
	N                          |
	Literal Stim               |
	Choice  StimExpr StimExpr  | 
	Compose StimExpr StimExpr
		deriving (Eq)		
\end{code}