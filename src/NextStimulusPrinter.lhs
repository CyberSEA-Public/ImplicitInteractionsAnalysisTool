\subsection{NextStimulusPrinter}

\begin{SourceHeader}
	FileName			& : & 	NextStimulusPrinter.lhs		\\
	Project				& : &   \projectname				\\
	Last Modified Date 	& : & 	July 15, 2014 				\\
	Last Modified By 	& : &	Jason Jaskolka	 			\\
\end{SourceHeader}

\begin{ModuleDescription}
	The \codestyle{NextStimulusPrinter} module contains an implementation of a pretty printer for the next stimulus mapping~$\outOp$. This module allows for the display of the next stimulus mapping and attempts to match the notation used in~\cite{Jaskolka2013aa,Jaskolka2014aa}.
\end{ModuleDescription}


\begin{code}
module NextStimulusPrinter ( printNSMap ) where
\end{code}


The \codestyle{NextStimulusTypes} module is imported in order to put the types for the next stimulus mapping in scope so that the mapping may be pretty printed. Various functions from the \codestyle{Text.PrettyPrint.HughesPJ} library are also imported to allow for the definition of the pretty printer. 
\begin{code}
import NextStimulusTypes
	
import Prelude hiding ((<>))	
import Text.PrettyPrint.HughesPJ ( Doc         , 
                                   renderStyle , 
                                   style       , 
                                   text        , 
                                   (<>)        , 
                                   (<+>)         )
								   								   
\end{code}


An instance of the the \codestyle{Show} class is created for \codestyle{NSExpr}s. This instance uses the functions imported from the \codestyle{Text.PrettyPrint.HughesPJ} library defines a pretty printer for elements of the next stimulus mapping so that when printed, they are easy to read.
\begin{code}
instance Show (NSExpr) where
	show e = renderStyle style $ prettyPrint e
\end{code}


\codestyle{nsOp} provides the symbol used for the next stimulus mapping~``$\outOp$''.
\begin{code}
nsOp :: Doc 
nsOp = text "\x03BB"	
\end{code}


\codestyle{prettyPrint} pretty prints elements of the next stimulus mapping.
\begin{code}
prettyPrint :: NSExpr -> Doc
prettyPrint (NS s a t) = nsOp <> text "(" <> stimToDoc s <> text "," <> ckaToDoc a <> text ")" <+>  text "=" <+> stimToDoc t
	where
		stimToDoc = text . show . parseStimExpr
		ckaToDoc  = text . show . parseCKAExpr
\end{code}


\codestyle{printNSMap} pretty prints a next stimulus mapping.
\begin{code}
printNSMap :: NSMap -> IO ()
printNSMap []     = return ()
printNSMap (m:ms) = (putStrLn $ "\t" ++ show m) >> printNSMap ms
\end{code}