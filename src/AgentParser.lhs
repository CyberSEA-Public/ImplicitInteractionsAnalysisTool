\subsection{AgentParser}

\begin{SourceHeader}
	FileName			& : & 	AgentParser.lhs		\\
	Project				& : &   \projectname		\\
	Last Modified Date 	& : & 	July 30, 2014 		\\
	Last Modified By 	& : &	Jason Jaskolka	 	\\
\end{SourceHeader}

\begin{ModuleDescription}
	The \codestyle{AgentParser} module contains an implementation of a parser for agent specification files. This module allows for the parsing of \codestyle{String} representations of agent specification files of the form shown in Figure~\ref{fig:agent_spec} to \codestyle{Agent} representations.
\begin{figure}
\begin{center}
	\begin{minipage}{2in}
		\begin{verbatim}
			begin AGENT where

			    AgentName := CKAExpr

			end

			begin NEXT_BEHAVIOUR where

			    (Stim,CKA) = CKA

			end

			begin NEXT_STIMULUS where

			    (Stim,CKA) = Stim

			end

			begin CONCRETE_BEHAVIOUR where

			    CKA => [ Stmt ]

			end
		\end{verbatim}
	\end{minipage}
\end{center}
\label{fig:agent_spec}
\caption{Form of an agent specification file.}
\end{figure}	
\end{ModuleDescription}


\begin{code}
module AgentParser ( parseAgent ) where
\end{code}


The \codestyle{AgentTypes} module is imported in order to put the types for agents in scope so that the specifications may be parsed. The \codestyle{exprParser} functions from the \codestyle{Behaviour}, \codestyle{NextStimulus}, and \codestyle{NextBehaviour} modules are imported along with the \codestyle{specParser} function from the \codestyle{LevelThreeSpec} module for use with the various parts of an agent specification. The \codestyle{(<*)} function from the Haskell \codestyle{Control.Applicative} library is imported to sequence actions while discarding the value of the second argument. Lastly, various functions required for the parsing functionality from the Haskell \codestyle{Text.Parsec} libraries are imported. 
\begin{code}
import AgentTypes          ( AgentSpec (..) , 
                             Agent (..)       )
import Behaviour           ( exprParser )
import NextStimulus        ( exprParser )
import NextBehaviour       ( exprParser )
import LevelThreeSpec      ( specParser )

import Control.Applicative  ( (<*) )

import Text.Parsec          ( alphaNum ,
                              eof      ,
                              letter   ,
                              parse    ,
                              oneOf    ,
                              many1      )
import Text.Parsec.Language ( emptyDef )
import Text.Parsec.String   ( Parser )							  
import Text.Parsec.Token    ( GenLanguageDef (..) ,
                              GenTokenParser (..) ,
                              makeTokenParser       )
\end{code}


\codestyle{grammar} provides the \codestyle{Agent} specification language grammar.
\begin{code}
grammar = emptyDef{ identStart      = letter                            ,
                    identLetter     = alphaNum                          ,
                    opStart         = oneOf ":"                         ,
                    opLetter        = oneOf "="                         ,
                    reservedOpNames = [":="]                            ,
                    reservedNames   = ["begin", "end", "where", "AGENT" , 
                                       "NEXT_BEHAVIOUR", "NEXT_STIMULUS", 
                                       "CONCRETE_BEHAVIOUR"              ] }		
\end{code}


A \codestyle{TokenParser} is constructed for the \codestyle{Agent} specification language grammar.
\begin{code}
TokenParser{ parens     = m_parens     , 
             identifier = m_identifier , 
             reservedOp = m_reservedOp ,
             reserved   = m_reserved   ,
             semiSep1   = m_semiSep1   ,
             whiteSpace = m_whiteSpace   } = makeTokenParser grammar
\end{code}


\codestyle{agentParser} provides the parser to parse \codestyle{Agent}s and is used as a part of the main parser.
\begin{code}	
agentParser :: Parser Agent
agentParser = do 
	m_reserved "begin"
	m_reserved "AGENT"
	m_reserved "where"
	name  <- m_identifier
	m_reservedOp ":="
	term  <- Behaviour.exprParser
	m_reserved "end"
	m_reserved "begin"
	m_reserved "NEXT_BEHAVIOUR"
	m_reserved "where"
	nbmap <- many1 NextBehaviour.exprParser
	m_reserved "end"
	m_reserved "begin"
	m_reserved "NEXT_STIMULUS"
	m_reserved "where"
	nsmap <- many1 NextStimulus.exprParser
	m_reserved "end"
	m_reserved "begin"
	m_reserved "CONCRETE_BEHAVIOUR"
	m_reserved "where"
	specs <- many1 specParser
	m_reserved "end"
	return $ Agent name (AgentSpec term nbmap nsmap specs)
\end{code}


\codestyle{mainParser} is the main parsing function for \codestyle{Agent}s. It ignores whitespace and ends when the ``end of file'' (\codestyle{eof}) token of the input string is reached.
\begin{code}	
mainParser :: Parser Agent
mainParser = m_whiteSpace >> agentParser <* eof
\end{code}


\codestyle{parseAgent} translates a \codestyle{String} representation an agent specification file into the \codestyle{Agent} data type representation.
\begin{code}	
parseAgent :: String -> Agent
parseAgent input = case parse mainParser "" input of
	Left  err -> error $ show err
	Right ans -> ans   		  
\end{code}