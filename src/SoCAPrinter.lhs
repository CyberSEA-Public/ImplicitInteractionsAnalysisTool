\subsection{SoCAPrinter}

\begin{SourceHeader}
	FileName			& : & 	SoCAPrinter.lhs		\\
	Project				& : &   \projectname		\\
	Last Modified Date 	& : & 	July 30, 2014 		\\
	Last Modified By 	& : &	Jason Jaskolka	 	\\
\end{SourceHeader}

\begin{ModuleDescription}
	The \codestyle{SoCAPrinter} module contains an implementation of a print function for \socas.
\end{ModuleDescription}


\begin{code}
module SoCAPrinter ( printSoCA ,
                     printSet    ) where
\end{code}


The \codestyle{SoCA} and \codestyle{Agent} types are imported respectively from the \codestyle{SoCATypes} and \codestyle{Agent} modules so that they may be used in the printing of a \soca. Additionally, various types and functions are imported from the Haskell \codestyle{Data.Set} library.
\begin{code}
import SoCATypes       ( SoCA (..) )
import Agent           ( Agent (..) )

import Data.Set as Set ( Set    ,  
                         map    ,   
                         toList   )
\end{code}



\codestyle{printSoCA} provides the printing functionality for a given \soca. The function prints and formats the name, stimulus set, behaviour set, constants set, and list of agents for the given \soca.
\begin{code}
printSoCA :: SoCA -> IO ()
printSoCA soca = do
	putStr   $ "SoCA            : "
	putStrLn $ ident soca
	putStr   $ "External Stimuli: "
	printSet $ stim soca
	putStr   $ "Behaviours      : "  
	printSet $ cka soca
	putStr   $ "Named Constants : "  
	printSet $ consts soca
	putStr   $ "Agents          : "
	printSet $ Set.map name $ agents soca
\end{code}


\codestyle{printSet} provides the printing functionality for a \codestyle{Set} representation of \codestyle{String}s.
\begin{code}
printSet :: Set String -> IO ()
printSet s = putStr "{" >> (printElems $ toList s) >> putStrLn "}"
	where
		printElems :: [String] -> IO ()
		printElems []     = putStr ""
		printElems [e]    = putStr $ removeQid e
		printElems (e:es) = (putStr $ removeQid e ++ ", ") >> printElems es
		removeQid  :: String -> String
		removeQid x = case x of
			"D"       -> "\x1D589"
			"N"       -> "\120211"
			otherwise -> filter (/= '\'') x
\end{code}
