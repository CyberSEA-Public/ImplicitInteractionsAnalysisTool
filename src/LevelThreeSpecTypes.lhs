\subsection{LevelThreeSpecTypes}

\begin{SourceHeader}
	FileName			& : & 	LevelThreeSpecTypes.lhs	\\
	Project				& : &   \projectname			\\
	Last Modified Date 	& : & 	July 16, 2014 			\\
	Last Modified By 	& : &	Jason Jaskolka	 		\\
\end{SourceHeader}

\begin{ModuleDescription}
	The \codestyle{LevelThreeSpecTypes} module contains all of the type definitions for the \levelThree of agent behaviours. We use Dijkstra's guarded commands~\cite{Dijkstra1975aa} as our concrete specification language.
\end{ModuleDescription}


\begin{code}
module LevelThreeSpecTypes ( VarName    ,
                             Constants  ,
                             Expr (..)  ,
                             Cond (..)  ,
                             Guard (..) ,
                             Stmt (..)  ,                             
                             Spec (..)  ,
                             CBSpec       ) where
\end{code}


The \codestyle{Set} constructor from the Haskell \codestyle{Data.Set} library is imported to allow for the construction of sets of named constants in the \levelThree of agent behaviours.
\begin{code}
import Data.Set (Set)	
\end{code}


\codestyle{BehaviourName} is a name given to an agent behaviour. An agent behaviour name is represented as a \codestyle{String}.
\begin{code}
type BehaviourName = String
\end{code}


\codestyle{VarName} is a name given to a variable in the \levelThree of an agent behaviour. A variable name is represented as a \codestyle{String}.
\begin{code}
type VarName = String
\end{code}


\codestyle{Constant} is a name given to a non-numeric constant value in the \levelThree of an agent behaviour.
\begin{code}
type Constant  = String
\end{code}


\codestyle{Constants} is a set of \codestyle{Constant}s representing the names of the non-numeric constant values in the \levelThree of agent behaviours.
\begin{code}
type Constants = Set Constant
\end{code}


An \codestyle{Expr} is a data type for expressions which may be assigned to a variable in the \levelThree of agent behaviours. An \codestyle{Expr} may be a \codestyle{Constant} value or as an arithmetic expression.
\begin{code}
data Expr = Val Constant   | 
            Add Expr Expr  |
            Sub Expr Expr  |
            Mul Expr Expr  |
            Div Expr Expr  
\end{code}


A \codestyle{Cond} is a data type for the condition which may be used in a guard of the \levelThree of an agent behaviour. A \codestyle{Cond} may be a constant value represented as a string or as an arithmetic expression. A \codestyle{Cond} is constructed from the defined constants \emph{true} (\codestyle{T}) and \emph{false} (\codestyle{F}) and named constants and additionally has operations for negation, conjunction, and disjunction.
\begin{code}
data Cond = T              |
            F              |  
            Con Constant   |
            Neg Cond       |
            And Cond Cond  |
            Or  Cond Cond  |
            Eq  Cond Cond  |
            Gt Cond Cond   |
            Lt Cond Cond   |
            Geq Cond Cond  |
            Leq Cond Cond  
\end{code}


A \codestyle{Guard} is a data type for the guarded command conditional guards that are used in the \levelThree of an agent behaviour. A single \codestyle{Guard} is constructed using the \codestyle{G} constructor. A \codestyle{Guard} may also consist of many alternatives. This is constructed as a list of guards.
\begin{code}
data Guard = G Cond Stmt  |
             Alt [Guard]
\end{code}


A \codestyle{Stmt} is a data type for the guarded command statements in the \levelThree of an agent behaviour. A \codestyle{Stmt} is constructed from a do anything command (\codestyle{Abort}), a do nothing command (\codestyle{Skip}), an assignment of an \codestyle{Expr} to a \codestyle{VarName}, the sequential composition of statements, conditional statements, and repetition (loop) statements, as well as send and receive primitives.
\begin{code}
data Stmt = Abort           |
            Skip            |
            VarName := Expr | 
            Seq [Stmt]      |
            If Guard        |
            Do Guard        |
            Send Expr       |
            Receive Expr  			
\end{code}


A \codestyle{Spec} is a data type for the guarded command statement associated with an agent behaviour name the \levelThree of an agent behaviour. A \codestyle{Spec} is represented as an assignment of a \codestyle{Stmt} to a \codestyle{BehaviourName}.
\begin{code}
data Spec = BehaviourName ::= Stmt
\end{code}


A \codestyle{CBSpec} is type for representing the \levelThree of an agent. A \codestyle{CBSpec} is a list of \codestyle{Spec}s.
\begin{code}
type CBSpec = [Spec]
\end{code}