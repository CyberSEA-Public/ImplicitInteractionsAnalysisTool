\subsection{SoCA}

\begin{SourceHeader}
	FileName			& : & 	SoCA.lhs			\\
	Project				& : &   \projectname		\\
	Last Modified Date 	& : & 	June 26, 2014 		\\
	Last Modified By 	& : &	Jason Jaskolka	 	\\
\end{SourceHeader}

\begin{ModuleDescription}
	The \codestyle{SoCA} module is a high-level module for \socas and provides an interface for the \codestyle{SoCATypes}, \codestyle{SoCAPrinter}, and \codestyle{SoCAParser} modules.
\end{ModuleDescription}

\begin{code}
module SoCA ( newSoCA            ,
              setStimSet         , 
              setCKASet          , 
              setConstants       ,  
              addAgent           , 
              lookupAgent        , 
              module SoCATypes   , 
              module SoCAPrinter ,
              module SoCAParser    ) where
\end{code}


The \codestyle{SoCATypes}, \codestyle{SoCAPrinter}, and \codestyle{SoCAParser} modules are imported so that they may be in scope in this module and so that they can be exported as part of the interface of this module. 
Various types and functions from the \codestyle{Agent}, \codestyle{LevelThreeSpec}, \codestyle{Stimulus}, and \codestyle{Behaviour} modules are also imported for use with \socas. Additionally a number of types and functions are imported from the Haskell \codestyle{Data.Set}, \codestyle{Data.List}, and \codestyle{Data.Text} libraries.
\begin{code}
import SoCATypes	
import SoCAPrinter
import SoCAParser

import Agent          ( AgentName , 
                        Agent (..) )
import LevelThreeSpec ( Constants )
import Stimulus       ( Stim          ,
                        StimSet       , 
                        stimForMaude  ,
                        parseStimExpr   )
import Behaviour      ( CKA          ,
                        CKASet       , 
                        ckaForMaude  ,
                        parseCKAExpr   )

import Data.Set       ( Set      , 
                        toList   , 
                        fromList , 
                        empty    , 
                        insert     )
import Data.List      ( find )
import Data.Text      ( unpack )
\end{code}


\codestyle{newSoCA} creates a new \soca from a given name, stimulus set, behaviour set, and set of constants. The set of agents is initially empty.
\begin{code}
newSoCA :: SoCAName -> StimSet -> CKASet -> Constants -> SoCA
newSoCA n s b c = SoCA n s b c empty
\end{code}


\codestyle{setStimSet} converts a list of basic external stimuli to a \codestyle{StimSet}. This set is also prepared for use with \maude by adding quoted identifiers to each basic external stimulus.\\
\begin{code}
setStimSet :: [Stim] -> StimSet
setStimSet = fromList . map (unpack . stimForMaude . parseStimExpr)
\end{code}


\codestyle{setCKASet} converts a list of basic agent behaviours to a \codestyle{CKASet}. This set is also prepared for use with \maude by adding quoted identifiers to each basic agent behaviour.\\
\begin{code}
setCKASet :: [CKA] -> CKASet
setCKASet = fromList . map (unpack . ckaForMaude . parseCKAExpr)
\end{code}


\codestyle{setConstants} converts a list of named constants to a set of \codestyle{Constants}.
\begin{code}
setConstants :: [String] -> Constants
setConstants cs = fromList cs 	
\end{code}


\codestyle{addAgent} inserts the given \codestyle{Agent} into the given \codestyle{SoCA}. 
\begin{code}
addAgent :: Agent -> SoCA -> IO SoCA
addAgent a soca = return soca{ agents = a `insert` (agents soca) }
\end{code}


\codestyle{lookupAgent} attempts to find the given \codestyle{AgentName} in the given \codestyle{SoCA}. If the given \codestyle{AgentName} is in the set of agents for the given \codestyle{SoCA}, then the corresponding \codestyle{Agent} is returned, otherwise an error is displayed.
\begin{code}
lookupAgent :: AgentName -> SoCA -> Agent
lookupAgent n soca = case find (\a -> name a == n) $ toList $ agents soca of
	Just a -> a
	Nothing -> error $ "Agent " ++ n ++ " does not exist in the given SoCA!"
\end{code}