\subsection{Test}

\begin{SourceHeader}
	FileName			& : & 	Test.lhs			\\
	Project				& : &   \projectname		\\
	Last Modified Date 	& : & 	January 16, 2020 	\\
	Last Modified By 	& : &	Jason Jaskolka	 	\\
\end{SourceHeader}

\begin{ModuleDescription}
	Provides a platform for testing the implementation of the \projectname with support for implicit interactions analysis.
\end{ModuleDescription}


\begin{code}
module Main where
\end{code}


\begin{code}
import ChemicalReactor
import ManufacturingCell
import PortTerminal
\end{code}

\codestyle{main} provides the means for calling all of the individual test functions for the implemented examples.
\begin{code}
main :: IO ()
main = manufacturingExample
	-- chemicalExample
	-- portExample
	-- manufacturingSimulation
\end{code}