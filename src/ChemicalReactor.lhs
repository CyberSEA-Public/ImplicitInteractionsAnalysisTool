\subsection{ChemicalReactor}

\begin{SourceHeader}
	FileName			& : & 	ChemicalReactor.lhs	\\
	Project				& : &   \projectname		\\
	Last Modified Date 	& : & 	July 26, 2016 		\\
	Last Modified By 	& : &	Jason Jaskolka	 	\\
\end{SourceHeader}

\begin{ModuleDescription}
	Provides a platform for testing the implementation of the \projectname with support for implicit interactions analysis.
\end{ModuleDescription}


\begin{code}
module ChemicalReactor ( chemicalExample ) where
\end{code}

A number of types and functions are imported from various modules in order to run the analysis to identify implicit interactions in the batch chemical reactor example.
\begin{code}
import Agent          ( AgentName ,
                        load      ,
						expr, spec  )
import Analysis       ( runAnalysis )
import CommPaths      ( CommType (..) , 
                        Paths,
						printPaths     )						
import Intended       ( getAgents , 
                        mkIntTree , 
                        mkMatrix  , 
                        intPaths    )
import MaudeInterface ( generateMaudeSpec ,
                        generateMaudeSOCA   )
import SoCA           ( SoCA         ,
                        addAgent     ,
                        newSoCA      , 
                        printSoCA    , 
                        setStimSet   , 
                        setCKASet    ,
                        setConstants   )

import Orbits	
-- For testing Influencing Stimuli
import InfluencingStimuli	
import SoCAPrinter							
\end{code}

\codestyle{chemicalExample} runs an analysis on the batch chemical reactor example to identify the implicit interactions that may exist.
\begin{code}
chemicalExample :: IO ()
chemicalExample = do

	-- ESTABLISH SETS OF BASIC STIMULI AND BEHAVIORS
	let stimSet = setStimSet ["start", "charge", "charged", "heat", "heated", "purgeN", "purgeH", "purged", "catalyze", "catalyzed", "wash", "washed", "raise", "raised", "vent", "vented", "transfer", "transferred", "end", "D", "N"]
	let ckaSet = setCKASet ["rIDLE", "rINIT", "rHEAT1", "rHEAT2", "rPRGE1", "rPRGE2", "rPRGE3", "rPRES1", "rPRES2", "rVENT", "rTRNS", "sIDLE", "sCHRG", "sCTLZ", "sWAIT", "sTRNS", "mON", "mOFF", "tHEAT", "tCOOL", "tOFF", "pOPEN", "pCLSD", "nOPEN", "nCLSD", "hOPEN", "hCLSD", "aOPEN", "aCLSD", "aON", "aOFF", "bOPEN", "bCLSD", "bON", "bOFF", "cOPEN", "cCLSD", "cON", "cOFF", "oOPEN", "oCLSD", "oON", "oOFF", "0", "1"]
	let constants = setConstants ["TRUE", "FALSE", "constAmtA", "constAmtB", "constAmtC", "constWashAmtA", "constWashAmtB"]
	
	-- CREATE A NEW SYSTEM OF COMMUNICATION AGENTS
	let soca = newSoCA "CHEMICAL" stimSet ckaSet constants	

	-- LOAD ALL OF THE AGENT SPECIFICATIONS
	agentA <- load "specs/ChemicalReactor/A.txt"
	agentB <- load "specs/ChemicalReactor/B.txt"
	agentC <- load "specs/ChemicalReactor/C.txt"
	agentO <- load "specs/ChemicalReactor/O.txt"
	agentH <- load "specs/ChemicalReactor/H.txt"
	agentN <- load "specs/ChemicalReactor/N.txt"
	agentT <- load "specs/ChemicalReactor/T.txt"
	agentP <- load "specs/ChemicalReactor/P.txt"
	agentM <- load "specs/ChemicalReactor/M.txt"
	agentS <- load "specs/ChemicalReactor/S.txt"
	agentR <- load "specs/ChemicalReactor/R.txt"

	-- ADD ALL OF THE AGENTS TO THE SYSTEM
	soca <- addAgent agentA soca
	soca <- addAgent agentB soca
	soca <- addAgent agentC soca
	soca <- addAgent agentO soca
	soca <- addAgent agentH soca
	soca <- addAgent agentN soca
	soca <- addAgent agentT soca
	soca <- addAgent agentP soca
	soca <- addAgent agentM soca
	soca <- addAgent agentS soca
	soca <- addAgent agentR soca
	printSoCA soca
	
	-- GENERATE THE MAUDE SPECIFICATIONS FOR EACH AGENT AND THE SYSTEM	
	generateMaudeSpec soca agentA
	generateMaudeSpec soca agentB
	generateMaudeSpec soca agentC
	generateMaudeSpec soca agentO
	generateMaudeSpec soca agentH
	generateMaudeSpec soca agentN
	generateMaudeSpec soca agentT
	generateMaudeSpec soca agentP
	generateMaudeSpec soca agentM
	generateMaudeSpec soca agentS
	generateMaudeSpec soca agentR
	generateMaudeSOCA soca
	
	-- COMPUTE SET OF INTENDED INTERACTIONS
	let mat = mkMatrix bcrEdges
	let xs  = getAgents bcrEdges
	let intended  = intPaths $ mkIntTree xs mat ((1,"S"),X)

	-- RUN ANALYSIS TO IDENTIFY IMPLICIT INTERACTIONS AND COMPUTE SEVERITY
	ints <- runAnalysis soca intended

	-- RUN ANALYSIS OF EXPLOITABILITY OF IMPLICIT INTERACTIONS
	runExploit soca ints
	putStrLn ""

\end{code}


The \codestyle{runExploit} function wraps the entire analysis of the exploitability of implicit interactions.
\begin{code}
runExploit :: SoCA -> Paths -> IO ()
runExploit s []     = putStr ""
runExploit s (p:ps) = do
	putStr   $ "\nImplicit Interaction = "
	printPaths [p]
	putStr $ "Attack = "
	printSet $ attack s p	
	putStrLn $ "Exploitability       = " ++ show (exploit s p)
	runExploit s ps
\end{code}

The \codestyle{bcrEdges} function gives the set of intended interactions as a set of edges from the unrolled message-passing diagram for the batch chemical reactor example. Each node in the unrolled message-passing diagram is enumerated and given a label of the form \codestyle{(Int,AgentName)}. An edge reads as (source, sink, type).
\begin{code}
bcrEdges :: [((Int,AgentName),(Int,AgentName),CommType)]
bcrEdges =
	[
		(( 1,"S"),( 2,"A"),S),
		(( 1,"S"),( 3,"M"),S),
		(( 1,"S"),( 4,"B"),S),
		(( 2,"A"),( 5,"B"),S),
		(( 2,"A"),( 6,"S"),S),
		(( 2,"A"),( 8,"R"),S),
		(( 4,"B"),( 6,"S"),S),
		(( 4,"B"),( 7,"A"),S),
		(( 4,"B"),( 8,"R"),S),
		(( 8,"R"),( 9,"M"),S),
		(( 8,"R"),(10,"T"),S),
		(( 8,"R"),(11,"N"),S),
		((10,"T"),(12,"R"),S),
		((12,"R"),(13,"N"),S),
		((12,"R"),(14,"P"),S),
		((12,"R"),(15,"M"),S),
		((13,"N"),(16,"R"),S),
		((13,"N"),(17,"S"),S),
		((13,"N"),(18,"P"),S),
		((17,"S"),(19,"N"),S),
		((17,"S"),(20,"C"),S),
		((17,"S"),(21,"M"),S),
		((20,"C"),(22,"A"),S),
		((20,"C"),(23,"B"),S),
		((22,"A"),(24,"B"),S),
		((22,"A"),(25,"C"),S),
		((23,"B"),(26,"A"),S),
		((23,"B"),(25,"C"),S),
		((25,"C"),(27,"S"),S),
		((25,"C"),(28,"R"),S),
		((28,"R"),(29,"P"),S),
		((28,"R"),(30,"H"),B),
		((30,"H"),(31,"P"),S),
		((30,"H"),(32,"R"),S),
		((32,"R"),(33,"H"),B),
		((32,"R"),(34,"T"),B),
		((33,"H"),(35,"R"),S),
		((35,"R"),(36,"H"),B),
		((35,"R"),(37,"T"),B),
		((36,"H"),(38,"R"),S),
		((38,"R"),(39,"M"),S),
		((38,"R"),(40,"H"),S),
		((38,"R"),(41,"P"),B),
		((41,"P"),(42,"R"),S),
		((42,"R"),(43,"P"),S),
		((42,"R"),(44,"N"),B),
		((42,"R"),(45,"M"),S),
		((44,"N"),(46,"P"),S),
		((44,"N"),(47,"R"),S),
		((47,"R"),(48,"T"),B),
		((47,"R"),(49,"M"),S),
		((47,"R"),(50,"N"),S),
		((48,"T"),(51,"R"),S),
		((48,"T"),(52,"S"),S),
		((51,"R"),(53,"N"),E),
		((51,"R"),(56,"O"),E),
		((52,"S"),(53,"N"),S),
		((52,"S"),(54,"T"),S),
		((52,"S"),(55,"M"),S),
		((52,"S"),(56,"O"),S),
		((56,"O"),(57,"N"),S),
		((56,"O"),(58,"S"),S),
		((56,"O"),(59,"R"),S),
		((58,"S"),(60,"O"),S),
		((59,"R"),(60,"O"),S)
	]	
\end{code}