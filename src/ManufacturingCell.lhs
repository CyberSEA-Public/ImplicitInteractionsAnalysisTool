\subsection{ManufacturingCell}

\begin{SourceHeader}
	FileName			& : & 	ManufacturingCell.lhs	\\
	Project				& : &   \projectname			\\
	Last Modified Date 	& : & 	July 26, 2016 			\\
	Last Modified By 	& : &	Jason Jaskolka	 		\\
\end{SourceHeader}

\begin{ModuleDescription}
	Provides a platform for testing the implementation of the \projectname with support for implicit interactions analysis.
\end{ModuleDescription}


\begin{code}
module ManufacturingCell ( manufacturingExample, manufacturingSimulation ) where
\end{code}

A number of types and functions are imported from various modules in order to run the analysis to identify implicit interactions in the manufacturing cell example.
\begin{code}
import Agent          ( AgentName ,
                        load      ,
						expr, spec  )
import Analysis       ( runAnalysis )
import CommPaths      ( CommType (..) , 
                        Paths,
						printPaths     )						
import Intended       ( getAgents , 
                        mkIntTree , 
                        mkMatrix  , 
                        intPaths    )
import MaudeInterface ( generateMaudeSpec ,
                        generateMaudeSOCA   )
import SoCA           ( SoCA         ,
                        addAgent     ,
                        newSoCA      , 
                        printSoCA    , 
                        setStimSet   , 
                        setCKASet    ,
                        setConstants   )

-- For testing Influencing Stimuli
import InfluencingStimuli
import SoCAPrinter	

-- For SIMULATION
import Simulation
import Data.Text as Text ( Text, concat, pack, unpack )
import Data.Set ( member, toList )
import Data.List ( elemIndices, take )
import Behaviour
import Stimulus	
import Debug.Trace
\end{code}

\codestyle{manufacturingExample} runs an analysis on the manufacturing cell example to identify the implicit interactions that may exist.
\begin{code}
manufacturingExample :: IO ()
manufacturingExample = do
	
	-- ESTABLISH SETS OF BASIC STIMULI AND BEHAVIORS
	let stimSet = setStimSet ["start", "load", "loaded", "prepare", "done", "setup", "unload", "unloaded", "ready", "process", "processed", "end", "D", "N"]
	let ckaSet = setCKASet ["IDLE", "LOAD", "PREP", "INIT", "PROC", "EMPTY", "FULL", "WAIT", "MOVE", "STBY", "SET", "WORK", "0", "1"]	
	let constants = setConstants ["PROCESS", "NULL"]
	
	-- CREATE A NEW SYSTEM OF COMMUNICATION AGENTS
	let soca = newSoCA "MANUFACTURING" stimSet ckaSet constants
	
	-- LOAD ALL OF THE AGENT SPECIFICATIONS
	agentC <- load "specs/ManufacturingCell/C.txt"
	agentS <- load "specs/ManufacturingCell/S.txt"
	agentH <- load "specs/ManufacturingCell/H.txt"
	agentP <- load "specs/ManufacturingCell/P.txt"
	
	-- ADD ALL OF THE AGENTS TO THE SYSTEM
	soca <- addAgent agentC soca
	soca <- addAgent agentS soca
	soca <- addAgent agentH soca
	soca <- addAgent agentP soca	
	printSoCA soca
	
	-- GENERATE THE MAUDE SPECIFICATIONS FOR EACH AGENT AND THE SYSTEM
	generateMaudeSpec soca agentC
	generateMaudeSpec soca agentS
	generateMaudeSpec soca agentH
	generateMaudeSpec soca agentP	
	generateMaudeSOCA soca
	
	-- COMPUTE SET OF INTENDED INTERACTIONS
	let adjMatrix = mkMatrix mcEdges
	let agents    = getAgents mcEdges
	let intended  = intPaths $ mkIntTree agents adjMatrix ((1,"C"),X)

	-- RUN ANALYSIS TO IDENTIFY IMPLICIT INTERACTIONS AND COMPUTE SEVERITY
	ints <- runAnalysis soca intended

	-- RUN ANALYSIS OF EXPLOITABILITY OF IMPLICIT INTERACTIONS
	runExploit soca ints
	putStrLn ""

\end{code}


\codestyle{manufacturingSimulation} runs a simulation of the manufacturing cell example.
\begin{code}
manufacturingSimulation :: IO ()
manufacturingSimulation = do
	
	-- ESTABLISH SETS OF BASIC STIMULI AND BEHAVIORS
	let stimSet = setStimSet ["start", "load", "loaded", "prepare", "done", "setup", "unload", "unloaded", "ready", "process", "processed", "end", "D", "N"]
	let ckaSet = setCKASet ["IDLE", "LOAD", "PREP", "INIT", "PROC", "EMPTY", "FULL", "WAIT", "MOVE", "STBY", "SET", "WORK", "0", "1"]	
	let constants = setConstants ["PROCESS", "NULL"]
	
	-- CREATE A NEW SYSTEM OF COMMUNICATION AGENTS
	let soca = newSoCA "MANUFACTURING" stimSet ckaSet constants
	
	-- LOAD ALL OF THE AGENT SPECIFICATIONS
	agentC <- load "specs/ManufacturingCell/C.txt"
	agentS <- load "specs/ManufacturingCell/S.txt"
	agentH <- load "specs/ManufacturingCell/H.txt"
	agentP <- load "specs/ManufacturingCell/P.txt"
	
	-- ADD ALL OF THE AGENTS TO THE SYSTEM
	soca <- addAgent agentC soca
	soca <- addAgent agentS soca
	soca <- addAgent agentH soca
	soca <- addAgent agentP soca	
	printSoCA soca
	
	-- GENERATE THE MAUDE SPECIFICATIONS FOR EACH AGENT AND THE SYSTEM
	generateMaudeSpec soca agentC
	generateMaudeSpec soca agentS
	generateMaudeSpec soca agentH
	generateMaudeSpec soca agentP	
	generateMaudeSOCA soca
	
	-- COMPUTE SET OF INTENDED INTERACTIONS
	let adjMatrix = mkMatrix mcEdges
	let agents    = getAgents mcEdges
	let intended  = intPaths $ mkIntTree agents adjMatrix ((1,"C"),X)

	-- SIMULATIONS AND VERIFICATION AND VALIDATION OF PROPERTIES
	putStrLn "Simulation with initial configuration:  (start, IDLE * WAIT * STBY * EMPTY) = "
	print $ simulate "start" "IDLE * WAIT * STBY * EMPTY"

	putStrLn "Requirement 1 satisfied for configuration (start, IDLE * WAIT * STBY * EMPTY) = "
	print $ checkReq1 $ simulate "start" "IDLE * WAIT * STBY * EMPTY"

	putStrLn "Requirement 2 satisfied for configuration (start, IDLE * WAIT * STBY * EMPTY) = "
	print $ checkReq2 $ simulate "start" "IDLE * WAIT * STBY * EMPTY"
	putStrLn ""

	putStrLn "Simulation with initial configuration: (load ++ unload, LOAD * STBY * WAIT * (EMPTY + FULL)) = "
	print $ simulate "load ++ unload" "LOAD * STBY * WAIT * (EMPTY + FULL)"

	putStrLn "Requirement 1 satisfied for configuration (load ++ unload, LOAD * STBY * WAIT * (EMPTY + FULL)) = "
	print $ checkReq1 $ simulate "load ⊕ unload" "LOAD * STBY * WAIT * (EMPTY + FULL)"

	putStrLn "Requirement 2 satisfied for configuration (load ++ unload, LOAD * STBY * WAIT * (EMPTY + FULL)) = "
	print $ checkReq2 $ simulate "load ⊕ unload" "LOAD * STBY * WAIT * (EMPTY + FULL)"

\end{code}

The \codestyle{mcEdges} function gives the set of intended interactions as a set of edges from the unrolled message-passing diagram for the manufacturing cell example. Each node in the unrolled message-passing diagram is enumerated and given a label of the form \codestyle{(Int,AgentName)}. An edge reads as (source, sink, type).
\begin{code}
mcEdges :: [((Int,AgentName),(Int,AgentName),CommType)]
mcEdges =
	[
		((1,"C"),(2,"S"),S),
		((2,"S"),(3,"C"),S),
		((2,"S"),(4,"H"),E),
		((3,"C"),(4,"H"),S),
		((4,"H"),(5,"S"),S),
		((4,"H"),(7,"P"),E),
		((5,"S"),(6,"C"),S),
		((5,"S"),(7,"P"),E),
		((6,"C"),(7,"P"),B),
		((7,"P"),(8,"H"),S),
		((8,"H"),(9,"P"),S),
		((8,"H"),(11,"C"),S),
		((9,"P"),(10,"C"),S),
		((11,"C"),(12,"P"),S)
	]	
\end{code}

The \codestyle{runExploit} function wraps the entire analysis of the exploitability of implicit interactions.
\begin{code}
runExploit :: SoCA -> Paths -> IO ()
runExploit s []     = putStr ""
runExploit s (p:ps) = do
	putStr   $ "\nImplicit Interaction = "
	printPaths [p]
	putStr $ "Attack = "
	printSet $ attack s p	
	putStrLn $ "Exploitability       = " ++ show (exploit s p)
	runExploit s ps
\end{code}

The \codestyle{checkReq1} function checks whether the requirement is satisfied for the traces generated from a simulation. This function codes the example requirement that states that the processing agent must not be working while the handling agent is moving.
\begin{code}
checkReq1 :: [[(StimExpr,CKAExpr)]] -> Bool
checkReq1 []     = True
checkReq1 (t:ts) = checkTraceReq1 t && checkReq1 ts
	
checkTraceReq1 :: [(StimExpr,CKAExpr)] -> Bool
checkTraceReq1 []         = True
checkTraceReq1 ((s,a):ks) = (not ("WORK" `member` as && "MOVE" `member` as)) && (checkTraceReq1 ks)
	where 
		as = extractCKA a
\end{code}

The \codestyle{checkReq2} function checks whether the requirement is satisfied for the traces generated from a simulation. This function codes the example requirement that states that the system must not issue a process event before it has received a ready event.
\begin{code}
checkReq2 :: [[(StimExpr,CKAExpr)]] -> Bool
checkReq2 []     = True
checkReq2 (t:ts) = checkTraceReq2 t && checkReq2 ts
	
checkTraceReq2 :: [(StimExpr,CKAExpr)] -> Bool
checkTraceReq2 [] = True
checkTraceReq2 ks = case (r,p) of 
	([],[])   -> True
	(_,[])    -> True
	([],_)    -> False
	otherwise -> (any (\x -> x < (minimum p)) r)
	where 
		ss = map fst ks
		r = elemIndices (Stimulus.Literal "ready") ss
		p = elemIndices (Stimulus.Literal "process") ss
\end{code}


