\subsection{CommPaths}

\begin{SourceHeader}
	FileName			& : & 	CommPaths.lhs		\\
	Project				& : &   \projectname		\\
	Last Modified Date 	& : & 	July 19, 2016 		\\
	Last Modified By 	& : &	Jason Jaskolka	 	\\
\end{SourceHeader}

\begin{ModuleDescription}
	The \codestyle{CommPaths} module provides the types and functions for working with potential communication paths for \socas.
\end{ModuleDescription}


\begin{code}
module CommPaths ( AgentPair     ,
                   CommType (..) ,
                   DirectComm    ,
                   Path          ,
                   Paths         ,
                   paths         ,
                   mkTree        ,
                   printPaths    ,
                   printPathswithSeverity ,
                   writePaths   ) where
\end{code}

The \codestyle{AgentName} type from the \codestyle{Agent} module is imported so that it may be in scope for this module. It is required to build edges in a communication path. A number of additional library functions are imported.
\begin{code}
import Agent       ( AgentName )

import Data.List   ( elemIndex , 
                     notElem   , 
                     filter      )
import Data.Matrix ( Matrix , 
                     getCol   )
import Data.Maybe  ( fromJust )
import Data.Tree   ( Tree (..) )
import Data.Vector ( findIndices , 
                     toList        )
import Text.Printf ( printf )
import GHC.IO.Handle
import System.IO
\end{code}


An \codestyle{AgentPair} is a type representing a pair of agent names. This type is used to specify an edge of a communication path between two agents.
\begin{code}
type AgentPair = (AgentName, AgentName)	
\end{code}


A \codestyle{CommType} is a data type for the type of communication means between two agents. The \codestyle{CommType} is either via external stimuli (\codestyle{S}) or via shared environments (\codestyle{E}). It can also be \codestyle{B} (both \codestyle{S} and \codestyle{E}) and \codestyle{X} (no edge). These additional types are added simply to reduce the number of total edges produced in the communication graph. \codestyle{CommType} derives the \codestyle{Eq}, \codestyle{Ord}, and \codestyle{Show} classes in order to allow for comparisons and printing of \codestyle{CommType}s.
\begin{code}
data CommType = S | E | B | X
	deriving (Eq, Ord, Show)		
\end{code}


A \codestyle{DirectComm} is a type representing a direct communication between a pair of agents along with the type of the communication. 
\begin{code}
type DirectComm = (AgentPair, CommType)	
\end{code}


A \codestyle{Path} is a type for a communication path. A communication path consists of a list of \codestyle{(AgentName, CommType)} pairs.
\begin{code}
type Path  = [(AgentName, CommType)]	
\end{code}

\codestyle{Paths} is a type for a list of communication paths. This type gives the ability to represent more than one communication path between two given agents. 
\begin{code}
type Paths = [Path]
\end{code}

\codestyle{paths} is a function that all possible communication paths between two specified agents given a Tree representation of the reachability from a source agent to a sink agent. Since the tree is built from bottom-up, we need to reverse all of the tree paths to get the actual paths from source to sink. \codestyle{allPaths} and \codestyle{treeFold} are helper functions to walk the tree in a depth-first fashion.
\begin{code}
paths :: Tree a -> [[a]]
paths = map reverse . allPaths

allPaths :: Tree a -> [[a]]
allPaths t = treeFold (\a fs p ->
	let p' = p ++ [a] in
		if null fs then [p'] else concatMap ($ p') fs) t []

treeFold :: (a -> [b] -> b) -> Tree a -> b
treeFold func (Node header list) = func header (map (treeFold func) list)
\end{code}


\codestyle{printPaths} is a function that prints the given list of possible communication paths.
\begin{code}
printPaths :: Paths -> IO ()
printPaths []     = return ()
printPaths (p:ps) = putStr "\t" >> printPath p >> printPaths ps		
	where
		printPath :: Path -> IO () 	
		printPath []     = putStrLn ""
		printPath [s]    = putStrLn $ fst s
		printPath (s:ss) = (putStr $ fst s ++ "  ->" ++ (show $ snd s) ++ "  ") >> printPath ss
\end{code}

\codestyle{writePaths} is a function that writes the given list of possible communication paths to a given location in the disk. The communication paths are written in the reverse order.
\begin{code}
writePaths :: Paths -> Handle -> IO ()
writePaths [] h = return ()
writePaths (p:ps) h = do
        hPutStrLn h $ toString p
        writePaths ps h
\end{code}

\codestyle{toString} is a function which gives the string representation of a path
\begin{code}
toString :: Path -> String
toString p = do
        let res = []
        let aux p h = case p of
                [] -> h
                [s] -> (fst s) ++ " " ++ h
                s:q -> aux q $ (show $ snd s) ++ " " ++ (fst s) ++ " " ++ h
        aux p res
\end{code}


\codestyle{printPaths} is a function that prints the given list of possible communication paths along with their severity measure.
\begin{code}
printPathswithSeverity :: [(Path,Float)] -> IO ()
printPathswithSeverity []     = return ()
printPathswithSeverity (p:ps) = putStr "\t" >> putStr "SEVERITY = " >> printf "%.2f\t\t" (snd p) >> printPath (fst p) >> putStrLn "" >> printPathswithSeverity ps		
	where
		printPath :: Path -> IO () 	
		printPath []     = putStr ""
		printPath [s]    = putStr $ fst s
		printPath (s:ss) = (putStr $ fst s ++ "  ->" ++ (show $ snd s) ++ "  ") >> printPath ss		
\end{code}


\codestyle{mkTree} builds a reachability tree from source to sink given an adjacency matrix representation of the communication graph. The root of the tree is the sink and it is built in a bottom-up fashion.
\begin{code}
mkTree :: [AgentName] -> Matrix CommType -> [AgentName] -> AgentName -> (AgentName,CommType) -> Tree (AgentName,CommType)
mkTree as adjMat visited source sink
	| fst sink == source = Node sink []
	| otherwise          = Node sink (map (mkTree as adjMat vst source) snk)
		where
			vst = (fst sink) : visited
			snk = filter (\(a,_) -> a `notElem` vst) (reachedFrom (fst sink) as adjMat)
\end{code}


\codestyle{reachedFrom} builds a list of all of the agents and the type of their communication from which the given agent can be reached. This is computed using the adjacency matrix representation of the communication graph.
\begin{code}
reachedFrom :: AgentName -> [AgentName] -> Matrix CommType -> [(AgentName,CommType)]
reachedFrom x xs adj = (zip ss stype) ++ (zip es etype) ++ (zip bs bEtype) ++ (zip bs bStype)
	where
		snk    = fromJust $ elemIndex x xs
		col    = getCol (snk+1) adj
		ss     = map (xs !!) $ toList $ findIndices (== S) col
		stype  = replicate (length ss) S
		es     = map (xs !!) $ toList $ findIndices (== E) col
		etype  = replicate (length es) E
		bs     = map (xs !!) $ toList $ findIndices (== B) col
		bEtype = replicate (length bs) E
		bStype = replicate (length bs) S
\end{code}