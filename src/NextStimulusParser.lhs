\subsection{NextStimulusParser}

\begin{SourceHeader}
	FileName			& : & 	NextStimulusParser.lhs	\\
	Project				& : &   \projectname			\\
	Last Modified Date 	& : & 	July 16, 2014 			\\
	Last Modified By 	& : &	Jason Jaskolka	 		\\
\end{SourceHeader}

\begin{ModuleDescription}
	The \codestyle{NextStimulusParser} module contains an implementation of a parser for next stimulus mappings. This module allows for the parsing of \codestyle{String} representations of next stimulus mappings from a \rightSemimodule{\cka}~$\OutSemimodule$ to \codestyle{NSMap} representations.
\end{ModuleDescription}


\begin{code}
module NextStimulusParser ( exprParser , 
                            parseNSMap   ) where
\end{code}


The \codestyle{NextStimulusTypes} module is imported in order to put the types for next stimulus mappings in scope so that the expressions and mappings may be parsed. The \codestyle{(<*)} function from the Haskell \codestyle{Control.Applicative} library is imported to sequence actions while discarding the value of the second argument. Lastly, various functions required for the parsing functionality from the Haskell \codestyle{Text.Parsec} libraries are imported. 
\begin{code}
import NextStimulusTypes

import Control.Applicative  ( (<*) )

import Text.Parsec          ( alphaNum ,
                              eof      ,
                              letter   ,
                              char     ,
                              parse    ,
                              oneOf    ,
                              (<|>)      )
import Text.Parsec.Language ( emptyDef )
import Text.Parsec.String   ( Parser )							  
import Text.Parsec.Token    ( GenLanguageDef (..) ,
                              GenTokenParser (..) ,
                              makeTokenParser       )
\end{code}


\codestyle{grammar} provides the \codestyle{NSExpr} language grammar.
\begin{code}
grammar = emptyDef{ identStart      = letter                             ,
                    identLetter     = alphaNum <|> char ' ' <|> char '*' ,
                    opStart         = oneOf "(),="                       ,
                    opLetter        = oneOf "(),="                       ,
                    reservedOpNames = ["(",")",",","=",".", "*", "+"]    ,
                    reservedNames   = []                                   }					  
\end{code}


A \codestyle{TokenParser} is constructed for the \codestyle{NSExpr} language grammar.
\begin{code}
TokenParser{ parens     = m_parens     , 
             identifier = m_identifier , 
             reservedOp = m_reservedOp ,
             reserved   = m_reserved   ,
             semiSep1   = m_semiSep1   ,
             whiteSpace = m_whiteSpace   } = makeTokenParser grammar
\end{code}			 


\codestyle{exprParser} provides the parser to parse \codestyle{NSExpr}s and is used as a part of the main parser.
\begin{code}	
exprParser :: Parser NSExpr
exprParser = do 
	m_reservedOp "("
	s <- m_identifier
	m_reservedOp ","
	a <- m_identifier
	m_reservedOp ")"
	m_reservedOp "="
	b <- m_identifier
	return $ NS s a b
\end{code}


\codestyle{mainParser} is the main parsing function for \codestyle{NSExpr}s. It ignores whitespace and ends when the ``end of file'' (\codestyle{eof}) token of the input string is reached.
\begin{code}	
mainParser :: Parser NSExpr
mainParser = m_whiteSpace >> exprParser <* eof
\end{code}


\codestyle{parseNSExpr} translates a \codestyle{String} representation of an element of a next stimulus mapping into the \codestyle{NSExpr} data type representation.
\begin{code}	
parseNSExpr :: String -> NSExpr
parseNSExpr input = case parse mainParser "" input of
	Left  err -> error $ show err
	Right ans -> ans   		  
\end{code}


\codestyle{parseNSMap} translates a list of \codestyle{String} representations of elements of a next stimulus mapping into the \codestyle{NSMap} type representation.
\begin{code}	
parseNSMap :: [String] -> NSMap
parseNSMap = map parseNSExpr
\end{code}