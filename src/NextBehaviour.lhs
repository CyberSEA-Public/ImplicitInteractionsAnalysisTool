\subsection{NextBehaviour}

\begin{SourceHeader}
	FileName			& : & 	NextBehaviour.lhs	\\
	Project				& : &   \projectname		\\
	Last Modified Date 	& : & 	July 15, 2014 		\\
	Last Modified By 	& : &	Jason Jaskolka	 	\\
\end{SourceHeader}

\begin{ModuleDescription}
	The \codestyle{NextBehaviour} module is a high-level module for next behaviour mappings and provides an interface for the \codestyle{NextBehaviourTypes}, \codestyle{NextBehaviourPrinter}, and \codestyle{NextBehaviourParser} modules.
\end{ModuleDescription}


\begin{code}
module NextBehaviour ( nbForMaude                  ,  
                       module NextBehaviourTypes   , 
                       module NextBehaviourPrinter , 
                       module NextBehaviourParser    ) where
\end{code}


The \codestyle{NextBehaviourTypes}, \codestyle{NextBehaviourPrinter}, and \codestyle{NextBehaviourParser} modules are imported so that they may be in scope in this module and so that they can be exported as part of the interface of this module. A number of functions from the Haskell \codestyle{Data.Text} library are imported for use in the functions implemented in this module.
\begin{code}
import NextBehaviourTypes
import NextBehaviourPrinter
import NextBehaviourParser

import Data.Text as Text ( Text ,  
                           pack   )
\end{code}


\codestyle{nbForMaude} translates a given \codestyle{NBExpr} to a \codestyle{Text} representation which is suitable for use with \maude.
\begin{code}
nbForMaude :: NBExpr -> Text
nbForMaude (NB s a b) 
	| b == "0" || b == "1" = pack $ "NB('" ++ s ++ ",'" ++ a ++ ") = "  ++ b 
	| otherwise            = pack $ "NB('" ++ s ++ ",'" ++ a ++ ") = '" ++ b
\end{code}