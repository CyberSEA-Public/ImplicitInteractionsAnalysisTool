\subsection{LevelThreeSpec}

\begin{SourceHeader}
	FileName			& : & 	LevelThreeSpec.lhs	\\
	Project				& : &   \projectname		\\
	Last Modified Date 	& : & 	July 16, 2014 		\\
	Last Modified By 	& : &	Jason Jaskolka	 	\\
\end{SourceHeader}

\begin{ModuleDescription}
	The \codestyle{LevelThreeSpec} module is a high-level module for the \levelThree of agents and provides an interface for the \codestyle{LevelThreeSpecTypes}, \codestyle{LevelThreeSpecPrinter}, and \codestyle{LevelThreeSpecParser} modules.
\end{ModuleDescription}


\begin{code}
module LevelThreeSpec ( getName                      , 
                        getSpec                      , 
                        getDefs                      , 
                        getRefs                      , 
                        module LevelThreeSpecTypes   , 
                        module LevelThreeSpecPrinter , 
                        module LevelThreeSpecParser    ) where
\end{code}


The \codestyle{LevelThreeSpecTypes}, \codestyle{LevelThreeSpecPrinter}, and \codestyle{LevelThreeSpecParser} modules are imported so that they may be in scope in this module and so that they can be exported as part of the interface of this module. A number of functions from the Haskell \codestyle{Data.Set} library are imported for use in the functions implemented in this module.
\begin{code}
import LevelThreeSpecTypes
import LevelThreeSpecPrinter
import LevelThreeSpecParser	

import Data.Set ( fromList , 
                  member   , 
                  union      )
\end{code}


\codestyle{getName} returns the name of the agent behaviour being specified in the \levelThree.
\begin{code}
getName :: Spec -> String	
getName (n ::= s) = n 
\end{code}


\codestyle{getSpec} returns the specification statement of the agent behaviour being specified in the \levelThree.
\begin{code}
getSpec :: Spec -> Stmt
getSpec (n ::= s) = s 
\end{code}


\codestyle{getGuardDefs} returns a list of the defined variables in a given \levelThree guard (\codestyle{Guard}).
\begin{code}
getGuardDefs :: Guard -> [VarName]
getGuardDefs (G c s)      = getDefs s
getGuardDefs (Alt [])     = []
getGuardDefs (Alt (g:gs)) = getGuardDefs g ++ getGuardDefs (Alt gs)
\end{code}


\codestyle{getDefs} returns a list of the defined variables in a given \levelThree statement (\codestyle{Stmt}).
\begin{code}
getDefs :: Stmt -> [VarName]
getDefs (Abort)      = []
getDefs (Skip)       = []
getDefs (v := e)     = [v]
getDefs (Seq [])     = []
getDefs (Seq (s:ss)) = getDefs s ++ getDefs (Seq ss)
getDefs (If g)       = getGuardDefs g
getDefs (Do g)       = getGuardDefs g
getDefs (Send e)     = []
getDefs (Receive e)  = []
\end{code}


\codestyle{getExprRefs} returns a list of the referenced variables in a given \levelThree expression (\codestyle{Expr}). This function uses the set of constants provided to remove any constants in the \levelThree guard from the list of referenced variables.
\begin{code}
getExprRefs :: Expr -> Constants -> [VarName]
getExprRefs (Val v)   cs = if isConstant v cs then [] else [v]
getExprRefs (Add a b) cs = getExprRefs a cs ++ getExprRefs b cs
getExprRefs (Sub a b) cs = getExprRefs a cs ++ getExprRefs b cs
getExprRefs (Mul a b) cs = getExprRefs a cs ++ getExprRefs b cs
getExprRefs (Div a b) cs = getExprRefs a cs ++ getExprRefs b cs
\end{code}


\codestyle{getCondRefs} returns a list of the referenced variables in a given \levelThree guard condition (\codestyle{Cond}). This function uses the set of constants provided to remove any constants in the \levelThree guard condition from the list of referenced variables.
\begin{code}
getCondRefs :: Cond -> Constants -> [VarName]
getCondRefs (T)       cs = []
getCondRefs (F)       cs = []
getCondRefs (Con c)   cs = if isConstant c cs then [] else [c]
getCondRefs (Neg a)   cs = getCondRefs a cs
getCondRefs (And a b) cs = getCondRefs a cs ++ getCondRefs b cs
getCondRefs (Or  a b) cs = getCondRefs a cs ++ getCondRefs b cs
getCondRefs (Eq  a b) cs = getCondRefs a cs ++ getCondRefs b cs
getCondRefs (Gt  a b) cs = getCondRefs a cs ++ getCondRefs b cs
getCondRefs (Lt  a b) cs = getCondRefs a cs ++ getCondRefs b cs
getCondRefs (Geq a b) cs = getCondRefs a cs ++ getCondRefs b cs
getCondRefs (Leq a b) cs = getCondRefs a cs ++ getCondRefs b cs
\end{code}


\codestyle{getGuardRefs} returns a list of the referenced variables in a given \levelThree guard (\codestyle{Guard}). This function uses the set of constants provided to remove any constants in the \levelThree guard from the list of referenced variables.
\begin{code}
getGuardRefs :: Guard -> Constants -> [VarName]
getGuardRefs (G c s)      cs = getCondRefs c cs ++ getRefs s cs
getGuardRefs (Alt [])     _  = []
getGuardRefs (Alt (g:gs)) cs = getGuardRefs g cs ++ getGuardRefs (Alt gs) cs
\end{code}


\codestyle{getRefs} returns a list of the referenced variables in a given \levelThree statement (\codestyle{Stmt}). This function uses the set of constants provided to remove any constants in the \levelThree statement from the list of referenced variables.
\begin{code}
getRefs :: Stmt -> Constants -> [VarName]
getRefs (Abort)      _  = []
getRefs (Skip)       _  = []
getRefs (v := e)     cs = getExprRefs e cs
getRefs (Seq [])     _  = []
getRefs (Seq (s:ss)) cs = getRefs (Seq ss) (updateConstants s cs) ++ getRefs s cs
getRefs (If g)       cs = getGuardRefs g cs
getRefs (Do g)       cs = getGuardRefs g cs
getRefs (Send e)     cs = []
getRefs (Receive v)  cs = []
\end{code}

\begin{code}
updateConstants :: Stmt -> Constants -> Constants
updateConstants (Abort)      cs = cs
updateConstants (Skip)       cs = cs
updateConstants (v := e)     cs = cs
updateConstants (Seq [])     cs = cs
updateConstants (Seq (s:ss)) cs = cs
updateConstants (If g)       cs = cs
updateConstants (Do g)       cs = cs
updateConstants (Send e)     cs = cs
updateConstants (Receive v)  cs = cs `union` (fromList $ getExprRefs v cs)	
\end{code}


\codestyle{isConstant} determines if a given \levelThree expression value is a constant numeric value or a pre-defined (non-numeric) constant. This function uses the set of constants to check whether a given value is a pre-defined constant. The predefined set of constants implicitly includes the values ``true'' and ``false''.
\begin{code} 
isConstant :: String -> Constants -> Bool
isConstant s cs = s `member` (cs `union` fromList ["true", "false"]) || isInteger s || isDouble s
\end{code}

\codestyle{isInteger} determines if a given \levelThree expression value is an integer constant.
\begin{code}
isInteger :: String -> Bool
isInteger s = case reads s :: [(Integer, String)] of
	[(_, "")] -> True
	_         -> False
\end{code}


\codestyle{isInteger} determines if a given \levelThree expression value is a double or floating point constant.
\begin{code}
isDouble :: String -> Bool
isDouble s = case reads s :: [(Double, String)] of
	[(_, "")] -> True
	_         -> False
\end{code}