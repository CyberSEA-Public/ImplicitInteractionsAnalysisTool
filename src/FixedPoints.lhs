\subsection{FixedPoints}

\begin{SourceHeader}
	FileName			& : & 	FixedPoints.lhs		\\
	Project				& : &   \projectname		\\
	Last Modified Date 	& : & 	July 29, 2014 		\\
	Last Modified By 	& : &	Jason Jaskolka	 	\\
\end{SourceHeader}

\begin{ModuleDescription}
	The \codestyle{FixedPoints} module provides the functionality to determine whether an agent behaviours or external stimulus is a fixed point with respect to the next behaviour mapping~$\actOp$ and next stimulus mapping~$\outOp$, respectively. 
\end{ModuleDescription}


\begin{code}
module FixedPoints ( isFixedCKA     , 
                     isFixedSTIM    ,
                     ckaFixedPoint  , 
                     stimFixedPoint   ) where
\end{code}


We import a number of types and functions from the \codestyle{Behaviour} and \codestyle{Stimulus} modules so that they may be in scope in this module. Additionally, the \codestyle{maude} function from the \codestyle{MaudeInterface} module is imported to allow for calls to \maude. Lastly, we import the the required functions from the Haskell \codestyle{Data.Set} and \codestyle{Data.Text} libraries.
\begin{code}
import Behaviour      ( CKA           , 
                        CKASet        ,
                        parseCKAExpr  ,
                        ckaForMaude     )
import Stimulus       ( Stim          , 
                        StimSet       ,
                        parseStimExpr ,
                        stimForMaude    )

import MaudeInterface ( maude )

import Data.List      ( (\\) )    
import Data.Set       ( toList )
import Data.Text      ( unpack )
\end{code}


\codestyle{isFixedCKA} decides whether a given external stimulus and agent behaviour results in a fixed behaviour with respect to the next behaviour mapping~$\actOp$.
\begin{code}	
isFixedCKA :: Stim -> CKA -> Bool	
isFixedCKA s a = (maude term) == (maude e)
	where
		term = "NB((" ++ s ++ "),(" ++ e ++"))"
		e = unpack $ ckaForMaude $ parseCKAExpr a
\end{code}


\codestyle{isFixedSTIM} decides whether a given external stimulus and agent behaviour results in a fixed behaviour with respect to the next stimulus mapping~$\outOp$.
\begin{code}
isFixedSTIM :: Stim -> CKA ->  Bool	
isFixedSTIM s a = (maude term) == (maude e)
	where
		term = "NS((" ++ e ++ "),(" ++ a ++"))"
		e = unpack $ stimForMaude $ parseStimExpr s
\end{code}


\codestyle{ckaFixedPoint} tests whether a given agent behaviour is a fixed point with respect to the next behaviour mapping~$\actOp$. An element~$a \in \CKAset$ is called a \emph{fixed point} if~$\lnotation{\forall}{s}{s \in \STIMset \STdiff \set{\Dstim}}{\lAct{a}{s} = a}$.
\begin{code}
ckaFixedPoint :: StimSet -> CKA -> Bool
ckaFixedPoint ss a = all (\x -> isFixedCKA x a) ((toList ss) \\ ["D"])
\end{code}


\codestyle{stimFixedPoint} tests whether a given external stimulus is a fixed point with respect to the next stimulus mapping~$\outOp$. An element~$s \in \STIMset$ is called a \emph{fixed point} if~$\lnotation{\forall}{a}{a \in \CKAset \STdiff \set{0}}{\lOut{a}{s} = s}$.
\begin{code}
stimFixedPoint :: CKASet -> Stim -> Bool
stimFixedPoint as s = all (\x -> isFixedSTIM s x) ((toList as) \\ ["0"])
\end{code}