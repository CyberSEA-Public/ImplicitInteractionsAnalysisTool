\subsection{MaudeInterface}

\begin{SourceHeader}
	FileName			& : & 	MaudeInterface.lhs	\\
	Project				& : &   \projectname		\\
	Last Modified Date 	& : & 	January 16, 2020 	\\
	Last Modified By 	& : &	Jason Jaskolka	 	\\
\end{SourceHeader}

\begin{ModuleDescription}
	The \codestyle{MaudeInterface} module is a provides the interface to \maude. This module allows for calls to be made to \maude and has functionality to parse the results for use with other modules in the \projectname.
\end{ModuleDescription}


\begin{code}
module MaudeInterface ( maude                  ,
                        generateMaudeSpec      , 
                        generateMaudeSOCA        ) where
\end{code}


This module imports an extensive selection of types and functions from a number of additional modules from within the \projectname as well as from standard Haskell libraries. Each of these types and functions are required for the calls to \maude, parsing the results and generating the specification files for agents and \socas.
\begin{code}
import Language.Maude.Exec ( MaudeConf (..)           ,
                             MaudeCommand ( Rewrite ) ,
                             runMaude                 ,  
                             maudeStdout                )
import Data.Text as Text   ( Text         , 
                             filter       , 
                             unpack       , 
                             pack         , 
                             strip        , 
                             lines        , 
                             words        , 
                             replace      , 
                             splitOn      , 
                             dropWhileEnd   )
import System.IO.Unsafe    ( unsafePerformIO )
import Data.Char           ( toUpper , 
                             toLower   )
import Data.Set            ( toList )

import Behaviour           ( CKA          ,
                             CKAExpr      , 
                             extractCKA   , 
                             parseCKAExpr   )
import Stimulus            ( Stim          ,
                             StimExpr      , 
                             StimSet       , 
                             parseStimExpr   )
import NextBehaviour       ( NBMap      , 
                             nbForMaude   )
import NextStimulus        ( NSMap      , 
                             nsForMaude   )
import Agent               ( AgentName      , 
                             AgentSpec (..) , 
                             Agent (..)       )
import SoCA                ( SoCAName  , 
                             SoCA (..)   )
\end{code}


\codestyle{maude} is the main interface function to \maude. It makes the call to \maude and returns the result as a string representation of the term.
\begin{code}
maude :: String -> String
maude = either show show . chooseTermType . unsafePerformIO . maudeRewrite . pack
\end{code}


\codestyle{maudeRewrite} performs a rewriting task by making a call to \maude. \codestyle{Text} represents a Maude term. This function returns a pair containing \codestyle{String} representations of the resulting rewritten term and its type.
\begin{code}
maudeRewrite :: Text -> IO (String,String)
maudeRewrite term = do
	-- rewrite <- runMaude chemicalConf $ Rewrite term
	rewrite <- runMaude manufacturingConf $ Rewrite term
	-- rewrite <- runMaude portConf $ Rewrite term
	let result = maudeStdout rewrite
	let (x,y)  = ( unpack $ cleanResultTerm $ extractResultTerm result , 
	               unpack $ extractResultType result                     )
	return (x,y)	
\end{code}


The following function is required to point to the correct file path to \maude. 
\begin{code}							
manufacturingConf :: MaudeConf
manufacturingConf = MaudeConf { maudeCmd  = "maude/maude-core/macos/maude.darwin64" ,
                                loadFiles = ["maude/manufacturing/Manufacturing.maude"]  }

								
portConf :: MaudeConf
portConf = MaudeConf { maudeCmd  = "maude/maude-core/macos/maude.darwin64" ,
                       loadFiles = ["maude/port/Port.maude"]     }	 

chemicalConf :: MaudeConf
chemicalConf = MaudeConf { maudeCmd  = "maude/maude-core/macos/maude.darwin64" ,
                           loadFiles = ["maude/chemical/Chemical.maude"]     }	
						   
-- FOR LINUX USAGE						   
-- manufacturingConf :: MaudeConf
-- manufacturingConf = MaudeConf { maudeCmd  = "maude/maude-core/linux/maude.linux64" ,
--                                 loadFiles = ["maude/manufacturing/Manufacturing.maude"]  }

-- portConf :: MaudeConf
-- portConf = MaudeConf { maudeCmd  = "maude/maude-core/linux/maude.linux64" ,
--                        loadFiles = ["maude/port/Port.maude"]     }

-- chemicalConf :: MaudeConf
-- chemicalConf = MaudeConf { maudeCmd  = "maude/maude-core/linux/maude.linux64" ,
--                            loadFiles = ["maude/chemical/Chemical.maude"]     }		   								
\end{code}


\codestyle{extractResultTerm} extracts the resulting term from the rewrite result from the call to \maude.
\begin{code}
extractResultTerm :: Text -> Text
extractResultTerm t = strip $ (splitOn (pack ":") ((Text.lines t) !! 1)) !! 1
\end{code}

\codestyle{cleanResultTerm} filters out any sort of artefacts from \maude after the result term has been extracted. These artefacts include quoted identifiers and types specifications.
\begin{code}
cleanResultTerm :: Text -> Text 
cleanResultTerm t = replace (pack "(0).Behaviour") (pack "0") $ replace (pack "(1).Behaviour") (pack "1") $ removeQid t
\end{code}


\codestyle{extractResultType} extracts the resulting type from the rewrite result from the call to \maude.
\begin{code}
extractResultType :: Text -> Text
extractResultType t = dropWhileEnd (==':') ((Text.words $ (Text.lines t) !! 1) !! 1)
\end{code}


\codestyle{chooseTermType} make s selection of the appropriate parser for a rewrite result from \maude based on the result type.
\begin{code}	
chooseTermType :: (String, String) -> Either StimExpr CKAExpr
chooseTermType (r,t) = case t of
		"Behaviour" -> Right $ parseCKAExpr  r
		"Stimulus"  -> Left  $ parseStimExpr r
		"Qid"       -> Right $ parseCKAExpr  r
		otherwise   -> error "Unknown Result Type"
\end{code}


\codestyle{removeQid} strips the quoted identifier from terms coming from \maude before parsing.
\begin{code}	
removeQid :: Text -> Text
removeQid = Text.filter (/= '\'')
\end{code}


\codestyle{generateMaudeSpec} generates the specification file for \maude for a given agent with respect to a given \soca.
\begin{code}
generateMaudeSpec :: SoCA -> Agent -> IO ()	
generateMaudeSpec soca a = do
	let fp = filename (ident soca) (name a)
	writeMaudeLoadC2KA fp
	writeMaudeAgentSpecPreamble fp (name a)
	writeMaudeNBMapEquations fp $ nbmap $ spec a
	writeMaudeNSMapEquations fp $ nsmap $ spec a
	writeMaudeAgentSpecPostamble fp	
\end{code}


\codestyle{generateMaudeSOCA} generates the specification file for \maude for a given \soca.
\begin{code}
generateMaudeSOCA :: SoCA -> IO ()
generateMaudeSOCA soca = do
	let name = ident soca
	let fp = filenameSoCA name
	let as = toList $ agents soca
	writeMaudeLoadC2KA fp
	writeMaudeSoCASpecIncludes fp as
	writeMaudeASoCASpecPreamble fp soca
	writeMaudeASoCASpecPostamble fp
	writeMaudeSoCAModuleStart fp soca
	writeSoCAModuleAgents fp as
	writeMaudeSoCAModuleEnd fp soca	
	appendFile fp $ "\n\nset print with parentheses on ."
\end{code}

	
\codestyle{dir} specifies the directory for \maude files as ``\texttt{maude/}''.
\begin{code}
dir :: FilePath
dir = "maude/"
\end{code}


\codestyle{ext} specifies the file extension for \maude files as ``\texttt{.maude}''.
\begin{code}
ext :: FilePath
ext = ".maude"
\end{code}


\codestyle{filename} generates the \codestyle{FilePath} for the given \codestyle{AgentName} with respect to the given \codestyle{SoCAName}. This function generates a \codestyle{FilePath} assuming that the specification of the \soca and its agents for \maude are given in a directory with the same name as the \codestyle{SoCA} formatted in lowercase.
\begin{code}
filename :: SoCAName -> AgentName -> FilePath
filename s n = dir ++ (map toLower s) ++ "/Agent" ++ n ++ ext 
\end{code}


\codestyle{filenameSoCA} generates the \codestyle{FilePath} for the given \codestyle{SoCAName}. This function generates a \codestyle{FilePath} assuming that the specification of the \soca and its agents for \maude are given in a directory with the same name as the \codestyle{SoCA} formatted in lowercase.
\begin{code}
filenameSoCA :: SoCAName -> FilePath
filenameSoCA n = dir ++ (map toLower n) ++ "/" ++ n ++ ext 
\end{code}


\codestyle{writeMaudeLoadC2KA} writes the load directive for the \CCKAabbrv specification for \maude in the specified file.
\begin{code}
writeMaudeLoadC2KA :: FilePath -> IO ()
writeMaudeLoadC2KA fp = writeFile fp "load ../c2ka-core/C2KA.maude\n"
\end{code}


\codestyle{writeMaudeAgentSpecPreamble} writes the agent specification preamble for \maude in the specified file.
\begin{code}	
writeMaudeAgentSpecPreamble :: FilePath -> String -> IO ()
writeMaudeAgentSpecPreamble fp a = appendFile fp ("\nfmod AGENT" ++ a ++ "_SPEC is\n\tprotecting C2KA .\n")
\end{code}


\codestyle{writeMaudeNBMapEquations} writes the next behaviour mapping equations for \maude in the specified file.
\begin{code}	
writeMaudeNBMapEquations :: FilePath -> NBMap -> IO ()
writeMaudeNBMapEquations fp [] = return ()
writeMaudeNBMapEquations fp (e:es) = appendFile fp ("\teq  " ++  (unpack $ nbForMaude e) ++ " .\n") >> writeMaudeNBMapEquations fp es
\end{code}


\codestyle{writeMaudeNSMapEquations} writes the next stimulus mapping equations for \maude in the specified file.
\begin{code}
writeMaudeNSMapEquations :: FilePath -> NSMap -> IO ()
writeMaudeNSMapEquations fp [] = return ()
writeMaudeNSMapEquations fp (e:es) = appendFile fp ("\teq  " ++ (unpack $ nsForMaude e) ++ " .\n") >> writeMaudeNSMapEquations fp es	
\end{code}


\codestyle{noStimulusInitiators} writes the equations for \maude in the specified file to specify that there ought to be no stimulus initiators. This function extracts all of the basic behaviours for a given agent and writes the corresponding equations for those behaviours.
\begin{code}
noStimulusInitiators :: FilePath -> CKAExpr -> IO ()
noStimulusInitiators fp expr = appendFile fp ("\n\t*** (no stimulus initiators)\n") >> writeNoStimInit fp (toList $ extractCKA expr)
	where
		writeNoStimInit :: FilePath -> [CKA] -> IO ()
		writeNoStimInit fp [] = return ()
		writeNoStimInit fp (e:es) = appendFile fp ("\teq  " ++  "NS(N,'" ++ e ++ ") = N .\n") >> writeNoStimInit fp es		
\end{code}


\codestyle{writeMaudeAgentSpecPostamble} writes the agent specification post-amble for \maude in the specified file.
\begin{code}
writeMaudeAgentSpecPostamble :: FilePath -> IO ()
writeMaudeAgentSpecPostamble fp = appendFile fp "endfm"
\end{code}


\codestyle{writeMaudeSoCASpecIncludes} writes the load directives for the agent specifications for \maude in the specified file for a \soca.
\begin{code}
writeMaudeSoCASpecIncludes :: FilePath -> [Agent] -> IO ()
writeMaudeSoCASpecIncludes fp [] = return ()
writeMaudeSoCASpecIncludes fp (a:as) = (appendFile fp $ "load " ++ "/Agent" ++ name a ++ ext ++ "\n") >> writeMaudeSoCASpecIncludes fp as
\end{code}


\codestyle{writeMaudeASoCASpecPreamble} writes the \soca specification preamble for \maude in the specified file.
\begin{code}
writeMaudeASoCASpecPreamble :: FilePath -> SoCA -> IO ()
writeMaudeASoCASpecPreamble fp s = appendFile fp ("\nfmod " ++ (map toUpper $ ident s)  ++ "_SPEC is\n\tprotecting C2KA .\n")	
\end{code}


\codestyle{withoutReactivation} writes the equations for \maude in the specified file to specify that the system ought to be without reactivation. This function takes the set of all basic stimuli for a given system and writes the corresponding equations for those stimuli.
\begin{code}
withoutReactivation :: FilePath -> StimSet -> IO ()	
withoutReactivation fp stim = appendFile fp ("\n\t*** (without reactivation)\n") >> writeNoReactivation fp (toList stim)
	where
		writeNoReactivation :: FilePath -> [Stim] -> IO ()
		writeNoReactivation fp [] = return ()
		writeNoReactivation fp (s:ss) = appendFile fp ("\teq  " ++  "NB(" ++ s ++ ",1) = 1 .\n") >> writeNoReactivation fp ss	
\end{code}


\codestyle{writeMaudeASoCASpecPostamble} writes the \soca specification post-amble for \maude in the specified file.
\begin{code}
writeMaudeASoCASpecPostamble :: FilePath -> IO ()
writeMaudeASoCASpecPostamble fp = appendFile fp "endfm\n\n"	
\end{code}


\codestyle{writeMaudeSoCAModuleStart} writes the start of the \soca specification module for \maude in the specified file.
\begin{code}
writeMaudeSoCAModuleStart :: FilePath -> SoCA -> IO ()
writeMaudeSoCAModuleStart fp s = appendFile fp $ "mod " ++ (map toUpper $ ident s) ++ " is\n\tincluding C2KA .\n"
\end{code}


\codestyle{writeSoCAModuleAgents} writes the agent module inclusions of the \soca specification module for \maude in the specified file. This function write an inclusion for each agent in the given list of agents.
\begin{code}
writeSoCAModuleAgents :: FilePath -> [Agent] -> IO () 
writeSoCAModuleAgents fp [] = return ()
writeSoCAModuleAgents fp (a:as) = (appendFile fp $ "\tincluding AGENT" ++ name a ++ "_SPEC .\n") >> writeSoCAModuleAgents fp as
\end{code}


\codestyle{writeMaudeSoCAModuleEnd} writes the end of the \soca specification module for \maude in the specified file.
\begin{code}
writeMaudeSoCAModuleEnd :: FilePath -> SoCA -> IO ()
writeMaudeSoCAModuleEnd fp s = appendFile fp $ "\tincluding " ++ (map toUpper $ ident s) ++ "_SPEC .\nendm"
\end{code}