\subsection{SoCATypes}

\begin{SourceHeader}
	FileName			& : & 	SoCATypes.lhs		\\
	Project				& : &   \projectname		\\
	Last Modified Date 	& : & 	July 30, 2014 		\\
	Last Modified By 	& : &	Jason Jaskolka	 	\\
\end{SourceHeader}

\begin{ModuleDescription}
	The \codestyle{SoCATypes} module contains all of the type definitions for \socas. 
\end{ModuleDescription}


\begin{code}
module SoCATypes ( SoCAName , 
                   SoCA (..)  ) where
\end{code}


The \codestyle{Agent}, \codestyle{CKASet}, \codestyle{StimSet}, and \codestyle{Constants} types are imported respectively from the \codestyle{Agent}, \codestyle{Behaviour}, \codestyle{Stimulus}, and \codestyle{LevelThreeSpec} modules so that they may be used in the specification of a \soca. Additionally, the \codestyle{Set} constructor from the Haskell \codestyle{Data.Set} library is imported.
\begin{code}
import Agent          ( Agent )
import Behaviour      ( CKASet )
import Stimulus       ( StimSet )
import LevelThreeSpec ( Constants )

import Data.Set       ( Set )
\end{code}


A \codestyle{SoCAName} specifies the name of a given \soca. The name of a \soca is represented as a \codestyle{String}.
\begin{code}
type SoCAName = String
\end{code}


A \codestyle{SoCA} is a data type for \socas. A \codestyle{SoCA} is represented as a record consisting of a \codestyle{SoCAName} representing the name of the \soca, a \codestyle{StimSet} representing the set of all basic external stimuli, a \codestyle{CKASet} representing the set of all basic agent behaviours, a set of \codestyle{Constants} representing the set of all named constants, and a set of \codestyle{Agent}s representing all of the agents that exist in the system.
\begin{code}
data SoCA = SoCA { ident  :: SoCAName  ,
                   stim   :: StimSet   ,
                   cka    :: CKASet    ,
                   consts :: Constants ,
                   agents :: Set Agent   }
\end{code}