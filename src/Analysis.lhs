\subsection{Analysis}

\begin{SourceHeader}
	FileName			& : & 	Analysis.lhs		\\
	Project				& : &   \projectname		\\
	Last Modified Date 	& : & 	July 26, 2016 		\\
	Last Modified By 	& : &	Jason Jaskolka	 	\\
\end{SourceHeader}

\begin{ModuleDescription}
	The \codestyle{Analysis} module provides the functions to analyse a given system to identify implicit interactions and to measure their severity.
\end{ModuleDescription}


\begin{code}
module Analysis ( runAnalysis ) where
\end{code}


A number of types and functions are imported from various modules in order to run the analysis to identify implicit interactions.
\begin{code}
import Agent       ( Agent     , 
                     AgentName ,
                     name        )
import SoCA        ( SoCA   , 
                     agents   )
import CommPaths   ( CommType (..)          , 
                     Paths                  , 
                     mkTree                 ,  
                     paths                  ,
                     printPaths             ,
                     printPathswithSeverity   )
import PFC         ( pfc, 
                     makeAdjMatrix )

import Implicit    ( findImplicit,
                     severity)

import Data.Set    ( toList )
import Data.Matrix ( Matrix   , 
                     fromList   )
\end{code}


The \codestyle{runAnalysis} function wraps the entire analysis of the identification of implicit interactions and the calculation of their severity.
\begin{code}
runAnalysis :: SoCA -> Paths -> IO (Paths)
runAnalysis s i = do
	putStrLn ""
	putStrLn ""
	putStrLn "------------------------"
	putStrLn "INTENDED INTERACTIONS   "
	putStrLn "------------------------"
	printPaths i
	putStrLn ""
	putStrLn ""
	let as  = makeAgentPairs s
	let xs  = map name $ toList $ agents s
	let top = ((,) <$> xs <*> xs)
	let n   = length xs
	let mat = fromList n n $ makeAdjMatrix s top
	(res,imp) <- analyzePairs xs mat i as 
	putStrLn ""
	putStrLn ""
	putStrLn "------------------------"
	putStrLn "ANALYSIS SUMMARY        "
	putStrLn "------------------------"
	printSummaries as res
	putStrLn "------------------------"
	putStr "TOTAL: "
	let (totalAll,totalImp) = unzip res
	putStrLn $ (show $ sum totalImp) ++ "/" ++ (show $ sum totalAll)
	putStrLn "------------------------"
	return imp 
\end{code}

The \codestyle{analyzePairs} function wraps the entire analysis of the identification of implicit interactions and their severity for each pair of agents.
\begin{code}
analyzePairs :: [AgentName] -> Matrix CommType -> Paths -> [(Agent,Agent)] -> IO ([(Int, Int)],Paths)
analyzePairs as m i [] = return ([],[])
analyzePairs as m i (x:xs) = do
	let a   = fst x
	let b   = snd x
	let str = (name a) ++ " ~>+ " ++ (name b)
	putStrLn ""
	putStr $ str ++ ": "
	let tree = mkTree as m [] (name a) (name b,X)		
	let (cond, paths) = pfc tree a b
	let imp  = findImplicit paths i
	print cond
	putStrLn "------------------------"
	putStrLn $ "ALL PATHS: " ++ str
	putStrLn "------------------------"
	let sev = zip paths (map (severity i) paths)
	printPathswithSeverity sev
	let numAll = length paths
	putStrLn "------------------------"
	putStrLn $ "IMPLICIT PATHS: " ++ str
	putStrLn "------------------------"
	printPaths imp
	let numImp = length imp
	r <- analyzePairs as m i xs
	return $ ([(numAll, numImp)] ++ (fst r), imp ++ (snd r))
\end{code}

The \codestyle{makeAgentPairs} function creates a list of all pairs of agents $(A,B)$ such that $A \neq B$ for the given set of agents in the specified system of communication agents.
\begin{code}
makeAgentPairs :: SoCA -> [(Agent,Agent)]	
makeAgentPairs s = prod as as
	where 
		as         = toList $ agents s
		prod xs ys = [(x,y) | x <- xs, y <- ys, x /= y]
\end{code}

The \codestyle{printSummaries} function prints the summary of the results for identifying implicit interactions.
\begin{code}	
printSummaries :: [(Agent,Agent)] -> [(Int, Int)]-> IO ()
printSummaries [] []         = putStr ""
printSummaries (a:as) (n:ns) = do
	putStr   $ (name $ fst a) ++ " ~>+ " ++ (name $ snd a) ++ ": "
	putStrLn $ (show $ snd n) ++ "/" ++ (show $ fst n)
	printSummaries as ns
\end{code}
