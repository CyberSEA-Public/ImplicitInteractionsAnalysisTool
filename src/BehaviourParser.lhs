\subsection{BehaviourParser}

\begin{SourceHeader}
	FileName			& : & 	BehaviourParser.lhs	\\
	Project				& : &   \projectname		\\
	Last Modified Date 	& : & 	July 2, 2014 		\\
	Last Modified By 	& : &	Jason Jaskolka	 	\\
\end{SourceHeader}

\begin{ModuleDescription}
	The \codestyle{BehaviourParser} module contains an implementation of a parser for \CKAabbrv behaviours. This module allows for the parsing of \codestyle{String} representations of \CKAabbrv behaviour expressions from a \CKAabbrv to \codestyle{CKAExpr} representations.
\end{ModuleDescription}


\begin{code}
module BehaviourParser ( parseCKAExpr , 
                         exprParser     ) where
\end{code}

The \codestyle{BehaviourTypes} module is imported in order to put the types for \CKAabbrv behaviours in scope so that the expressions may be parsed. The \codestyle{(<*)} function from the Haskell \codestyle{Control.Applicative} library is imported to sequence actions while discarding the value of the second argument. Lastly, various functions required for the parsing functionality from the Haskell \codestyle{Text.Parsec} libraries are imported. 
\begin{code}
import BehaviourTypes

import Control.Applicative  ( (<*) )

import Text.Parsec          ( alphaNum ,
                              eof      ,
                              letter   ,
                              parse    ,
                              oneOf    , 
                              (<?>)    , 
                              (<|>)      )
import Text.Parsec.Expr     ( Assoc (AssocLeft)         , 
                              Operator (Infix, Postfix) ,
                              buildExpressionParser       )
import Text.Parsec.Language ( emptyDef )
import Text.Parsec.String   ( Parser )							  
import Text.Parsec.Token    ( GenLanguageDef (..) ,
                              GenTokenParser (..) ,
                              makeTokenParser       )
\end{code}


\codestyle{grammar} provides the \codestyle{CKAExpr} language grammar.
\begin{code}
grammar = emptyDef{ identStart      = letter                      ,
                    identLetter     = alphaNum                    ,
                    opStart         = oneOf "+*;^"                ,
                    opLetter        = oneOf "+*;^"                ,
                    reservedOpNames = ["+", ";", "*", "^;", "^*"] ,
                    reservedNames   = ["1", "0"]                    }	
\end{code}


A \codestyle{TokenParser} is constructed for the \codestyle{CKAExpr} language grammar.
\begin{code}			  
TokenParser{ parens     = m_parens     , 
             identifier = m_identifier , 
             reservedOp = m_reservedOp ,
             reserved   = m_reserved   ,
             semiSep1   = m_semiSep1   ,
             whiteSpace = m_whiteSpace   } = makeTokenParser grammar
\end{code}


\codestyle{exprParser} provides the parser to parse \codestyle{CKAExpr}s and is used as a part of the main parser.
\begin{code}	
exprParser :: Parser CKAExpr
exprParser = buildExpressionParser table term <?> "CKAExpr"
	where
		table = [ [Infix   (m_reservedOp "+"  >> return (Choice))  AssocLeft] ,
		          [Infix   (m_reservedOp ";"  >> return (Seq))     AssocLeft] ,
		          [Infix   (m_reservedOp "*"  >> return (Par))     AssocLeft] ,
		          [Postfix (m_reservedOp "^;" >> return (IterSeq))          ] ,
		          [Postfix (m_reservedOp "^*" >> return (IterPar))          ]   ]
		term  = m_parens exprParser
		        <|> fmap Literal m_identifier
		        <|> (m_reserved "1" >> return (O))
		        <|> (m_reserved "0" >> return (Z))
\end{code}


\codestyle{mainParser} is the main parsing function for \codestyle{CKAExpr}s. It ignores whitespace and ends when the ``end of file'' (\codestyle{eof}) token of the input string is reached.
\begin{code}
mainParser :: Parser CKAExpr
mainParser = m_whiteSpace >> exprParser <* eof	
\end{code}


\codestyle{parseCKAExpr} translates a \codestyle{String} representation of a \CKAabbrv behaviour expression into the \codestyle{CKAExpr} data type representation.
\begin{code}	
parseCKAExpr :: String -> CKAExpr
parseCKAExpr input = case parse mainParser "" input of
	Left  err -> error $ show err
	Right ans -> ans  	
\end{code}