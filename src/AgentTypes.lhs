\subsection{AgentTypes}

\begin{SourceHeader}
	FileName			& : & 	AgentTypes.lhs		\\
	Project				& : &   \projectname		\\
	Last Modified Date 	& : & 	July 30, 2014 		\\
	Last Modified By 	& : &	Jason Jaskolka	 	\\
\end{SourceHeader}

\begin{ModuleDescription}
	The \codestyle{AgentTypes} module contains all of the type definitions for agents. 
\end{ModuleDescription}


\begin{code}
module AgentTypes ( AgentName      ,
                    AgentSpec (..) ,
                    Agent (..)     ,
                    Eq (..)        ,
                    Ord (..)         ) where	
\end{code}


The \codestyle{CKAExpr}, \codestyle{NSMap}, \codestyle{NBMap}, \codestyle{CBSpec} constructors are imported respectively from the \codestyle{Behaviour}, \codestyle{NextStimulus}, \codestyle{NextBehaviour}, and \codestyle{LevelThreeSpec} modules so that they may be used in the specification of an agent.
\begin{code}
import Behaviour      ( CKAExpr )
import NextStimulus   ( NSMap )
import NextBehaviour  ( NBMap )
import LevelThreeSpec ( CBSpec )
\end{code}


\codestyle{AgentName} is a type for the name of an agent in a system. An agent name is represented as a \codestyle{String}.
\begin{code}
type AgentName = String
\end{code}


An \codestyle{AgentSpec} is a data type for the specification of an agent. An \codestyle{AgentSpec} is represented as a record consisting of a \codestyle{CKAExpr} representing the behaviour of the agent, an \codestyle{NBMap} representing the specification of the next behaviour mapping~$\actOp$ the agent, an \codestyle{NBMap} representing the specification of the next stimulus mapping~$\outOp$ the agent, and a \codestyle{CBSpec} representing the \levelThree of the agent.
\begin{code}
data AgentSpec = AgentSpec { expr   :: CKAExpr ,
                             nbmap  :: NBMap   ,
                             nsmap  :: NSMap   ,
                             cbspec :: CBSpec    }
\end{code}


An \codestyle{Agent} is a data type for the specification of a named agent. An \codestyle{Agent} is represented as a record consisting of an \codestyle{AgentName} representing the name of the agent and an \codestyle{AgentSpec} representing the specification of the agent.
\begin{code}
data Agent = Agent { name :: AgentName ,
                     spec :: AgentSpec   }
\end{code}


An instance of the the \codestyle{Eq} class is created for \codestyle{Agent}s. This instance tests for \codestyle{Agent} equality  by their names.
\begin{code}
instance Eq (Agent) where
	(Agent a _) == (Agent b _) = a == b
\end{code}


An instance of the the \codestyle{Ord} class is created for \codestyle{Agent}s. This instance orders \codestyle{Agent}s by their names.
\begin{code}
instance Ord (Agent) where
	(Agent a _) `compare` (Agent b _) = a `compare` b
\end{code}