\subsection{StimulusParser}

\begin{SourceHeader}
	FileName			& : & 	StimulusParser.lhs	\\
	Project				& : &   \projectname		\\
	Last Modified Date 	& : & 	July 2, 2014	 	\\
	Last Modified By 	& : &	Jason Jaskolka	 	\\
\end{SourceHeader}

\begin{ModuleDescription}
	The \codestyle{StimulusParser} module contains an implementation of a parser for external stimuli. This module allows for the parsing of \codestyle{String} representations of stimulus expressions from a stimulus structure to \codestyle{StimExpr} representations.
\end{ModuleDescription}


\begin{code}
module StimulusParser ( parseStimExpr ) where
\end{code}


The \codestyle{StimulusTypes} module is imported in order to put the types for external stimuli in scope so that the expressions may be parsed. The \codestyle{(<*)} function from the Haskell \codestyle{Control.Applicative} library is imported to sequence actions while discarding the value of the second argument. Lastly, various functions required for the parsing functionality from the Haskell \codestyle{Text.Parsec} libraries are imported. 
\begin{code}
import StimulusTypes

import Control.Applicative  ( (<*) )

import Text.Parsec          ( alphaNum ,
                              eof      ,
                              letter   ,
                              parse    ,
                              oneOf    , 
                              (<?>)    , 
                              (<|>)      )
import Text.Parsec.Expr     ( Assoc (AssocLeft)     , 
                              Operator (Infix)      ,
                              buildExpressionParser   )
import Text.Parsec.Language ( emptyDef )
import Text.Parsec.String   ( Parser )							  
import Text.Parsec.Token    ( GenLanguageDef (..) ,
                              GenTokenParser (..) ,
                              makeTokenParser       )
\end{code}


\codestyle{grammar} provides the \codestyle{StimExpr} language grammar.
\begin{code}
grammar = emptyDef{ identStart      = letter                           ,
                    identLetter     = alphaNum                         ,
                    opStart         = oneOf "+*:"                      ,
                    opLetter        = oneOf "+*"                       ,
                    reservedOpNames = ["++", "\8853", "**", "\8857"]    ,
                    reservedNames   = ["N", "\120211", "D", "\120201"]   }		
\end{code}


A \codestyle{TokenParser} is constructed for the \codestyle{StimExpr} language grammar.
\begin{code}
TokenParser{ parens     = m_parens     , 
             identifier = m_identifier , 
             reservedOp = m_reservedOp ,
             reserved   = m_reserved   ,
             semiSep1   = m_semiSep1   ,
             whiteSpace = m_whiteSpace   } = makeTokenParser grammar
\end{code}


\codestyle{exprParser} provides the parser to parse \codestyle{StimExpr}s and is used as a part of the main parser.
\begin{code}
exprParser :: Parser StimExpr
exprParser = buildExpressionParser table term <?> "StimExpr"
	where
		table = [ [Infix (m_reservedOp "++"     >> return (Choice))  AssocLeft] ,
		          [Infix (m_reservedOp "\8853" >> return (Choice))  AssocLeft] ,
		          [Infix (m_reservedOp "**"    >> return (Compose)) AssocLeft] ,
		          [Infix (m_reservedOp "\8857" >> return (Compose)) AssocLeft]   ]
		term  = m_parens exprParser
		        <|> fmap Literal m_identifier
		        <|> (m_reserved "N"       >> return (N))
		        <|> (m_reserved "\120211" >> return (N))
		        <|> (m_reserved "D"       >> return (D))	
		        <|> (m_reserved "\120201" >> return (N))
\end{code}


\codestyle{mainParser} is the main parsing function for \codestyle{StimExpr}s. It ignores whitespace and ends when the ``end of file'' (\codestyle{eof}) token of the input string is reached.
\begin{code}
mainParser :: Parser StimExpr
mainParser = m_whiteSpace >> exprParser <* eof	
\end{code}


\codestyle{parseStimExpr} translates a \codestyle{String} representation of a stimulus expression into the \codestyle{StimExpr} data type representation.
\begin{code}
parseStimExpr :: String -> StimExpr
parseStimExpr input = case parse mainParser "" input of
	Left  err -> error $ show err
	Right ans -> ans   			  
\end{code}