begin AGENT where

	SVa :=  CRANES + PLAN + DOCK + RLSE
	
end


begin NEXT_BEHAVIOUR where
	
	(arrive, CRANES)   = CRANES
	(mnge1, CRANES)    = CRANES
	(mnge2, CRANES)    = CRANES
	(ship1, CRANES)    = CRANES
	(ship2, CRANES)    = CRANES
	(crane1, CRANES)   = CRANES
	(crane2, CRANES)   = CRANES
	(allocd, CRANES)   = PLAN
	(berth, CRANES)    = CRANES
	(dock, CRANES)     = CRANES
	(oper1, CRANES)    = CRANES
	(oper2, CRANES)    = CRANES
	(carrier, CRANES)  = CRANES
	(assgnd, CRANES)   = CRANES
	(serve, CRANES)    = CRANES
	(served, CRANES)   = CRANES
	(done, CRANES)     = CRANES
	(compl1, CRANES)   = CRANES
	(compl2, CRANES)   = CRANES
	(deprt1, CRANES)   = CRANES
	(deprt2, CRANES)   = CRANES
	
	(arrive, PLAN)   = PLAN
	(mnge1, PLAN)    = PLAN
	(mnge2, PLAN)    = PLAN
	(ship1, PLAN)    = CRANES
	(ship2, PLAN)    = PLAN
	(crane1, PLAN)   = PLAN
	(crane2, PLAN)   = PLAN
	(allocd, PLAN)   = PLAN
	(berth, PLAN)    = PLAN
	(dock, PLAN)     = DOCK
	(oper1, PLAN)    = PLAN
	(oper2, PLAN)    = PLAN
	(carrier, PLAN)  = PLAN
	(assgnd, PLAN)   = PLAN
	(serve, PLAN)    = PLAN
	(served, PLAN)   = PLAN
	(done, PLAN)     = PLAN
	(compl1, PLAN)   = PLAN
	(compl2, PLAN)   = PLAN
	(deprt1, PLAN)   = PLAN
	(deprt2, PLAN)   = PLAN
	
	(arrive, DOCK)   = DOCK
	(mnge1, DOCK)    = DOCK
	(mnge2, DOCK)    = DOCK
	(ship1, DOCK)    = CRANES
	(ship2, DOCK)    = DOCK
	(crane1, DOCK)   = DOCK
	(crane2, DOCK)   = DOCK
	(allocd, DOCK)   = DOCK
	(berth, DOCK)    = DOCK
	(dock, DOCK)     = DOCK
	(oper1, DOCK)    = DOCK
	(oper2, DOCK)    = DOCK
	(carrier, DOCK)  = DOCK
	(assgnd, DOCK)   = DOCK
	(serve, DOCK)    = DOCK
	(served, DOCK)   = DOCK
	(done, DOCK)     = RLSE
	(compl1, DOCK)   = DOCK
	(compl2, DOCK)   = DOCK
	(deprt1, DOCK)   = DOCK
	(deprt2, DOCK)   = DOCK
	
	(arrive, RLSE)   = RLSE
	(mnge1, RLSE)    = RLSE
	(mnge2, RLSE)    = RLSE
	(ship1, RLSE)    = CRANES
	(ship2, RLSE)    = RLSE
	(crane1, RLSE)   = RLSE
	(crane2, RLSE)   = RLSE
	(allocd, RLSE)   = RLSE
	(berth, RLSE)    = RLSE
	(dock, RLSE)     = RLSE
	(oper1, RLSE)    = RLSE
	(oper2, RLSE)    = RLSE
	(carrier, RLSE)  = RLSE
	(assgnd, RLSE)   = RLSE
	(serve, RLSE)    = RLSE
	(served, RLSE)   = RLSE
	(done, RLSE)     = RLSE
	(compl1, RLSE)   = RLSE
	(compl2, RLSE)   = RLSE
	(deprt1, RLSE)   = RLSE
	(deprt2, RLSE)   = RLSE

end


begin NEXT_STIMULUS where

	(arrive, CRANES)   = N
	(mnge1, CRANES)    = N
	(mnge2, CRANES)    = N
	(ship1, CRANES)    = N
	(ship2, CRANES)    = N
	(crane1, CRANES)   = N
	(crane2, CRANES)   = N
	(allocd, CRANES)   = berth
	(berth, CRANES)    = N
	(dock, CRANES)     = N
	(oper1, CRANES)    = N
	(oper2, CRANES)    = N
	(carrier, CRANES)  = N
	(assgnd, CRANES)   = N
	(serve, CRANES)    = N
	(served, CRANES)   = N
	(done, CRANES)     = N
	(compl1, CRANES)   = N
	(compl2, CRANES)   = N
	(deprt1, CRANES)   = N
	(deprt2, CRANES)   = N
	
	(arrive, PLAN)   = N
	(mnge1, PLAN)    = N
	(mnge2, PLAN)    = N
	(ship1, PLAN)    = crane1
	(ship2, PLAN)    = N
	(crane1, PLAN)   = N
	(crane2, PLAN)   = N
	(allocd, PLAN)   = N
	(berth, PLAN)    = N
	(dock, PLAN)     = oper1
	(oper1, PLAN)    = N
	(oper2, PLAN)    = N
	(carrier, PLAN)  = N
	(assgnd, PLAN)   = N
	(serve, PLAN)    = N
	(served, PLAN)   = N
	(done, PLAN)     = N
	(compl1, PLAN)   = N
	(compl2, PLAN)   = N
	(deprt1, PLAN)   = N
	(deprt2, PLAN)   = N
	
	(arrive, DOCK)   = N
	(mnge1, DOCK)    = N
	(mnge2, DOCK)    = N
	(ship1, DOCK)    = crane1
	(ship2, DOCK)    = N
	(crane1, DOCK)   = N
	(crane2, DOCK)   = N
	(allocd, DOCK)   = N
	(berth, DOCK)    = N
	(dock, DOCK)     = N
	(oper1, DOCK)    = N
	(oper2, DOCK)    = N
	(carrier, DOCK)  = N
	(assgnd, DOCK)   = N
	(serve, DOCK)    = N
	(served, DOCK)   = N
	(done, DOCK)     = compl1
	(compl1, DOCK)   = N
	(compl2, DOCK)   = N
	(deprt1, DOCK)   = N
	(deprt2, DOCK)   = N
	
	(arrive, RLSE)   = N
	(mnge1, RLSE)    = N
	(mnge2, RLSE)    = N
	(ship1, RLSE)    = crane1
	(ship2, RLSE)    = N
	(crane1, RLSE)   = N
	(crane2, RLSE)   = N
	(allocd, RLSE)   = N
	(berth, RLSE)    = N
	(dock, RLSE)     = N
	(oper1, RLSE)    = N
	(oper2, RLSE)    = N
	(carrier, RLSE)  = N
	(assgnd, RLSE)   = N
	(serve, RLSE)    = N
	(served, RLSE)   = N
	(done, RLSE)     = N
	(compl1, RLSE)   = N
	(compl2, RLSE)   = N
	(deprt1, RLSE)   = N
	(deprt2, RLSE)   = N
	
end


begin CONCRETE_BEHAVIOUR where

	CRANES => [ numCranesA := numContainersA / (CRANEEFF * serviceTA)  ]
	
	PLAN   => [ berthPosA := berthA; 
	            bayPlanA := berthPosA + alloCranesA + manifestA + numBaysA ]
	
	DOCK   => [ dockedA := TRUE ]
	
	RLSE   => [ dockedA := FALSE;
	            berthPosA := NULL;
	            bayPlanA := NULL; 
	            numCranesA := 0  ]	
	
end