begin AGENT where

	SMa := SRVT + POSN + LEAVE 
	
end


begin NEXT_BEHAVIOUR where
	
	(arrive, SRVT)   = SRVT
	(mnge1, SRVT)    = SRVT
	(mnge2, SRVT)    = SRVT
	(ship1, SRVT)    = SRVT
	(ship2, SRVT)    = SRVT
	(crane1, SRVT)   = SRVT
	(crane2, SRVT)   = SRVT
	(allocd, SRVT)   = SRVT
	(berth, SRVT)    = POSN
	(dock, SRVT)     = SRVT
	(oper1, SRVT)    = SRVT
	(oper2, SRVT)    = SRVT
	(carrier, SRVT)  = SRVT
	(assgnd, SRVT)   = SRVT
	(serve, SRVT)    = SRVT
	(served, SRVT)   = SRVT
	(done, SRVT)     = SRVT
	(compl1, SRVT)   = SRVT
	(compl2, SRVT)   = SRVT
	(deprt1, SRVT)   = SRVT
	(deprt2, SRVT)   = SRVT
	
	(arrive, POSN)   = POSN
	(mnge1, POSN)    = POSN
	(mnge2, POSN)    = POSN
	(ship1, POSN)    = POSN
	(ship2, POSN)    = POSN
	(crane1, POSN)   = POSN
	(crane2, POSN)   = POSN
	(allocd, POSN)   = POSN
	(berth, POSN)    = POSN
	(dock, POSN)     = POSN
	(oper1, POSN)    = POSN
	(oper2, POSN)    = POSN
	(carrier, POSN)  = POSN
	(assgnd, POSN)   = POSN
	(serve, POSN)    = POSN
	(served, POSN)   = POSN
	(done, POSN)     = POSN
	(compl1, POSN)   = LEAVE
	(compl2, POSN)   = POSN
	(deprt1, POSN)   = POSN
	(deprt2, POSN)   = POSN
	
	(arrive, LEAVE)   = LEAVE
	(mnge1, LEAVE)    = SRVT
	(mnge2, LEAVE)    = LEAVE
	(ship1, LEAVE)    = LEAVE
	(ship2, LEAVE)    = LEAVE
	(crane1, LEAVE)   = LEAVE
	(crane2, LEAVE)   = LEAVE
	(allocd, LEAVE)   = LEAVE
	(berth, LEAVE)    = LEAVE
	(dock, LEAVE)     = LEAVE
	(oper1, LEAVE)    = LEAVE
	(oper2, LEAVE)    = LEAVE
	(carrier, LEAVE)  = LEAVE
	(assgnd, LEAVE)   = LEAVE
	(serve, LEAVE)    = LEAVE
	(served, LEAVE)   = LEAVE
	(done, LEAVE)     = LEAVE
	(compl1, LEAVE)   = LEAVE
	(compl2, LEAVE)   = LEAVE
	(deprt1, LEAVE)   = LEAVE
	(deprt2, LEAVE)   = LEAVE

end


begin NEXT_STIMULUS where

	(arrive, SRVT)   = N
	(mnge1, SRVT)    = N
	(mnge2, SRVT)    = N
	(ship1, SRVT)    = N
	(ship2, SRVT)    = N
	(crane1, SRVT)   = N
	(crane2, SRVT)   = N
	(allocd, SRVT)   = N
	(berth, SRVT)    = dock
	(dock, SRVT)     = N
	(oper1, SRVT)    = N
	(oper2, SRVT)    = N
	(carrier, SRVT)  = N
	(assgnd, SRVT)   = N
	(serve, SRVT)    = N
	(served, SRVT)   = N
	(done, SRVT)     = N
	(compl1, SRVT)   = N
	(compl2, SRVT)   = N
	(deprt1, SRVT)   = N
	(deprt2, SRVT)   = N
	
	(arrive, POSN)   = N
	(mnge1, POSN)    = N
	(mnge2, POSN)    = N
	(ship1, POSN)    = N
	(ship2, POSN)    = N
	(crane1, POSN)   = N
	(crane2, POSN)   = N
	(allocd, POSN)   = N
	(berth, POSN)    = N
	(dock, POSN)     = N
	(oper1, POSN)    = N
	(oper2, POSN)    = N
	(carrier, POSN)  = N
	(assgnd, POSN)   = N
	(serve, POSN)    = N
	(served, POSN)   = N
	(done, POSN)     = N
	(compl1, POSN)   = deprt1
	(compl2, POSN)   = N
	(deprt1, POSN)   = N
	(deprt2, POSN)   = N
	
	(arrive, LEAVE)   = N
	(mnge1, LEAVE)    = ship1
	(mnge2, LEAVE)    = N
	(ship1, LEAVE)    = N
	(ship2, LEAVE)    = N
	(crane1, LEAVE)   = N
	(crane2, LEAVE)   = N
	(allocd, LEAVE)   = N
	(berth, LEAVE)    = N
	(dock, LEAVE)     = N
	(oper1, LEAVE)    = N
	(oper2, LEAVE)    = N
	(carrier, LEAVE)  = N
	(assgnd, LEAVE)   = N
	(serve, LEAVE)    = N
	(served, LEAVE)   = N
	(done, LEAVE)     = N
	(compl1, LEAVE)   = N
	(compl2, LEAVE)   = N
	(deprt1, LEAVE)   = N
	(deprt2, LEAVE)   = N
	
end


begin CONCRETE_BEHAVIOUR where

	SRVT  => [ serviceTA := departTA - arriveTA - waitTA ]
	
	POSN  => [ dockPosA := berthPosA ]
	
	LEAVE => [ dockPosA := NULL; serviceTA := 0 ]	
	
end